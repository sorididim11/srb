
#ifndef __COMM_h__
#define __COMM_h__


/***************************** C headers ****************************/

#include <srbp/assert.h>
#include <stddef.h> // for wchar_t
#include <sys/types.h>

/***************************** config *******************************/

#ifdef _WINDOWS
#include <srbp/config-win32.h>
#else
#include <srbp/config.h>
#endif


#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif


#if (defined(PIC) && defined(HAVE_SHARED_EXCEPTS)) || \
    (!defined(PIC) && defined(HAVE_EXCEPTS))
#define HAVE_EXCEPTIONS 1
#endif

#ifdef SRBP_CONF_NO_EXCEPTIONS
#undef HAVE_EXCEPTS
#undef HAVE_SHARED_EXCEPTS
#undef HAVE_EXCEPTIONS
#endif


#ifdef HAVE_NAMESPACE
#define SRBP_NAMESPACE_DECL namespace
#define SRBP_NAMESPACE_END_DECL 
#define SRBP_EXPORT_DECL extern
#define SRBP_EXPORT_FCT_DECL extern
#define SRBP_INLINE_FCT_DECL static inline
#define SRBP_EXPORT_VAR_DECL extern
#define SRBP_EXPORT_TYPEVAR_DECL extern
#else
#error "no namespaces"
#endif

#define SRBP_EXPORT_CONST_DECL SRBP_EXPORT_VAR_DECL
#define SRBP_SCOPE(S,V) S::V


// This Macro is used in the idl compiler even on unix
#define _VC_NAMESPACE_HACK "_VCHACK__"

// Assume that Namespace handling is buggy in Visual-C++
#define _VC_NAMESPACE_BUG

#ifdef _WINDOWS
#undef environ

#undef SRBP_SCOPE
#define SRBP_SCOPE(S,V) V
    
#ifdef BUILD_SRBP_DLL
#undef  SRBP_EXPORT_VAR_DECL
#undef  SRBP_EXPORT_CONST_DECL
#define SRBP_EXPORT_VAR_DECL extern
#define SRBP_EXPORT_CONST_DECL extern
#define SRBP_EXPORT_TYPEVAR_DECL extern
#else
#undef  SRBP_EXPORT_VAR_DECL
#undef  SRBP_EXPORT_CONST_DECL
#define SRBP_EXPORT_VAR_DECL __declspec(dllimport) 
#define SRBP_EXPORT_CONST_DECL extern __declspec(dllimport) 
#define SRBP_EXPORT_TYPEVAR_DECL extern
#endif
#endif


/***************************** STL headers **************************/


#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>

#if defined(_WINDOWS) && !defined(__MINGW32__)
#include <limits>
#endif

#ifdef HAVE_NAMESPACE
namespace std {
    typedef int ____foo____;
};
using namespace std;
#endif


typedef vector<int>::size_type srbp_vec_size_type;


/************************** SRBP templates **************************/

#include <srbp/version.h>
#include <srbp/types.h>
#include <srbp/sequence.h>
#include <srbp/sequence_special.h>
#include <srbp/array.h>
#include <srbp/template.h>
//#include <srbp/fixed.h>
#include <srbp/native.h>

/*
 * VC++ insanity
 */

#if defined(_WINDOWS) && !defined(__MINGW32__)
namespace COMM {
	using namespace COMM;
}
#ifndef SRBP_CONF_NO_POA
namespace PortableServer {
	using namespace PortableServer;
}
#endif
#endif

/*************************** module COMM ***************************/

#include <srbp/basic.h>
#include <srbp/magic.h>
#include <srbp/address.h>
#include <srbp/transport.h>
#include <srbp/buffer.h>
#include <srbp/string.h>
#include <srbp/codec.h>
#include <srbp/except.h>
#include <srbp/object.h>
//#include <srbp/value.h>
//#include <srbp/basic_seq.h>
#include <srbp/srb.h>

#include <srbp/static.h>
//#include <srbp/tcconst.h>

/********************** Global ******************************************/

COMM::ULong srbp_string_hash (const char *, COMM::ULong);

//#include <srbp/throw.h>
#include <srbp/template_impl.h>

#endif // __COMM_h__
