
#ifndef __srbp_transport_impl_h__
#define __srbp_transport_impl_h__

namespace SRBP {	
	
class SelectDispatcher : public COMM::Dispatcher {
    typedef fd_set FDSet;

    struct FileEvent {
	Event event;
	COMM::Long fd;
	COMM::DispatcherCallback *cb;
	COMM::Boolean deleted;

	FileEvent () {}
	FileEvent (Event _ev, COMM::Long _fd, COMM::DispatcherCallback *_cb)
	    : event(_ev), fd(_fd), cb(_cb), deleted(FALSE)
	{}
    };
    struct TimerEvent {
	Event event;
	COMM::Long delta;
	COMM::DispatcherCallback *cb;

	TimerEvent () {}
	TimerEvent (Event _ev, COMM::Long _delta,
		    COMM::DispatcherCallback *_cb)
	    : event(_ev), delta(_delta), cb(_cb)
	{}
    };

    list<FileEvent> fevents;
    list<TimerEvent> tevents;

    COMM::Long last_update;
    COMM::Boolean init;
    COMM::Long locked;
    COMM::Boolean modified;
    FDSet curr_wset, curr_rset, curr_xset;
    COMM::Long fd_max;

    void lock ();
    void unlock ();
    COMM::Boolean islocked () const;

    COMM::Long gettime () const;
    void update_tevents ();
    void handle_tevents ();
    void handle_fevents (FDSet &rset, FDSet &wset, FDSet &xset);
    void update_fevents ();
    void sleeptime (OSMisc::TimeVal &);
public:
    SelectDispatcher ();
    virtual ~SelectDispatcher ();
    virtual void rd_event (COMM::DispatcherCallback *, COMM::Long fd);
    virtual void wr_event (COMM::DispatcherCallback *, COMM::Long fd);
    virtual void ex_event (COMM::DispatcherCallback *, COMM::Long fd);
    virtual void tm_event (COMM::DispatcherCallback *, COMM::ULong tmout);
    virtual void remove (COMM::DispatcherCallback *, Event);
    virtual void move (COMM::Dispatcher *);
    virtual void run (COMM::Boolean infinite = TRUE);
    virtual COMM::Boolean idle () const;
};


class TCPTransport : public COMM::Transport,
		     public COMM::DispatcherCallback {
    COMM::Dispatcher *rdisp, *wdisp;
    COMM::TransportCallback *rcb, *wcb;
    COMM::Boolean ateof;
    string err;
    InetAddress local_addr, peer_addr;
    COMM::Boolean is_blocking;
    COMM::Boolean is_buffering;
public:
    COMM::Long fd;

    TCPTransport (COMM::Long fd = -1);
    ~TCPTransport ();
    
    void rselect (COMM::Dispatcher *, COMM::TransportCallback *);
    void wselect (COMM::Dispatcher *, COMM::TransportCallback *);
    void callback (COMM::Dispatcher *, COMM::Dispatcher::Event);
    
    COMM::Boolean bind (const COMM::Address *);
    COMM::Boolean connect (const COMM::Address *);
    void close ();
    void block (COMM::Boolean doblock = TRUE);
    COMM::Boolean isblocking ();
    void buffering (COMM::Boolean dobuffering = TRUE);
    COMM::Boolean isbuffering ();
    COMM::Boolean isreadable ();
    
    COMM::Long read (void *, COMM::Long len);
    COMM::Long write (const void *, COMM::Long len);
    
    const COMM::Address *addr ();
    const COMM::Address *peer ();
    
    COMM::Boolean eof () const;
    COMM::Boolean bad () const;
    string errormsg () const;
};


class TCPTransportServer : public COMM::TransportServer,
			   public COMM::DispatcherCallback {
    COMM::Dispatcher *adisp;
    COMM::TransportServerCallback *acb;
    COMM::Long fd;
    string err;
    InetAddress local_addr;
    COMM::Boolean listening;
    void listen ();
    COMM::Boolean is_blocking;
public:
    TCPTransportServer ();
    ~TCPTransportServer ();
    
    void aselect (COMM::Dispatcher *,
    		  COMM::TransportServerCallback *);
    void callback (COMM::Dispatcher *, COMM::Dispatcher::Event);
    
    COMM::Boolean bind (const COMM::Address *);
    void close ();
    void block (COMM::Boolean doblock = TRUE);
    COMM::Boolean isblocking ();
    
    COMM::Transport *accept ();
    const COMM::Address *addr ();
    
    COMM::Boolean bad () const;
    string errormsg () const;
};

}

#endif // __srbp_transport_impl_h__
