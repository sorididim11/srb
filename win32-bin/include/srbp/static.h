
#ifndef __srbp_static_h__
#define __srbp_static_h__

namespace COMM {

class StaticRequest;
typedef StaticRequest *StaticRequest_ptr;

class StaticRequest : public SRBRequest {
	COMM::SRB_ptr _srb;
	COMM::DataEncoder *_ec;
	COMM::DataDecoder *_dc;
	COMM::String_var _svcname;
	COMM::Address *_addr;
	COMM::Address *_peer;
	COMM::ULong _msgid;

	COMM::ExceptionType _ex_type;

	COMM::SRBMarshaller *_ma;
	COMM::SRBDemarshaller *_dma;
public:
	StaticRequest (COMM::SRB_ptr, COMM::DataEncoder*,
			COMM::DataDecoder*,
			const char *svcname,
			const COMM::Address* addr,
			const COMM::Address* peer);
	~StaticRequest ();

	const COMM::Address *addr ();
	const COMM::Address *peer ();
	const char *svc_name ();

	COMM::DataEncoder *get_encoder ();
	COMM::DataDecoder *get_decoder ();
	void set_marshaller (COMM::SRBMarshaller *, COMM::SRBDemarshaller *);
	void marshal (COMM::DataEncoder *);
	COMM::Boolean demarshal (COMM::DataDecoder *);
	void set_sys_exception (COMM::SysExceptionCode);
	void set_sys_exception (COMM::SysExceptionCode, const char *);
	void set_exception_type (COMM::ExceptionType);
	COMM::ExceptionType get_exception_type ();
	
	COMM::Boolean copy_out_data (SRBRequest *);
	COMM::Boolean copy_in_data (SRBRequest *);

	void invoke ();

	static StaticRequest_ptr _duplicate (StaticRequest_ptr o) {
		if (o)
			o->_ref();
		return o;
	}
	static StaticRequest_ptr _nil () {
		return 0;
	}
};

typedef ObjVar<StaticRequest> StaticRequest_var;

}

#endif //__srbp_static_h__
