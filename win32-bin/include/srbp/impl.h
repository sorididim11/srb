
#ifndef __srbp_impl_h__
#define __srbp_impl_h__

#include <srbp/util.h>

#if defined(_WINDOWS) && !defined(__MINGW32__)
#include <sys/timeb.h>
#include <winsock2.h>
#elif defined (_WINDOWS) && defined(__MINGW32__)
#include <sys/timeb.h>
#include <winsock.h>
#else
#include <sys/time.h>
#include <sys/socket.h>
#endif

#ifdef HAVE_SYS_SELECT_H
#include <sys/select.h>
#endif

/*
 * SRBP Namespace
 */

#if defined(_WINDOWS) && !defined(__MINGW32__)
namespace SRBP {
	using namespace SRBP;
}
#endif
#include <srbp/shlib.h>
#include <srbp/process.h>
#include <srbp/address_impl.h>
#include <srbp/transport_impl.h>
#include <srbp/shlib_impl.h>
#include <srbp/process_impl.h>
#include <srbp/codec_impl.h>
#include <srbp/psa.h>
#include <srbp/iop.h>

#endif // __srbp_impl_h__
