
#ifndef __srbp_codec_impl_h__
#define __srbp_codec_impl_h__

namespace SRBP {

class CDREncoder : public COMM::DataEncoder {
	COMM::ByteOrder data_bo, mach_bo;
public:
	CDREncoder ();
#ifdef _WINDOWS
	CDREncoder (COMM::Buffer *, COMM::Boolean do_free,
		COMM::ByteOrder);
#else
	CDREncoder (COMM::Buffer *, COMM::Boolean do_free = TRUE,
		COMM::ByteOrder = COMM::DefaultEndian);
#endif
	~CDREncoder ();

	COMM::DataEncoder *clone () const;
	COMM::DataEncoder *clone (COMM::Buffer *,
    			       COMM::Boolean dofree_b = TRUE) const;
	COMM::DataDecoder *decoder () const;
	COMM::DataDecoder *decoder (COMM::Buffer *,
    				 COMM::Boolean dofree_b = TRUE) const;

	void put_short (COMM::Short);
	void put_ushort (COMM::UShort);
	void put_long (COMM::Long);
	void put_longlong (COMM::LongLong);
	void put_ulong (COMM::ULong);
	void put_ulonglong (COMM::ULongLong);
	void put_float (COMM::Float);
	void put_double (COMM::Double);
	void put_longdouble (COMM::LongDouble);
	void put_char (COMM::Char);
	void put_char_raw (COMM::Char);
	void put_wchar (COMM::WChar);
	void put_octet (COMM::Octet);
	void put_boolean (COMM::Boolean);
	void put_string (const char *);
	void put_string_raw (const char *);
	void put_wstring (const wchar_t *);
    
	void put_shorts (const COMM::Short *, COMM::ULong);
	void put_ushorts (const COMM::UShort *, COMM::ULong);
	void put_longs (const COMM::Long *, COMM::ULong);
	void put_longlongs (const COMM::LongLong *, COMM::ULong);
	void put_ulongs (const COMM::ULong *, COMM::ULong);
	void put_ulonglongs (const COMM::ULongLong *, COMM::ULong);
	void put_floats (const COMM::Float *, COMM::ULong);
	void put_doubles (const COMM::Double *, COMM::ULong);
	void put_longdoubles (const COMM::LongDouble *, COMM::ULong);
	void put_chars (const COMM::Char *, COMM::ULong);
	void put_chars_raw (const COMM::Char *, COMM::ULong);
	void put_wchars (const COMM::WChar *, COMM::ULong);
	void put_booleans (const COMM::Boolean *, COMM::ULong);

	COMM::ULong max_alignment () const;

	COMM::ByteOrder byteorder () const;
	void byteorder (COMM::ByteOrder);
};

class CDRDecoder : public COMM::DataDecoder {
	COMM::ByteOrder data_bo, mach_bo;
public:
	CDRDecoder ();
#ifdef _WINDOWS
	CDRDecoder (COMM::Buffer *b, COMM::Boolean do_free,
		COMM::ByteOrder);
#else
	CDRDecoder (COMM::Buffer *b, COMM::Boolean do_free = TRUE,
		COMM::ByteOrder = COMM::DefaultEndian);
#endif
	~CDRDecoder ();

	COMM::DataDecoder *clone () const;
	COMM::DataDecoder *clone (COMM::Buffer *b,
    			       COMM::Boolean dofree_b = TRUE) const;
	COMM::DataEncoder *encoder () const;
	COMM::DataEncoder *encoder (COMM::Buffer *b,
    				 COMM::Boolean dofree_b = TRUE) const;

	COMM::Boolean get_short (COMM::Short &);
	COMM::Boolean get_ushort (COMM::UShort &);
	COMM::Boolean get_long (COMM::Long &);
	COMM::Boolean get_longlong (COMM::LongLong &);
	COMM::Boolean get_ulong (COMM::ULong &);
	COMM::Boolean get_ulonglong (COMM::ULongLong &);
	COMM::Boolean get_float (COMM::Float &);
	COMM::Boolean get_double (COMM::Double &);
	COMM::Boolean get_longdouble (COMM::LongDouble &);
	COMM::Boolean get_char (COMM::Char &);
	COMM::Boolean get_char_raw (COMM::Char &);
	COMM::Boolean get_wchar (COMM::WChar &);
	COMM::Boolean get_octet (COMM::Octet &);
	COMM::Boolean get_boolean (COMM::Boolean &);
	COMM::Boolean get_string (COMM::String_out);
	COMM::Boolean get_string_raw (COMM::String_out);
	COMM::Boolean get_wstring (COMM::WString_out);

	COMM::Boolean get_shorts (COMM::Short *, COMM::ULong);
	COMM::Boolean get_ushorts (COMM::UShort *, COMM::ULong);
	COMM::Boolean get_longs (COMM::Long *, COMM::ULong);
	COMM::Boolean get_longlongs (COMM::LongLong *, COMM::ULong);
	COMM::Boolean get_ulongs (COMM::ULong *, COMM::ULong);
	COMM::Boolean get_ulonglongs (COMM::ULongLong *, COMM::ULong);
	COMM::Boolean get_floats (COMM::Float *, COMM::ULong);
	COMM::Boolean get_doubles (COMM::Double *, COMM::ULong);
	COMM::Boolean get_longdoubles (COMM::LongDouble *, COMM::ULong);
	COMM::Boolean get_chars (COMM::Char *, COMM::ULong);
	COMM::Boolean get_chars_raw (COMM::Char *, COMM::ULong);
	COMM::Boolean get_wchars (COMM::WChar *, COMM::ULong);
	COMM::Boolean get_booleans (COMM::Boolean *, COMM::ULong);

	COMM::ULong max_alignment () const;

	COMM::ByteOrder byteorder () const;
	void byteorder (COMM::ByteOrder);
};

}

#endif //__srbp_codec_impl_h__

