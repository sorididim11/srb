
#ifndef __srbp_os_misc_h__
#define __srbp_os_misc_h__

#if defined(_WINDOWS) && !defined(__MINGW32__)

#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <sys/timeb.h>
#include <io.h>
#include <process.h>

struct OSMisc {
    static void _init()
    {
        SignalMutex= CreateMutex (NULL, FALSE, NULL);
    }
    typedef __int64 longlong;
    typedef unsigned __int64 ulonglong;
    static HANDLE SignalMutex;

#if 0    
    struct TimeVal {
      SRBP_Long tv_sec;
      SRBP_Long tv_usec;
    };
#else
    typedef struct timeval TimeVal;
#endif

    static TimeVal gettime ()
    {
      struct _timeb timebuffer;
      ::_ftime( &timebuffer );
      TimeVal ct;
      ct.tv_sec  = timebuffer.time;
      ct.tv_usec = timebuffer.millitm * 1000;
      return ct;
    }

    enum AccessMode {
	ACCESS_READ = 0,
	ACCESS_WRITE = 0,
	ACCESS_EXEC = 0
    };

    static SRBP_Long access (const char *file, AccessMode md)
    {
	return ::_access (file, md);
    }

    static SRBP_Long getpid ()
    {
	return ::_getpid();
    }

    static FILE *popen (const char *cmd, const char *mode)
    {
	return ::_popen (cmd, mode);
    }

    static void pclose (FILE *fp)
    {
	::_pclose (fp);
    }

    typedef short SigState;

    static void block_sigs (SigState &)
    {
        WaitForSingleObject( SignalMutex, INFINITE );
    }

    static void restore_sigs (SigState &)
    {
        ReleaseMutex( SignalMutex);
    }
};

#elif defined(_WINDOWS) && defined (__MINGW32__)

#include <windows.h>
#include <winsock.h>
#include <stdio.h>
#include <sys/timeb.h>
#include <io.h>
#include <process.h>


struct OSMisc {
    static void _init()
    {
        SignalMutex= CreateMutex (NULL, FALSE, NULL);
    }
    typedef SRBP_LongLong longlong;
    typedef SRBP_ULongLong ulonglong;
    static HANDLE SignalMutex;

    typedef struct timeval TimeVal;

    static TimeVal gettime ()
    {
      struct _timeb timebuffer;
      ::_ftime( &timebuffer );
      TimeVal ct;
      ct.tv_sec  = timebuffer.time;
      ct.tv_usec = timebuffer.millitm * 1000;
      return ct;
    }

    enum AccessMode {
	ACCESS_READ = 0,
	ACCESS_WRITE = 0,
	ACCESS_EXEC = 0
    };

    static SRBP_Long access (const char *file, AccessMode md)
    {
	return ::_access (file, md);
    }

    static SRBP_Long getpid ()
    {
	return ::_getpid();
    }

    static FILE *popen (const char *cmd, const char *mode)
    {
	return ::_popen (cmd, mode);
    }

    static void pclose (FILE *fp)
    {
	::_pclose (fp);
    }

    typedef short SigState;

    static void block_sigs (SigState &)
    {
        WaitForSingleObject( SignalMutex, INFINITE );
    }

    static void restore_sigs (SigState &)
    {
        ReleaseMutex( SignalMutex);
    }
};

#else

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/time.h>

#ifdef __CYGWIN32__
extern "C" int gettimeofday (struct timeval *, struct timezone *);
#endif

struct OSMisc {
    typedef long long longlong;
    typedef unsigned long long ulonglong;

    static void _init()
    {
    }

    typedef struct timeval TimeVal;

    static TimeVal gettime ()
    {
      struct timeval tv;
#ifdef HAVE_GETTIMEOFDAY_TWOARGS
      gettimeofday (&tv, NULL);
#else
      gettimeofday (&tv);
#endif
      return tv;
    }

    enum AccessMode {
	ACCESS_READ = R_OK,
	ACCESS_WRITE = W_OK,
	ACCESS_EXEC = X_OK
    };

    static SRBP_Long access (const char *file, AccessMode md)
    {
	return ::access (file, md);
    }

    static SRBP_Long getpid ()
    {
	return ::getpid();
    }

    static FILE *popen (const char *cmd, const char *mode)
    {
	return ::popen (cmd, mode);
    }

    static void pclose (FILE *fp)
    {
	::pclose (fp);
    }

    typedef sigset_t SigState;

    static void block_sigs (SigState &osigs)
    {
        SigState sigs;
        sigemptyset (&sigs);
        // XXX ...
        sigaddset (&sigs, SIGCHLD);
	sigprocmask (SIG_BLOCK, &sigs, &osigs);
    }

    static void restore_sigs (SigState &osigs)
    {
	sigprocmask (SIG_SETMASK, &osigs, NULL);
    }
};

#endif

#endif // __srbp_os_misc_h__
