
#ifndef __srbp_shlib_h__
#define __srbp_shlib_h__

namespace SRBP {

class SharedLib {
public:
    virtual ~SharedLib ();

    virtual void *symbol (const char *) = 0;
    virtual const char *error () = 0;
    virtual operator COMM::Boolean () = 0;

    virtual const char *name () = 0;

    COMM::Boolean init ();
    void exit ();
};

}


#endif // __srbp_shlib_h__
