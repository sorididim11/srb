
#ifndef __srbp_except_h__
#define __srbp_except_h__

namespace COMM {

enum CompletionStatus {
    COMPLETED_YES,
    COMPLETED_NO,
    COMPLETED_MAYBE
};

enum ExceptionType {
    NO_EXCEPTION,
    USER_EXCEPTION,
    SYSTEM_EXCEPTION
};

enum SysExceptionCode {
	SERVICE_NOT_EXIST,
	NUM_OF_EX_CODES
};

SRBP_EXPORT_FCT_DECL string get_sys_exception_name (SysExceptionCode ex_code);
SRBP_EXPORT_FCT_DECL string get_sys_exception_name (SysExceptionCode ex_code, const char *msg);

}

#endif // __srbp_except_h__

