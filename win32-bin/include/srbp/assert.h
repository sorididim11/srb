
#ifndef __srbp_assert_h__
#define __srbp_assert_h__

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

/*
 * this makes the data section of the generated .o files tiny compared
 * to the real assert (which prints out the failed expression, thus
 * generating a string in the data section for each assertion), because
 * asserts are used in the templates...
 */

void srbp_assert (const char *, int);

#undef assert

#if !defined (NDEBUG)
#define assert(exp) if (!(exp)) srbp_assert (__FILE__, __LINE__)
#else
#define assert(exp)
#endif

#define NOT_IMPLEMENTED srbp_assert (__FILE__, __LINE__)

#endif /* __srbp_assert_h__ */
