

#ifndef __srbp_native_h__
#define __srbp_native_h__

/*
 * These are definitions for "native" idl types. They are realized as
 * macros rather than typedefs because they may refer to types in
 * other contexts (namespaces) that we don't have access to.
 *
 * The idl definition
 *
 * module ABC {
 *   native DEF;
 * };
 *
 * results in
 *
 * namespace ABC {
 *   typedef SRBP_Native_ABC_DEF DEF;
 * };
 */

#define SRBP_Native_COMM_AbstractBase COMM::AbstractBase*
#define SRBP_Native_PortableServer_Servant PortableServer::ServantBase*
#define SRBP_Native_PortableServer_ServantLocator_Cookie void*

#endif
