
#ifndef __srbp_basic_h__
#define __srbp_basic_h__

namespace COMM {

// basic types
typedef SRBP_Short  Short;
typedef SRBP_Short& Short_out;
typedef SRBP_Long  Long;
typedef SRBP_Long& Long_out;
typedef SRBP_LongLong  LongLong;
typedef SRBP_LongLong& LongLong_out;
typedef SRBP_UShort  UShort;
typedef SRBP_UShort& UShort_out;
typedef SRBP_ULong  ULong;
typedef SRBP_ULong& ULong_out;
typedef SRBP_ULongLong  ULongLong;
typedef SRBP_ULongLong& ULongLong_out;
typedef SRBP_Float  Float;
typedef SRBP_Float& Float_out;
typedef SRBP_Double  Double;
typedef SRBP_Double& Double_out;
typedef SRBP_LongDouble  LongDouble;
typedef SRBP_LongDouble& LongDouble_out;
typedef SRBP_Char  Char;
typedef SRBP_Char& Char_out;
typedef SRBP_WChar  WChar;
typedef SRBP_WChar& WChar_out;
typedef SRBP_Boolean  Boolean;
typedef SRBP_Boolean& Boolean_out;
typedef SRBP_Octet Octet;
typedef SRBP_Octet& Octet_out;


typedef ULong Flags;


// forwards
class Environment;
class NamedValue;
class NVList;
class ExceptionList;
class ContextList;
class Request;
class Context;
class Principal;
class TypeCode;
class TypeCodeChecker;
class BOA;
class SRB;
class ImplementationDef;
class ImplRepository;
class Repository;
class InterfaceDef;
class OperationDef;
class ServerRequestBase;
class ServerRequest;
class ImplementationBase;
class DynamicImplementation;
class StaticServerRequest;
class ValueDef;

class String_var;
class String_out;
class WString_var;
class WString_out;
class Object;
class Exception;
class SystemException;
class UserException;
class UnknownUserException;
class Any;

class StaticTypeInfo;
class StaticAny;

class DataEncoder;
class DataDecoder;

class ValueBase;
class AbstractBase;

typedef Environment *Environment_ptr;
typedef ObjOut<Environment> Environment_out;  // needed in srb.h
typedef Environment *EnvironmentRef;
typedef NamedValue *NamedValue_ptr;
typedef NamedValue *NamedValueRef;
typedef NVList *NVList_ptr;
typedef NVList *NVListRef;
typedef ExceptionList *ExceptionList_ptr;
typedef ExceptionList *ExceptionListRef;
typedef ContextList *ContextList_ptr;
typedef ObjOut<ContextList> ContextList_out;  // needed in srb.h
typedef ContextList *ContextListRef;
typedef Request *Request_ptr;
typedef ObjOut<Request> Request_out;
typedef Request *RequestRef;
typedef Context *Context_ptr;
typedef Context *ContextRef;
typedef Principal *Principal_ptr;
typedef Principal *PrincipalRef;
typedef TypeCode *TypeCode_ptr;
typedef TypeCode *TypeCodeRef;
typedef ObjVar<TypeCode> TypeCode_var;
typedef ObjOut<TypeCode> TypeCode_out;
typedef BOA *BOA_ptr;
typedef BOA *BOARef;
typedef SRB *SRB_ptr;
typedef SRB *SRBRef;
typedef InterfaceDef *InterfaceDef_ptr;
// InterfaceDefRef is defined in ir.h
typedef OperationDef *OperationDef_ptr;
// OperationDefRef is defined in ir.h
typedef ValueDef *ValueDef_ptr;
typedef ServerRequest *ServerRequest_ptr;
typedef ServerRequest *ServerRequestRef;
typedef ServerRequestBase *ServerRequestBase_ptr;
typedef ServerRequestBase *ServerRequestBaseRef;
typedef StaticServerRequest *StaticServerRequest_ptr;
typedef Object *Object_ptr;
typedef ObjOut<Object> Object_out;  // needed in any.h
typedef Object *ObjectRef;

}

#endif // __srbp_basic_h__
