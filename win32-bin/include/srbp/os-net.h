
#ifndef __srbp_os_net_h__
#define __srbp_os_net_h__

#define MAX_SNDSIZE 4096

#if defined (_WINDOWS)
#include <errno.h>
#if !defined (__MINGW32__)
#include <winsock2.h>
#include <limits>
#else
#include <winsock.h>
#endif

#include <iostream>

using std::cout;

#ifndef EWOULDBLOCK
#define EWOULDBLOCK WSAEWOULDBLOCK
#endif

#ifndef ECONNRESET
#define ECONNRESET WSAECONNRESET
#endif

class OSNet {

#ifdef _DEBUG

  struct WSA_Number_Text_struct {
    int Number;
    char* Text;
  };

#define WSA_Number_Text_table_n 88
  static WSA_Number_Text_struct WSA_Number_Text_table[WSA_Number_Text_table_n];

  static void PrintWSAError (int n) 
  {
    int i;
    for( i=0; i < WSA_Number_Text_table_n; i++ ) {
      if (WSA_Number_Text_table[i].Number == n) {
	cout << "WSA: " << 
	  WSA_Number_Text_table[i].Number << " = " <<
	  WSA_Number_Text_table[i].Text << "\n";
	//break;
      }
    }
  }

#endif // _DEBUG

public:
    static void set_errno ()
    {
	int err = WSAGetLastError();
  
	switch (err) {
	case 0:
	    errno = 0;
	    break;

	case WSAEINTR:
	    errno = EINTR;
	    break;

	case WSAEWOULDBLOCK:
	    errno = EWOULDBLOCK; // EAGAIN
	    break;

	case WSAECONNRESET:
	    errno = ECONNRESET;
	    break;

        case WSAESHUTDOWN:
            errno = ECONNRESET;
            break;

	default:
#ifdef _DEBUG
	    cerr << "unhandled WSAGetLastError() result" << endl;
            PrintWSAError( err );
#endif
	    errno = -1;
	    break;
	}
    }

    static void sock_init ()
    {
	static bool winsock_init = false;

	if( !winsock_init ) {
	    WORD wVersionRequested;
	    WSADATA wsaData;
	    int err;
	
	    wVersionRequested = MAKEWORD( 2, 0 );
	    
	    err = WSAStartup( wVersionRequested, &wsaData );
	    assert( err == 0 );
	    winsock_init = true;
	}
    }

    static void sock_block (SRBP_Long fd, SRBP_Boolean on)
    {
	unsigned long para = on ? 0 /* NONBLOCK disabled */ : 1;
	ioctlsocket (fd,FIONBIO,&para);
    }

    static void sock_reuse (SRBP_Long fd, SRBP_Boolean on)
    {
	int _on = on;
	::setsockopt (fd, SOL_SOCKET, SO_REUSEADDR, (char *)&_on, sizeof(_on));
    }

    static void sock_broadcast (SRBP_Long fd, SRBP_Boolean on)
    {
	int _on = on;
	::setsockopt (fd, SOL_SOCKET, SO_BROADCAST, (char *)&_on, sizeof(_on));
    }

    static void sock_ndelay (SRBP_Long fd, SRBP_Boolean on)
    {
#ifdef TCP_NODELAY
	int _on = on;
	::setsockopt (fd, IPPROTO_TCP, TCP_NODELAY, (char *)&_on, sizeof(_on));
#endif
    }
    
    static void sock_bufsize(SRBP_Long fd, SRBP_ULong bufsize)
    {
        ::setsockopt(fd, SOL_SOCKET, SO_SNDBUF, (char*)&bufsize, sizeof(bufsize));
        ::setsockopt(fd, SOL_SOCKET, SO_RCVBUF, (char*)&bufsize, sizeof(bufsize));
    }

    static SRBP_Long sock_read (SRBP_Long fd, void *buf, SRBP_ULong count)
    {
	WSASetLastError(0);

	int ret = recv(fd,(char*)buf,count,0);
	if (ret != SOCKET_ERROR)
	    return ret;

	set_errno();

	return -1;
    }

    static SRBP_Long sock_read_from (SRBP_Long fd, void *buf,
				     SRBP_ULong count,
				     struct sockaddr *sa,
				     SRBP_ULong sa_count)
    {
	WSASetLastError(0);

	socket_size_t fromlen = sa_count;
	int ret = recvfrom(fd,(char*)buf,count,0,(socket_addr_t)sa,&fromlen);
	if (ret != SOCKET_ERROR)
	    return ret;

	set_errno();

	return -1;
    }

#if 1

    static SRBP_Long sock_write (SRBP_Long fd, const void *buf,
				 SRBP_ULong count)
    {
	WSASetLastError(0);

	int ret = send(fd,(char*)buf,count,0);
	if (ret != SOCKET_ERROR)
	    return ret;

	set_errno();

	return -1;
    }

#endif
#if 0
    static SRBP_Boolean iswritable(SRBP_Long fd)
    {
    fd_set wset;
    struct timeval tm;

    FD_ZERO (&wset);
    FD_SET (fd, &wset);

    tm.tv_sec = 1;
    tm.tv_usec = 0;

    int r = ::select (fd+1, 0, (select_addr_t)&wset, 0, &tm);
    return r > 0;
    }

    static SRBP_Long sock_write (SRBP_Long fd, const void *buf, 
		    SRBP_ULong count) {
	WSASetLastError(0);

    SRBP_ULong remained = count;
    SRBP_ULong tosend = 0;
    int tries = 0;
    
    while(remained > 0)
    {
        if(remained > MAX_SNDSIZE)
            tosend = MAX_SNDSIZE;
        else
            tosend = remained;
        
        iswritable(fd);
        
	    SRBP_Long ret = send(fd, (char*)buf + count - remained, tosend, 0);

	    if (ret == SOCKET_ERROR)
	    {
	        tries++;
	        if(tries > 10)
	        {
	            printf("(E) Socket write error: reached max retries.\n");
	            set_errno();
	            return -1;
	        }
	    }
	    
	    if(ret > 0)
	        remained -= ret;
    }
    
    return (count - remained);
    }
#endif

    static SRBP_Long sock_write_to (SRBP_Long fd, const void *buf,
				    SRBP_ULong count,
				    const struct sockaddr *sa,
				    SRBP_ULong sa_count)
    {
	WSASetLastError(0);

	int ret = sendto(fd,(char*)buf,count,0,(socket_addr_t)sa,sa_count);
	if (ret != SOCKET_ERROR)
	    return ret;

	set_errno();

	return -1;
    }

    static SRBP_Long sock_close (SRBP_Long fd)
    {
	WSASetLastError(0);

	int ret = closesocket(fd);
	if (ret == 0)
	    return 0;

	set_errno();
	
	return -1;
    }
};

#else // _WINDOWS

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>

#include <sys/socket.h>
#ifdef HAVE_SYS_UN_H
#include <sys/un.h>
#endif
#include <netinet/in.h>
#include <arpa/inet.h>
#ifdef HAVE_NETINET_TCP_H
#include <netinet/tcp.h>
#endif
#include <netdb.h>
#include <signal.h>

#ifndef INADDR_LOOPBACK
#define INADDR_LOOPBACK 0x7F000001
#endif

#ifndef INADDR_ANY
#define INADDR_ANY 0x0
#endif

#ifdef __CYGWIN32__
extern "C" int ioctl (int, int, void *);
#endif

class OSNet {
#ifdef __CYGWIN32__
    static SRBP_Boolean sock_blocking (SRBP_Long fd)
    {
	COMM::Long flags = fcntl (fd, F_GETFL, 0);
	assert (flags != -1);
	return !!(flags & O_NONBLOCK);
    }

    static SRBP_Boolean sock_readable (SRBP_Long fd)
    {
	SRBP_Long nbytes = 0;
	::ioctl (fd, FIONREAD, &nbytes);
	return nbytes > 0;
    }
#endif
public:
    static void set_errno ()
    {
    }

    static void sock_init ()
    {
        // when peer closes a unix socket we receive SIGPIPE ...
        signal (SIGPIPE, SIG_IGN);
    }

    static void sock_block (SRBP_Long fd, SRBP_Boolean on)
    {
	COMM::Long flags = fcntl (fd, F_GETFL, 0);
	assert (flags != -1);
	flags = on ? (flags & ~O_NONBLOCK) : (flags | O_NONBLOCK);
	fcntl (fd, F_SETFL, flags);
    }

    static void sock_reuse (SRBP_Long fd, SRBP_Boolean on)
    {
	int _on = on;
	::setsockopt (fd, SOL_SOCKET, SO_REUSEADDR, (char *)&_on, sizeof(_on));
    }

    static void sock_broadcast (SRBP_Long fd, SRBP_Boolean on)
    {
	int _on = on;
	::setsockopt (fd, SOL_SOCKET, SO_BROADCAST, (char *)&_on, sizeof(_on));
    }

    static void sock_ndelay (SRBP_Long fd, SRBP_Boolean on)
    {
#ifdef TCP_NODELAY
	int _on = on;
	::setsockopt (fd, IPPROTO_TCP, TCP_NODELAY, (char *)&_on, sizeof(_on));
#endif
    }

    static SRBP_Long sock_read (SRBP_Long fd, void *buf, SRBP_ULong count)
    {
#ifdef __CYGWIN32__
	if (!sock_blocking (fd) && !sock_readable (fd)) {
	    errno = EWOULDBLOCK;
	    return -1;
	}
#endif
	return ::read (fd, buf, count);
    }

    static SRBP_Long sock_read_from (SRBP_Long fd, void *buf,
				     SRBP_ULong count,
				     struct sockaddr *sa,
				     SRBP_ULong sa_count)
    {
#ifdef __CYGWIN32__
	if (!sock_blocking (fd) && !sock_readable (fd)) {
	    errno = EWOULDBLOCK;
	    return -1;
	}
#endif
	socket_size_t fromlen = sa_count;
	return ::recvfrom (fd, (char *)buf, count, 0,
			   (socket_addr_t)sa, &fromlen);
    }

    static SRBP_Long sock_write (SRBP_Long fd, const void *buf,
				 SRBP_ULong count)
    {
	return ::write (fd, buf, count);
    }

    static SRBP_Long sock_write_to (SRBP_Long fd, const void *buf,
				    SRBP_ULong count,
				    const struct sockaddr *sa,
				    SRBP_ULong sa_count)
    {
	return ::sendto (fd, (char *)buf, count, 0,
			 (socket_addr_t)sa, sa_count);
    }

    static SRBP_Long sock_close (SRBP_Long fd)
    {
	return ::close (fd);
    }
};

#endif // _WINDOWS

#endif // __srbp_os_net_h__
