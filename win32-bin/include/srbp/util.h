
#ifndef __srbp_util_h__
#define __srbp_util_h__

#include <COMM.h>
#ifdef HAVE_ANSI_CPLUSPLUS_HEADERS
#include <iostream>
#else
#include <iostream.h>
#endif
#include <ctype.h>
#include <srbp/os-misc.h>

namespace SRBP {

class Logger {
public:
	enum MessageType {
		Info,           /* trivial stuff */
		Warning,        /* warnings that might be of interest */
		Error,          /* errors that must be noticed */
		GISP,           /* i.e. sending and receiving of messages */
		IISP,           /* i.e. incoming and outcoming connections */
		Transport,      /* raw message contents */
		All 		/* must be the maximum value */
	};

	static ostream ** _out;
	static Logger * _instance;

public:
	Logger ();
	~Logger ();

	static void Log (MessageType, COMM::Boolean = 1, const char * file = 0);
	static void Log (const char *, COMM::Boolean = 1, const char * file = 0);
	static COMM::Boolean IsLogged (MessageType);
	static ostream & Stream (MessageType);
};

}


class SRBPGetOpt {
public:
	typedef map<string, string, less<string> > OptMap;
	typedef vector<pair<string,string> > OptVec;
private:
	OptMap _in_opts;
	OptVec _out_opts;

	COMM::Boolean parse (const vector<string> &args, vector<int> &erase,
			  COMM::Boolean ignore = FALSE);
public:
	SRBPGetOpt (const OptMap &opts);
	~SRBPGetOpt ();

	COMM::Boolean parse (int &argc, char *argv[], 
		    COMM::Boolean ignore = FALSE);
	COMM::Boolean parse (const vector<string> &args, 
		    COMM::Boolean ignore = FALSE);
	COMM::Boolean parse (const string &filename, 
		    COMM::Boolean ignore = FALSE);

	const OptVec &opts () const;
};

template<class V>
inline COMM::Long
srbp_vec_compare (V v1, V v2)
{
    int len = v1.size() < v2.size() ? v1.size() : v2.size();
    for (int i = 0; i < len; ++i) {
	if (v1[i] < v2[i])
	    return -1;
	if (v2[i] < v1[i])
	    return 1;
    }
    return v1.size() - v2.size();
}

static inline COMM::Long
srbp_key_compare (const COMM::Octet *k1, const COMM::Octet *k2,
		  COMM::Long len)
{
    while (--len >= 0) {
	if (*k1 != *k2)
	    return (COMM::Long)*k1 - (COMM::Long)*k2;
	++k1;
	++k2;
    }
    return 0;
}


static inline COMM::Octet
srbp_from_xdigit (COMM::Octet c)
{
    c = tolower (c);
    assert (isxdigit (c));
    return isdigit (c) ? c - '0' : c - 'a' + 10;
}

static inline COMM::Octet
srbp_to_xdigit (COMM::Octet o)
{
    static const char *xdigits = "0123456789abcdef";
    assert (o < 16);
    return xdigits[o];
}

extern void srbp_ieee2float (COMM::Octet ieee[4], COMM::Float &f);
extern void srbp_float2ieee (COMM::Octet ieee[4], COMM::Float  f);
extern void srbp_ieee2double (COMM::Octet ieee[8], COMM::Double &f);
extern void srbp_double2ieee (COMM::Octet ieee[8], COMM::Double  f);
extern void srbp_ieee2ldouble (COMM::Octet ieee[16], COMM::LongDouble &f);
extern void srbp_ldouble2ieee (COMM::Octet ieee[16], COMM::LongDouble  f);
extern COMM::Boolean srbp_fnmatch (const char *repoid, const char *patt);

extern string xstrerror (int);
extern string xdec (int);
extern string xdec (long);
extern string xdec (OSMisc::longlong);
extern string xdec (unsigned int);
extern string xdec (unsigned long);
extern string xdec (OSMisc::ulonglong);

extern size_t xwcslen (const wchar_t *);
extern int xwcscmp (const wchar_t *, const wchar_t *);
extern wchar_t *xwcscpy (wchar_t *, const wchar_t *);
extern wchar_t *xwcsncpy (wchar_t *, const wchar_t *, size_t);

extern char * srbp_url_encode (const COMM::Octet *, COMM::ULong);
extern COMM::Octet * srbp_url_decode (const char *, COMM::ULong &);

extern COMM::ULong srbp_string_hash (const char *s, COMM::ULong max);

#ifndef HAVE_STRERROR_PROTO
extern "C" char *strerror (int);
#endif

#ifndef HAVE_GETHOSTNAME_PROTO
extern "C" int gethostname (char *, size_t);
#endif

#ifndef HAVE_FINITE_PROTO
extern "C" int finite (double);
#endif

#ifndef HAVE_FTIME_PROTO
extern "C" int ftime (struct timeb *);
#endif

#endif // __srbp_util_h__
