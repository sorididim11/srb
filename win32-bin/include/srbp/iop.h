
#ifndef __srbp_iop_h__
#define __srbp_iop_h__

namespace GISP {

enum MsgType {
	Request = 0,
	Reply,
	CancelRequest,
	CloseConnection,
	MessageError
};

enum ReplyStatusType {
	NO_EXCEPTION = 0,
	USER_EXCEPTION,
	SYSTEM_EXCEPTION
};

}

namespace SRBP {

class GISPCodec;

class GISPInContext {
    COMM::DataDecoder *_dc;
    COMM::Buffer *_buf;
    COMM::Boolean _delete_buf;
    COMM::Boolean _delete_dc;

public:
    GISPInContext (GISPCodec *, COMM::Buffer *);
    ~GISPInContext ();
    COMM::DataDecoder *dc() { return _dc; }
    void buffer (COMM::Buffer *);
    COMM::DataDecoder *_retn();
};

class GISPOutContext {
    COMM::DataEncoder *_ec;
    COMM::Buffer *_buf;
    COMM::Boolean _delete_buf;
    COMM::Boolean _delete_ec;

public:
    GISPOutContext (GISPCodec *);
    GISPOutContext (COMM::DataEncoder *);
    ~GISPOutContext ();
    COMM::DataEncoder *ec() { return _ec; }
    void reset ();
    COMM::Buffer *_retn();
};

#define GISP_BYTEORDER_BIT 1

class GISPCodec;
typedef GISPCodec *GISPCodec_ptr;
typedef ObjVar<GISPCodec> GISPCodec_var;

class GISPCodec : public COMM::ServerlessObject {
    COMM::DataDecoder *_dc_proto;
    COMM::DataEncoder *_ec_proto;
    COMM::ULong _headerlen;
    COMM::ULong _size_offset;
    COMM::UShort _gisp_ver;

    COMM::ULong put_header (GISPOutContext &out, GISP::MsgType);
    void put_size (GISPOutContext &out, COMM::ULong key);
    /*
    void put_contextlist (GISPOutContext &out,
			  const IOP::ServiceContextList &ctx,
			  COMM::Boolean codesets = FALSE);
    void put_target (GISPOutContext &out, COMM::Object_ptr obj);
    COMM::Boolean put_args (GISPOutContext &out, COMM::SRBRequest *,
			     COMM::Boolean inp);

    COMM::Boolean get_contextlist (GISPInContext &in,
				    IOP::ServiceContextList &ctx,
				    COMM::Boolean codesets = FALSE);
    COMM::Boolean get_target (GISPInContext &in, COMM::Object_ptr obj);
    */
public:
    GISPCodec (COMM::DataDecoder *dc, COMM::DataEncoder *ec,
               COMM::UShort gisp_ver = 0x0100);
    ~GISPCodec ();

    COMM::DataDecoder *dc_proto()
    { return _dc_proto; }

    COMM::DataEncoder *ec_proto()
    { return _ec_proto; }

    COMM::UShort version () const
    { return _gisp_ver; }

    COMM::ULong header_length ()
    { return _headerlen; }

    COMM::Boolean put_invoke_request (GISPOutContext &out,
				       COMM::ULong req_id,
				       COMM::Octet response_flags,
				       COMM::SRBRequest *req);

    COMM:: Boolean put_invoke_reply (GISPOutContext &out,
				      COMM::ULong req_id,
				      GISP::ReplyStatusType,
				      COMM::SRBRequest *req);
/*
    COMM::Boolean put_invoke_reply_offset (GISPOutContext &out,
					    COMM::SRBRequest *req);

    COMM::Boolean put_cancel_request (GISPOutContext &out,
				       COMM::ULong req_id);
*/
    COMM::Boolean put_close_msg (GISPOutContext &out);

    COMM::Boolean put_error_msg (GISPOutContext &out);

    COMM::Boolean get_header (GISPInContext &in, GISP::MsgType &,
			       COMM::ULong &sz, COMM::Octet &flags);
    COMM::Boolean check_header (GISPInContext &in, GISP::MsgType &,
				 COMM::ULong &sz, COMM::Octet &flags);
    COMM::Boolean get_invoke_request (GISPInContext &in,
				       COMM::ULong &req_id,
				       COMM::Octet &response_flags,
				       COMM::SRBRequest * &req);

    COMM::Boolean get_invoke_reply1 (GISPInContext &in,
				      COMM::ULong &req_id,
				      GISP::ReplyStatusType &);

    COMM::Boolean get_invoke_reply2 (GISPInContext &in,
				      COMM::ULong req_id,
				      GISP::ReplyStatusType,
				      COMM::SRBRequest *req);

    COMM::Boolean get_cancel_request (GISPInContext &in,
				       COMM::ULong &req_id);

    COMM::Boolean get_close_msg (GISPInContext &in);

    COMM::Boolean get_error_msg (GISPInContext &in);
	
    static GISPCodec *_duplicate (GISPCodec *o) {
	if (o)
	    o->_ref();
	return o;
    }
    static GISPCodec *_nil () {
	return 0;
    }
};

class GISPRequest : public COMM::SRBRequest {
	string _svcname;

	COMM::DataDecoder *_idc;
	COMM::ULong _istart;

	COMM::DataEncoder *_oec;
	COMM::DataDecoder *_odc;
	COMM::ULong _ostart;
	COMM::Buffer _obuf;

	GISPCodec_ptr _codec;

	COMM::Boolean _is_except;
	COMM::SysExceptionCode _ex_code;
	string _ex_msg;

	COMM::SRBMarshaller *_ma;
	COMM::SRBDemarshaller *_dma;
public:
	GISPRequest (const char *svc, COMM::DataDecoder *indata, GISPCodec *);
	~GISPRequest ();
    
	const COMM::Address *addr ();
	const COMM::Address *peer ();
	const char *svc_name ();

	COMM::DataEncoder *get_encoder ();
	COMM::DataDecoder *get_decoder ();
	void set_marshaller (COMM::SRBMarshaller *, COMM::SRBDemarshaller *);
	void marshal (COMM::DataEncoder *);
	COMM::Boolean demarshal (COMM::DataDecoder *);
	void set_sys_exception (COMM::SysExceptionCode);
	void set_sys_exception (COMM::SysExceptionCode, const char *);
	void set_exception_type (COMM::ExceptionType);
	COMM::ExceptionType get_exception_type ();
	
	COMM::Boolean copy_out_data (SRBRequest *);
	COMM::Boolean copy_in_data (SRBRequest *);

	static GISPRequest* _nil () {
		return 0;
	}
	static GISPRequest* _duplicate (GISPRequest *o) {
		if (o)
			o->_ref();
		return o;
	}
};

struct GISPConnCallback;

class GISPConn : public COMM::SRBConn, public COMM::TransportCallback,
	public COMM::DispatcherCallback {
	COMM::SRB_ptr _srb;
	COMM::Dispatcher *_disp;
	COMM::Transport *_transp;
	list<COMM::Buffer *> _outbufs;
	COMM::Buffer *_inbufs;
	COMM::Buffer *_inbuf;
	COMM::ULong _inlen;
	COMM::Octet _inflags;
	GISPConnCallback *_cb;
	GISPCodec *_codec;
	GISPInContext _inctx;
	COMM::ULong _max_message_size;

	COMM::Long _refcnt;
	COMM::Long _idle_tmout;
	COMM::Boolean _have_tmout;
	COMM::Boolean _have_wselect;

	void check_idle ();
	void check_busy ();
	void do_write ();
public:
	GISPConn (COMM::SRB_ptr, COMM::Transport *, 
			GISPConnCallback *, GISPCodec *,
			COMM::Long tmout = 0, COMM::ULong max_size = 0);
	virtual ~GISPConn ();

	virtual COMM::StaticRequest *create_request (const char *svcname);

	virtual void callback (COMM::Transport *, 
			COMM::TransportCallback::Event);
	virtual void callback (COMM::Dispatcher *, 
			COMM::DispatcherCallback::Event);

	void ref ();
	COMM::Boolean deref (COMM::Boolean all = FALSE);

	void do_read ();

	void output (COMM::Buffer *);
	COMM::Buffer *input ();
	void flush ();

	COMM::Transport *transport () {
		return _transp;
	}

	GISPCodec *codec () { 
		return _codec; 
	}

	void buffering (COMM::Boolean dobuffering);
};

struct GISPConnCallback {
	enum Event { InputReady, Closed, Idle };
	virtual COMM::Boolean callback (GISPConn *, Event) = 0;
	virtual ~GISPConnCallback ();
};

class IISPProxyInvokeRec {
public:
	typedef COMM::ULong MsgId;
private:
	MsgId _id;
	GISPConn *_conn;
	COMM::SRBRequest *_req;

public:
	IISPProxyInvokeRec () {}

	~IISPProxyInvokeRec () {}

	void free () {}

	void init (MsgId idval, GISPConn *connval, COMM::SRBRequest *req = 0) {
		_id = idval;
		_conn = connval;
		_req = req;
	}

	GISPConn *conn () const
	{ return _conn; }

	COMM::SRBRequest *request () const
	{ return _req; }

	MsgId id () const
	{ return _id; }
};

class IISPProxy : public COMM::ServiceAdapter, public GISPConnCallback {
	typedef map<MsgId, IISPProxyInvokeRec *, less<MsgId> > MapIdConn;

	struct addrcomp :
		binary_function<const COMM::Address *, const COMM::Address *, bool> {
		bool operator() (const COMM::Address *a1,
			 const COMM::Address *a2) const
		{ return a1->compare (*a2) < 0; }
	};
	typedef map<const COMM::Address *, GISPConn *, addrcomp> MapAddrConn;

	MapIdConn _ids;
	MapAddrConn _conns;
	COMM::SRB_ptr _srb;
	COMM::UShort _gisp_ver;
	COMM::ULong _max_message_size;

	IISPProxyInvokeRec *_cache_rec;
	COMM::Boolean _cache_used;

	static COMM::Boolean _isblocking;

	IISPProxyInvokeRec *create_invoke ();
	IISPProxyInvokeRec *get_invoke (MsgId id);
	void add_invoke (IISPProxyInvokeRec *rec);
	void del_invoke (MsgId id);
	void abort_invoke (MsgId id);
	void redo_invoke (MsgId id);

	void conn_error (GISPConn *, COMM::Boolean send_error = TRUE);

	COMM::Boolean handle_input (GISPConn *);
	void exec_invoke_reply (GISPInContext &, COMM::ULong req_id,
			    GISP::ReplyStatusType,
			    COMM::SRBRequest *,
			    GISPConn *conn);
	COMM::Boolean handle_invoke_reply (GISPConn *conn, GISPInContext &);
public:
	IISPProxy (COMM::SRB_ptr,
               COMM::UShort gisp_ver = 0x0100,
	       COMM::ULong max_size = 0);
	~IISPProxy ();

	static void block (COMM::Boolean b) {
		_isblocking = b;
	}
	static COMM::Boolean isblocking () {
		return _isblocking;
	}

	COMM::Boolean has_service (const char *svcname);
	COMM::Boolean is_local () const;

	COMM::Boolean invoke (COMM::ULong, COMM::SRBRequest *, COMM::Boolean response_exp);
	int answer_invoke(COMM::ULong, COMM::SRBRequest *, COMM::InvokeStatus);
	void shutdown (COMM::Boolean wait_for_completion);
	GISPConn *make_conn (const COMM::Address *, COMM::Boolean create = TRUE,
			 COMM::UShort version = 0);
	void kill_conn (GISPConn *, COMM::Boolean redo = FALSE);
	COMM::Boolean callback (GISPConn *, GISPConnCallback::Event);
};

class IISPServerInvokeRec {
public:
	typedef COMM::ULong MsgId;
private:
	COMM::SRBRequest *_req;
	MsgId _srbid, _reqid;
	GISPConn *_conn;
public:
	IISPServerInvokeRec ();
	~IISPServerInvokeRec ();
	void free ();

	void init_invoke (GISPConn *conn, MsgId reqid, MsgId srbid,
		      COMM::SRBRequest *req);

	COMM::SRBRequest *req()
	{ return _req; }
    
	MsgId srbid()
	{ return _srbid; }

	void srbid (MsgId id)
	{ _srbid = id; }

	MsgId reqid()
	{ return _reqid; }

	GISPConn *conn()
	{ return _conn; }
};

class IISPServer : public COMM::ServiceAdapter, public COMM::SRBCallback,
	public GISPConnCallback, public COMM::TransportServerCallback {
	typedef COMM::ULong MsgId;
	typedef map<MsgId, IISPServerInvokeRec *, less<MsgId> > MapIdConn;
	typedef list<GISPConn *> ListConn;
	typedef vector<COMM::TransportServer *> VecTranspServ;

	VecTranspServ _tservers;
	MapIdConn _srbids;

	COMM::SRB_ptr _srb;

	IISPServerInvokeRec *_cache_rec;
	COMM::Boolean _cache_used;
	COMM::UShort _iisp_ver;
	COMM::ULong _max_message_size;

	IISPServerInvokeRec *create_invoke();
	IISPServerInvokeRec *get_invoke_reqid (MsgId, GISPConn *conn);
	IISPServerInvokeRec *get_invoke_srbid (MsgId);
	
	void add_invoke (IISPServerInvokeRec *);
	void del_invoke_reqid (MsgId, GISPConn *conn);
	void del_invoke_srbid (MsgId);
	void abort_invoke_srbid (MsgId);


	COMM::Boolean handle_input (GISPConn *);
	MsgId exec_invoke_request (GISPInContext &, COMM::SRBRequest *,
			COMM::Boolean resp_exp, GISPConn *conn,
			MsgId msgid);
	COMM::Boolean handle_invoke_request (GISPConn *conn, GISPInContext &);
	COMM::Boolean handle_cancel_request (GISPConn *conn, GISPInContext &);
	void handle_invoke_reply (MsgId msgid);

	void conn_error (GISPConn *, COMM::Boolean send_error = TRUE);
	void conn_closed (GISPConn *);
public:
	ListConn _conns;

	IISPServer (COMM::SRB_ptr, COMM::UShort iisp_ver = 0x0100,
		COMM::ULong max_size = 0);
	~IISPServer ();

	COMM::Boolean listen (COMM::Address *);
	COMM::Boolean listen ();

	COMM::Boolean has_service (const char *svcname);
	COMM::Boolean is_local () const;

	COMM::Boolean invoke (COMM::ULong, COMM::SRBRequest *, 
			COMM::Boolean response_exp);
	int answer_invoke(COMM::ULong, COMM::SRBRequest *, COMM::InvokeStatus);
	void shutdown (COMM::Boolean wait_for_completion);

	void callback (COMM::SRB_ptr, MsgId, COMM::SRBCallback::Event);
	COMM::Boolean callback (GISPConn *, GISPConnCallback::Event);
	void callback (COMM::TransportServer *, 
			COMM::TransportServerCallback::Event);

	void kill_conn (GISPConn *);
};

}

#endif // __srbp_iop_h__
