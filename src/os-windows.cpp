
#include <COMM.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <srbp/impl.h>
#include <srbp/template_impl.h>
#include <srbp/util.h>
#include <srbp/os-net.h>
#include <srbp/os-misc.h>


#ifdef _DEBUG
OSNet::WSA_Number_Text_struct
OSNet::WSA_Number_Text_table[WSA_Number_Text_table_n] = {
    { ENAMETOOLONG, "ENAMETOOLONG" },
    { ENOTEMPTY, "ENOTEMPTY" },
    { EWOULDBLOCK, "EWOULDBLOCK" },
    { HOST_NOT_FOUND, "HOST_NOT_FOUND" },
    { NO_ADDRESS, "NO_ADDRESS" },
    { NO_DATA, "NO_DATA" },
    { NO_RECOVERY, "NO_RECOVERY" },
    { TRY_AGAIN, "TRY_AGAIN" },
    { WSABASEERR, "WSABASEERR" },
    { WSAEACCES, "WSAEACCES" },
    { WSAEADDRINUSE, "WSAEADDRINUSE" },
    { WSAEADDRNOTAVAIL, "WSAEADDRNOTAVAIL" },
    { WSAEAFNOSUPPORT, "WSAEAFNOSUPPORT" },
    { WSAEALREADY, "WSAEALREADY" },
    { WSAEBADF, "WSAEBADF" },
    { WSAECANCELLED, "WSAECANCELLED" },
    { WSAECONNABORTED, "WSAECONNABORTED" },
    { WSAECONNREFUSED, "WSAECONNREFUSED" },
    { WSAECONNRESET, "WSAECONNRESET" },
    { WSAEDESTADDRREQ, "WSAEDESTADDRREQ" },
    { WSAEDISCON, "WSAEDISCON" },
    { WSAEDQUOT, "WSAEDQUOT" },
    { WSAEFAULT, "WSAEFAULT" },
    { WSAEHOSTDOWN, "WSAEHOSTDOWN" },
    { WSAEHOSTUNREACH, "WSAEHOSTUNREACH" },
    { WSAEINPROGRESS, "WSAEINPROGRESS" },
    { WSAEINTR, "WSAEINTR" },
    { WSAEINVAL, "WSAEINVAL" },
    { WSAEINVALIDPROCTABLE, "WSAEINVALIDPROCTABLE" },
    { WSAEINVALIDPROVIDER, "WSAEINVALIDPROVIDER" },
    { WSAEISCONN, "WSAEISCONN" },
    { WSAELOOP, "WSAELOOP" },
    { WSAEMFILE, "WSAEMFILE" },
    { WSAEMSGSIZE, "WSAEMSGSIZE" },
    { WSAENAMETOOLONG, "WSAENAMETOOLONG" },
    { WSAENETDOWN, "WSAENETDOWN" },
    { WSAENETRESET, "WSAENETRESET" },
    { WSAENETUNREACH, "WSAENETUNREACH" },
    { WSAENOBUFS, "WSAENOBUFS" },
    { WSAENOMORE, "WSAENOMORE" },
    { WSAENOPROTOOPT, "WSAENOPROTOOPT" },
    { WSAENOTCONN, "WSAENOTCONN" },
    { WSAENOTEMPTY, "WSAENOTEMPTY" },
    { WSAENOTSOCK, "WSAENOTSOCK" },
    { WSAEOPNOTSUPP, "WSAEOPNOTSUPP" },
    { WSAEPFNOSUPPORT, "WSAEPFNOSUPPORT" },
    { WSAEPROCLIM, "WSAEPROCLIM" },
    { WSAEPROTONOSUPPORT, "WSAEPROTONOSUPPORT" },
    { WSAEPROTOTYPE, "WSAEPROTOTYPE" },
    { WSAEPROVIDERFAILEDINIT, "WSAEPROVIDERFAILEDINIT" },
    { WSAEREFUSED, "WSAEREFUSED" },
    { WSAEREMOTE, "WSAEREMOTE" },
    { WSAESHUTDOWN, "WSAESHUTDOWN" },
    { WSAESOCKTNOSUPPORT, "WSAESOCKTNOSUPPORT" },
    { WSAESTALE, "WSAESTALE" },
    { WSAETIMEDOUT, "WSAETIMEDOUT" },
    { WSAETOOMANYREFS, "WSAETOOMANYREFS" },
    { WSAEUSERS, "WSAEUSERS" },
    { WSAEWOULDBLOCK, "WSAEWOULDBLOCK" },
    { WSAHOST_NOT_FOUND, "WSAHOST_NOT_FOUND" },
    { WSANOTINITIALISED, "WSANOTINITIALISED" },
    { WSANO_DATA, "WSANO_DATA" },
    { WSANO_RECOVERY, "WSANO_RECOVERY" },
    { WSASERVICE_NOT_FOUND, "WSASERVICE_NOT_FOUND" },
    { WSASYSCALLFAILURE, "WSASYSCALLFAILURE" },
    { WSASYSNOTREADY, "WSASYSNOTREADY" },
    { WSATRY_AGAIN, "WSATRY_AGAIN" },
    { WSATYPE_NOT_FOUND, "WSATYPE_NOT_FOUND" },
    { WSAVERNOTSUPPORTED, "WSAVERNOTSUPPORTED" },
    { WSA_E_CANCELLED, "WSA_E_CANCELLED" },
    { WSA_E_NO_MORE, "WSA_E_NO_MORE" },
    { WSA_FLAG_MULTIPOINT_C_LEAF, "WSA_FLAG_MULTIPOINT_C_LEAF" },
    { WSA_FLAG_MULTIPOINT_C_ROOT, "WSA_FLAG_MULTIPOINT_C_ROOT" },
    { WSA_FLAG_MULTIPOINT_D_LEAF, "WSA_FLAG_MULTIPOINT_D_LEAF" },
    { WSA_FLAG_MULTIPOINT_D_ROOT, "WSA_FLAG_MULTIPOINT_D_ROOT" },
    { WSA_FLAG_OVERLAPPED, "WSA_FLAG_OVERLAPPED" },
    { WSA_INFINITE, "WSA_INFINITE" },
    { WSA_INVALID_HANDLE, "WSA_INVALID_HANDLE" },
    { WSA_INVALID_PARAMETER, "WSA_INVALID_PARAMETER" },
    { WSA_IO_INCOMPLETE, "WSA_IO_INCOMPLETE" },
    { WSA_IO_PENDING, "WSA_IO_PENDING" },
    { WSA_MAXIMUM_WAIT_EVENTS, "WSA_MAXIMUM_WAIT_EVENTS" },
    { WSA_NOT_ENOUGH_MEMORY, "WSA_NOT_ENOUGH_MEMORY" },
    { WSA_OPERATION_ABORTED, "WSA_OPERATION_ABORTED" },
    { WSA_WAIT_EVENT_0, "WSA_WAIT_EVENT_0" },
    { WSA_WAIT_FAILED, "WSA_WAIT_FAILED" },
    { WSA_WAIT_IO_COMPLETION, "WSA_WAIT_IO_COMPLETION" },
    { WSA_WAIT_TIMEOUT, "WSA_WAIT_TIMEOUT" }
};
#endif
HANDLE OSMisc::SignalMutex=NULL;


/**************************** UnixProcess *****************************/


#ifdef HAVE_NAMESPACE
namespace SRBP {

  SRBP::UnixProcess::ListProcess UnixProcess::_procs;
  // List of Processhandles, except for first one, which
  // is a WakeUp Event for the Waiting Thread
  HANDLE SRBP::UnixProcess::s_childprocs[OSWIN_MAXPROCS+1];
  DWORD SRBP::UnixProcess::s_childpids[OSWIN_MAXPROCS+1];
  int SRBP::UnixProcess::s_numofchildren=0;
  // Handle of the waiting thread
  HANDLE SRBP::UnixProcess::s_waitthread=NULL;
  int SRBP::UnixProcess::s_stop_waiting=FALSE;
  
};
#else
SRBP::UnixProcess::ListProcess SRBP::UnixProcess::_procs;
#endif

SRBP::UnixProcess::UnixProcess (const char *cmd, SRBP::ProcessCallback *cb)
{
    WaitForSingleObject(OSMisc::SignalMutex,INFINITE);
    _exit_status = -1;
    _pid = 0;
    _detached = FALSE;
    _cb = cb;
    _procs.push_back (this);
    _args = cmd;
    
    hRequestExitEvent = NULL;
    
    ReleaseMutex(OSMisc::SignalMutex);  
}

SRBP::UnixProcess::~UnixProcess ()
{
    WaitForSingleObject(OSMisc::SignalMutex,INFINITE);
    for (ListProcess::iterator pos = _procs.begin(); pos != _procs.end(); ++pos) {
	if (*pos == this) {
	    _procs.erase (pos);

            CloseHandle(hRequestExitEvent);
            ReleaseMutex(OSMisc::SignalMutex);      
	    if (!_detached && !exited()) {
		// terminate ();
            }
	    return;
	}
    }
    assert (0);
}



COMM::Boolean
SRBP::UnixProcess::run ()
{
    STARTUPINFO StartupInfo;    
    PROCESS_INFORMATION processInfo;     	

    StartupInfo.cb = sizeof(StartupInfo);
    GetStartupInfo(&StartupInfo); 
    StartupInfo.lpReserved = NULL;
    StartupInfo.lpTitle = NULL;

    BOOL result = CreateProcess( NULL, (char*)_args.c_str(), NULL, NULL,
                                 FALSE, CREATE_NEW_CONSOLE /* 0 */ , NULL, NULL,
                                 &StartupInfo, &processInfo ); 

    CloseHandle(processInfo.hThread); // must be closed and we don't need it

    _hProcess= processInfo.hProcess;
    _pid=processInfo.dwProcessId;
           
    if ( result )
    {
        // Synchronization with waiting thread,
        WaitForSingleObject(OSMisc::SignalMutex,INFINITE);
        s_numofchildren++;
        // Insert new Process at the end
        s_childprocs[s_numofchildren]=_hProcess;
        s_childpids[s_numofchildren]=_pid;
        // Wake Up the thread
        SetEvent(s_childprocs[0]);

        
      if (hRequestExitEvent == NULL) { 
        char szExitEventName[] = "Process00000000";

        sprintf(szExitEventName, "Process%08X", _pid);
                hRequestExitEvent = CreateEvent(NULL, TRUE, FALSE, szExitEventName);
      }
      ReleaseMutex(OSMisc::SignalMutex);        
      return ((int)_pid);
    } else {
      return FALSE;
    }
}




void
SRBP::UnixProcess::process_died(DWORD pid)
{
 // int status;

	ListProcess::iterator i;
	for (i = _procs.begin(); i != _procs.end(); ++i) {
	    if (pid == (*i)->_pid)
        {
            DWORD exitcode;


            GetExitCodeProcess( (*i)->_hProcess, &exitcode);
            // cannot be set to exitcode which could be -1, if mains returns with -1
            (*i)->_exit_status = 0; // exitcode;
            // Avoid zombies
            CloseHandle((*i)->_hProcess);
            
            if ((*i)->_cb)
            {
            	/*TODO truevoid
                COMM::SRB_ptr srb = COMM::SRB_instance("srbp-local-srb");
                COMM::Dispatcher *disp = srb->dispatcher();
                disp->remove (*i, COMM::Dispatcher::Timer);
		 disp->tm_event (*i, 0);
		 */
            }
            
        } 
    }
}



void 
SRBP::UnixProcess::_init() 
{
  HANDLE hProcessExitEvent = NULL;
  char  szExitEventName[] = "Process00000000";
  HANDLE hExitThread = NULL;
  unsigned int ExitThreadID = 0;

  sprintf(szExitEventName, "Process%08X", GetCurrentProcessId());
  hProcessExitEvent = OpenEvent(SYNCHRONIZE, TRUE, szExitEventName);
  if (hProcessExitEvent != NULL) 
  { // we have been started by SRBP::UnixProcess::run()
    CloseHandle(hProcessExitEvent);
    // Start the thread waiting for process termination
#if !defined(__MINGW32__)
    hExitThread = (HANDLE)_beginthreadex(NULL, 0, ThreadExitFunc, NULL, 0, &ExitThreadID);
#else
    hExitThread = (HANDLE)_beginthread(ThreadExitFunc, 0, NULL);
#endif
    if (hExitThread != NULL) 
	{
      CloseHandle(hExitThread);
    }
  }
  return;
}


void
SRBP::UnixProcess::win32_process_init() {
  unsigned int	threadid = 0;

  s_stop_waiting = FALSE;
  s_childprocs[0] = CreateEvent(NULL, FALSE, FALSE, NULL);
  //XXX TODO kill the thread when shutting down
#ifndef __MINGW32__
  s_waitthread = (HANDLE)_beginthreadex(NULL, 0, wait_thread_func, NULL, 0, &threadid);
#else
  s_waitthread = (HANDLE)_beginthread(wait_thread_func, 0, NULL);
#endif
  if (s_waitthread != NULL) {
    CloseHandle(s_waitthread);
  }
  return;
}


#if !defined(__MINGW32__)
unsigned int
#else
void
#endif
__stdcall SRBP::UnixProcess::ThreadExitFunc(VOID *arg) {
  HANDLE hProcessExitEvent = NULL;
  char szExitEventName[] = "Process00000000";

  sprintf(szExitEventName, "Process%08X", GetCurrentProcessId());
  hProcessExitEvent = OpenEvent(SYNCHRONIZE, TRUE, szExitEventName);
  if (hProcessExitEvent != NULL) {
    WaitForSingleObject(hProcessExitEvent, INFINITE);
    ResetEvent(hProcessExitEvent);
    /*TODO truevoid
  COMM::SRB_ptr srb = COMM::SRB_instance("srbp-local-srb");
  srb->shutdown(FALSE);
  srb->perform_work();
  */
  }
#if !defined(__MINGW32__)
  return 0;
#endif
}


#if !defined(__MINGW32__)
unsigned int
#else
void
#endif
__stdcall SRBP::UnixProcess::wait_thread_func (VOID *arg)
{
    while(9*6)
    {
        DWORD rc = WaitForMultipleObjects (s_numofchildren+1, s_childprocs,
                                           FALSE,INFINITE);
        if (rc == WAIT_TIMEOUT)
            break;			// Exiting
//  Will be set when shutdown is requested        
//        if (s_stop_waiting)
//            break;
        
        rc -= WAIT_OBJECT_0;
        // if rc==0 it is a wakeup call to update
        // the list of child procs
        if (rc-- != 0)
        {
            // move last proc to the position of terminated proc
            // could be the same 

            WaitForSingleObject(OSMisc::SignalMutex,INFINITE);

            int pid=s_childpids[rc+1];

            s_childprocs[rc+1]=s_childprocs[s_numofchildren];
            s_childpids[rc+1]=s_childpids[s_numofchildren];
            s_numofchildren--;
            // Call the "signal_handler"
            (void)process_died (pid);
            
            ReleaseMutex(OSMisc::SignalMutex);

        }        
    }
#if !defined(__MINGW32__)
    return 0;
#endif
}

COMM::Boolean
SRBP::UnixProcess::exited ()
{
    return _pid == 0 || _exit_status >= 0;
}

COMM::Boolean
SRBP::UnixProcess::exit_status ()
{
    return _exit_status == 0;
}

void
    SRBP::UnixProcess::terminate() {

    SetEvent(hRequestExitEvent);
    return;
}


void
SRBP::UnixProcess::detach ()
{
    _detached = TRUE;
    _cb = 0;
}

SRBP::UnixProcess::operator COMM::Boolean ()
{
    return _pid > 0;
}


void
SRBP::UnixProcess::callback (COMM::Dispatcher *disp,
			     COMM::DispatcherCallback::Event ev)
{
    if (ev == COMM::Dispatcher::Timer && _cb)
       _cb->callback (this, SRBP::ProcessCallback::Exited);
}


/*************************** UnixSharedLib ****************************/


SRBP::UnixSharedLib::UnixSharedLib (const char *name)
{
    _name = name;
    _handle = ::LoadLibrary(name);
}

SRBP::UnixSharedLib::~UnixSharedLib ()
{
    if (_handle != NULL)
    {
	::FreeLibrary((HINSTANCE)_handle);
    }
}

void *
SRBP::UnixSharedLib::symbol (const char *sym)
{
    return ::GetProcAddress((HINSTANCE)_handle,sym);
}

const char *
SRBP::UnixSharedLib::error ()
{
  LPVOID lpMsgBuf = NULL; //Here is the Pointer to Error String.
  ::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		  FORMAT_MESSAGE_FROM_SYSTEM | 
		  FORMAT_MESSAGE_IGNORE_INSERTS,
		  NULL,
		  ::GetLastError(),
		  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		  (LPTSTR) &lpMsgBuf,
		  0,
		  NULL    
		  );
  const char *err = (const char*)lpMsgBuf;
  if (err)
    _error = err;
  ::LocalFree(lpMsgBuf);
  return _error.c_str();
}

SRBP::UnixSharedLib::operator COMM::Boolean ()
{
    return !!_handle;
}

const char *
SRBP::UnixSharedLib::name ()
{
    return _name.c_str();
}

