
#include <COMM.h>

#ifndef _WINDOWS
#include <string.h>
#endif
#ifdef HAVE_ANSI_CPLUSPLUS_HEADERS
#include <iostream>
#include <fstream>
#else
#include <iostream.h>
#include <fstream.h>
#endif
#include <srbp/os-net.h>
#include <srbp/impl.h>
#include <srbp/template_impl.h>
#include <srbp/util.h>
#ifdef _WINDOWS
#include <srbp/process_impl.h>
#endif
#include <algorithm>

#ifdef _WINDOWS
CRITICAL_SECTION gisp_conn_mut;
CRITICAL_SECTION handle_mut;
CRITICAL_SECTION iisp_p_ir_mut;
CRITICAL_SECTION iisp_s_ir_mut;
CRITICAL_SECTION job_q_mut;
CRITICAL_SECTION msgid_mut;
CRITICAL_SECTION so_mut;
CRITICAL_SECTION srb_ir_mut;
CRITICAL_SECTION thread_count_mut;
HANDLE job_sem;
#else
#include <pthread.h>
#include <semaphore.h>
pthread_mutex_t gisp_conn_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t handle_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t iisp_p_ir_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t iisp_s_ir_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t job_q_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t msgid_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t so_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t srb_ir_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t thread_count_mut = PTHREAD_MUTEX_INITIALIZER;
sem_t job_sem;
#endif

int thread_pool_size = 20;
int waiting_thread_count = 0;

/************************** misc dtors ****************************/


COMM::ServiceAdapter::~ServiceAdapter ()
{
}

COMM::SRBCallback::~SRBCallback ()
{
}


/************************** SRBRequest ****************************/


COMM::SRBRequest::~SRBRequest ()
{
}


/************************* SRBInvokeRecord ***************************/


COMM::SRBInvokeRec::SRBInvokeRec () 
{
	_have_result = FALSE;
	_req = 0;
}


COMM::SRBInvokeRec::~SRBInvokeRec ()
{
#if 0
#ifdef _WINDOWS 
   	EnterCriticalSection(&wait_count_mut);
#else
	pthread_mutex_lock(&wait_count_mut);
#endif
	
	if (!_have_result && waiting_thread_count > 0)
	{
#ifdef _WINDOWS
		ReleaseSemaphore(wait_result_sem, waiting_thread_count, NULL);
#else
        pthread_cond_broadcast(&select_cond);
#endif
	}
	
#ifdef _WINDOWS 
   	LeaveCriticalSection(&wait_count_mut);
#else
	pthread_mutex_unlock(&wait_count_mut);
#endif
#endif
}

void
COMM::SRBInvokeRec::free () 
{
#if 0
	COMM::release (_req);
	
#ifdef _WINDOWS 
   	EnterCriticalSection(&wait_count_mut);
#else
	pthread_mutex_lock(&wait_count_mut);
#endif
	
	if (!_have_result && waiting_thread_count > 0) 
	{
#ifdef _WINDOWS
		ReleaseSemaphore(wait_result_sem, waiting_thread_count, NULL);
#else
		pthread_cond_broadcast(&select_cond);
#endif
	}
	
#ifdef _WINDOWS 
   	LeaveCriticalSection(&wait_count_mut);
#else
	pthread_mutex_unlock(&wait_count_mut);
#endif
#endif
	
	_have_result = FALSE;
	_req = 0;
}

void
COMM::SRBInvokeRec::init_invoke (SRB_ptr srb, MsgId id, SRBRequest *r, Boolean response, SRBCallback *callback,ServiceAdapter *sa)
{
	_have_result = FALSE;
	_srb = srb;
	_myid = id;
	_req = r;
	_response_expected = response;
	_adapter = sa;
	_cb = callback;
}

void
COMM::SRBInvokeRec::set_answer_invoke (InvokeStatus state, SRBRequest *r) 
{
	assert (!_have_result);
	
	_invoke_stat = state;
	
	switch (state) 
	{
	case InvokeOk:
	case InvokeUsrEx:
	case InvokeSysEx:
		if (!_req->copy_out_data (r)) 
		{
		/*TODO
		COMM::MARSHAL ex;
		_req->set_out_args (&ex);
		_invoke_stat = InvokeSysEx;
			*/
		}
		break;
		
	default:
		assert (0);
	}
	
	_have_result = TRUE;
	
#if 0
#ifdef _WINDOWS 
   	EnterCriticalSection(&wait_count_mut);
#else
	pthread_mutex_lock(&wait_count_mut);
#endif
	
	if (waiting_thread_count > 0) 
	{
#ifdef _WINDOWS
		ReleaseSemaphore(wait_result_sem, waiting_thread_count, NULL);
#else
		pthread_cond_broadcast(&select_cond);
#endif
	}
	
#ifdef _WINDOWS 
   	LeaveCriticalSection(&wait_count_mut);
#else
	pthread_mutex_unlock(&wait_count_mut);
#endif
#endif
}

COMM::Boolean
COMM::SRBInvokeRec::get_answer_invoke (InvokeStatus &state, SRBRequest *&r) 
{
	if (!_have_result)
		return FALSE;
	
	state = _invoke_stat;
	r = _req;
	return TRUE;
}


/**************************** SRB *********************************/


static COMM::SRB_ptr srb_instance;
static SRBP::IISPProxy *iisp_proxy_instance = 0;
static SRBP::IISPServer *iisp_server_instance = 0;
static SRBP::PSA *psa_instance = 0;

COMM::SRB::SRB (int &argc, char **argv) 
{
	_disp = new SRBP::SelectDispatcher ();
	
	_is_running = FALSE;
	_is_stopped = TRUE;
	_is_shutdown = FALSE;
	_wait_for_completion = FALSE;
	
	_cache_used = FALSE;
	_cache_rec = new SRBInvokeRec;
	
	thread_count = 0;
	idle_thread_count = 0;
    
	init_job_queue ();	//������ list �ʱ�ȭ 
	
#ifdef _WINDOWS
	InitializeCriticalSection (&gisp_conn_mut);
	InitializeCriticalSection (&handle_mut);
	InitializeCriticalSection (&iisp_p_ir_mut);
	InitializeCriticalSection (&iisp_s_ir_mut);
	InitializeCriticalSection (&job_q_mut);
	InitializeCriticalSection (&msgid_mut);
	InitializeCriticalSection (&so_mut);
	InitializeCriticalSection (&srb_ir_mut);
	InitializeCriticalSection (&thread_count_mut);
#endif
}

COMM::SRB::~SRB () {
	delete _cache_rec;
	delete _disp;
	
	map<MsgId, SRBInvokeRec *, less<MsgId> >::iterator i;
	for(i = _invokes.begin () ; i != _invokes.end () ; ++i)
		delete (*i).second;
}

COMM::SRBConn *
COMM::SRB::create_connection (const char *host, COMM::UShort port) {
	return iisp_proxy_instance->make_conn (new SRBP::InetAddress(host, port));
}

void
COMM::SRB::close_connection (SRBConn *conn) {
	iisp_proxy_instance->kill_conn ((SRBP::GISPConn *)conn);
}

COMM::Boolean
COMM::SRB::is_local (const char *svcname) {
	return psa_instance->has_service (svcname) ? TRUE : FALSE;
}

COMM::ServiceAdapter *
COMM::SRB::get_sa (const char *svcname) 
{
	Boolean local = is_local (svcname);
	for (ULong i = 0; i < _adapters.size(); ++i) 
	{
		if (_adapters[i]->is_local() == local && _adapters[i]->has_service (svcname))  //PSA�� ���� 
			return _adapters[i];
	}
	return NULL;
}

COMM::SRBInvokeRec *
COMM::SRB::create_invoke () {
#ifdef _WINDOWS
	EnterCriticalSection(&srb_ir_mut);
#else
	pthread_mutex_lock(&srb_ir_mut);
#endif
	if (!_cache_used) {
		_cache_used = TRUE;
#ifdef _WINDOWS
		LeaveCriticalSection(&srb_ir_mut);
#else
		pthread_mutex_unlock(&srb_ir_mut);
#endif
		return _cache_rec;
	}
    
	COMM::SRBInvokeRec *ret = new SRBInvokeRec;
	
#ifdef _WINDOWS
	LeaveCriticalSection(&srb_ir_mut);
#else
	pthread_mutex_unlock(&srb_ir_mut);
#endif
	return ret;
}

void
COMM::SRB::add_invoke (SRBInvokeRec *rec) {
#ifdef _WINDOWS
	EnterCriticalSection(&srb_ir_mut);
#else
	pthread_mutex_lock(&srb_ir_mut);
#endif
	if (rec == _cache_rec) {
#ifdef _WINDOWS
		LeaveCriticalSection(&srb_ir_mut);
#else
		pthread_mutex_unlock(&srb_ir_mut);
#endif
		return;
	}
	
	_invokes[rec->id()] = rec;
#ifdef _WINDOWS
	LeaveCriticalSection(&srb_ir_mut);
#else
	pthread_mutex_unlock(&srb_ir_mut);
#endif
}

COMM::SRBInvokeRec *
COMM::SRB::get_invoke (MsgId id) 
{
#ifdef _WINDOWS
	EnterCriticalSection(&srb_ir_mut);
#else
	pthread_mutex_lock(&srb_ir_mut);
#endif

	if (_cache_used && _cache_rec->id() == id) 
	{

#ifdef _WINDOWS
		LeaveCriticalSection(&srb_ir_mut);
#else
		pthread_mutex_unlock(&srb_ir_mut);
#endif

		return _cache_rec;
	}
	map<MsgId, SRBInvokeRec *, less<MsgId> >::iterator i;
	i = _invokes.find (id);
	if (i == _invokes.end()) 
	{

#ifdef _WINDOWS
		LeaveCriticalSection(&srb_ir_mut);
#else
		pthread_mutex_unlock(&srb_ir_mut);
#endif
		return NULL;
	}
	
#ifdef _WINDOWS
	LeaveCriticalSection(&srb_ir_mut);
#else
	pthread_mutex_unlock(&srb_ir_mut);
#endif
	return (*i).second;
}

void
COMM::SRB::del_invoke (MsgId id) {
#ifdef _WINDOWS
	EnterCriticalSection(&srb_ir_mut);
#else
	pthread_mutex_lock(&srb_ir_mut);
#endif
	if (_cache_used && _cache_rec->id() == id) {
		_cache_rec->free();
		_cache_used = FALSE;
#ifdef _WINDOWS
		LeaveCriticalSection(&srb_ir_mut);
#else
		pthread_mutex_unlock(&srb_ir_mut);
#endif
		return;
	}
	
	map<MsgId, SRBInvokeRec *, less<MsgId> >::iterator i;
	i = _invokes.find (id);
	if (i != _invokes.end()) {
		delete (*i).second;
		_invokes.erase (i);
	}
#ifdef _WINDOWS
	LeaveCriticalSection(&srb_ir_mut);
#else
	pthread_mutex_unlock(&srb_ir_mut);
#endif
}

void
COMM::SRB::do_shutdown () {
	if (_is_shutdown)
		return;
	
	_is_shutdown = TRUE;
	
	if (_wait_for_completion)
		_shutting_down_adapters = _adapters;
	
	SAVec adapters_copy = _adapters;
	for (ULong i = 0; i < adapters_copy.size(); ++i) {
		adapters_copy[i]->shutdown (_wait_for_completion);
	}
	
	if (_wait_for_completion) {
		while (_shutting_down_adapters.size() > 0)
			_disp->run (FALSE);
	}
}

void
COMM::SRB::run () 
{
	assert (!_is_running);
	
	_is_running = TRUE;
	_is_stopped = FALSE;
	
	while (!_is_stopped) 
	{
		_disp->run (FALSE);
	}
	
	do_shutdown ();
}

void
COMM::SRB::answer_shutdown (ServiceAdapter *sa) {
	for (ULong i = 0; i < _shutting_down_adapters.size(); ++i) {
		if (_shutting_down_adapters[i] == sa) {
			_shutting_down_adapters.erase (
				_shutting_down_adapters.begin()+i);
			break;
		}
	}
}

void
COMM::SRB::shutdown (Boolean wait_for_completion) {
	_wait_for_completion = wait_for_completion;
	_is_stopped = TRUE;
	
	if(!_is_running)
		do_shutdown ();
}

void
COMM::SRB::destroy () {
	if (!_is_stopped)
		shutdown (TRUE);
	
	if (SRBP::Logger::IsLogged (SRBP::Logger::Info)) {
		SRBP::Logger::Stream (SRBP::Logger::Info)
			<< "SRB::shutdown : srb_instance->_refcnt() =  " 
			<< srb_instance->_refcnt() << endl;
	}
	
	COMM::release(srb_instance);
	srb_instance = _nil();
}

void
COMM::SRB::register_sa (ServiceAdapter *sa)
{
	_adapters.push_back (sa);			
}

void
COMM::SRB::unregister_sa (ServiceAdapter *sa) {
	for (ULong i = 0; i < _adapters.size(); ) {
		if (_adapters[i] == sa)
			_adapters.erase (_adapters.begin() + i);
		else
			++i;
	}
}

void
COMM::SRB::register_service (const char *svcname, SRBService *svc) 
{
	psa_instance->add_service (svcname, svc);  //PSA�� ���� 
}

void
COMM::SRB::unregister_service (const char *svcname) {
	psa_instance->del_service (svcname);
}

COMM::SRB::MsgId
COMM::SRB::new_msgid () {
#ifdef _WINDOWS
	EnterCriticalSection(&msgid_mut);
#else
	pthread_mutex_lock(&msgid_mut);
#endif
	if (_theid == 0)
		_theid = 1;
	COMM::SRB::MsgId ret = _theid++;
#ifdef _WINDOWS
	LeaveCriticalSection(&msgid_mut);
#else
	pthread_mutex_unlock(&msgid_mut);
#endif
	return ret;
}

/************ Job Queue *************/

void 
COMM::SRB::init_job_queue() 
{
	head = (job_node*)malloc(sizeof(job_node));
    
	if (head == NULL) 
	{
		printf("(E) Malloc Fail !! \n");
		assert(0);
	}
	
	tail = (job_node*)malloc(sizeof(job_node));
	if (tail == NULL) 
	{
		printf("(E) Malloc Fail !! \n");
		assert(0);
	}
	
	head->next = tail;
	tail->next = tail;
	insert_ptr = head;
	
	job_count = 0;
	
#ifdef _WINDOWS
	job_sem = CreateSemaphore(NULL, 0, 1000000, NULL);
#else
	sem_init(&job_sem, 0, 0);
#endif
}

int
COMM::SRB::get_job(job_node *job) {
#ifdef _WINDOWS
	EnterCriticalSection(&job_q_mut);
#else
	pthread_mutex_lock(&job_q_mut);
#endif
	
	if (job_count <= 0) {
#ifdef _WINDOWS
		LeaveCriticalSection(&job_q_mut);
#else
		pthread_mutex_unlock(&job_q_mut);
#endif   
		return -1;
	}
	
	job_node *front_job = head->next;
	memcpy(job, front_job, sizeof(job_node));
	head->next = front_job->next;
    
	if (insert_ptr == front_job)
		insert_ptr = head;
	
	free(front_job);
    
	job_count--;
	
#ifdef _WINDOWS
	LeaveCriticalSection(&job_q_mut);
#else
	pthread_mutex_unlock(&job_q_mut);
#endif
	
	return 0;
}

void
COMM::SRB::add_job(job_node *job) 
{
#ifdef _WINDOWS
	EnterCriticalSection(&job_q_mut);
#else
	pthread_mutex_lock(&job_q_mut);
#endif
	
	job_node *new_job;
	
	new_job = (job_node*)malloc(sizeof(job_node));
	if (new_job == NULL)
	{
		printf("(E) Malloc Fail !! \n");
		assert(0);
	}	
	
	job_count++;
	
	memcpy(new_job, job, sizeof(job_node));
	new_job->next = tail;
	insert_ptr->next = new_job;
	insert_ptr = new_job;
	
#ifdef _WINDOWS
	ReleaseSemaphore(job_sem, 1, NULL);
#else
	sem_post(&job_sem);
#endif
	
#ifdef _WINDOWS
	LeaveCriticalSection(&job_q_mut);
#else
	pthread_mutex_unlock(&job_q_mut);
#endif
}

#ifdef _WINDOWS

void
COMM::SRB::invoke_thread_win (void *args_in) 
{
	SRB *srb = (SRB *)args_in;
	
	EnterCriticalSection(&thread_count_mut);
	srb->thread_count++;
	srb->idle_thread_count++;
	LeaveCriticalSection(&thread_count_mut);
	
	job_node job;
	
	while(1) 
	{
		while (srb->get_job(&job) < 0) 
			WaitForSingleObject(job_sem, INFINITE);
		
		EnterCriticalSection(&thread_count_mut);
		srb->idle_thread_count--;
		LeaveCriticalSection(&thread_count_mut);
		
		//EnterCriticalSection(&handle_mut);
		
		job.sa->invoke (job.msgid, job.req, job.response_exp);
		COMM::release(job.req);
		
		//LeaveCriticalSection(&handle_mut);
		
		EnterCriticalSection(&thread_count_mut);
		srb->idle_thread_count++;
		LeaveCriticalSection(&thread_count_mut);
        
		if (srb->thread_count > thread_pool_size)
			break;
	}
    
	EnterCriticalSection(&thread_count_mut);
	srb->thread_count--;
	srb->idle_thread_count--;
	LeaveCriticalSection(&thread_count_mut);
	
	_endthread();
}

#else

void *
COMM::SRB::invoke_thread (void *args_in) 
{
	SRB *srb = (SRB *)args_in;
	
	pthread_mutex_lock(&thread_count_mut);
	srb->thread_count++;
	srb->idle_thread_count++;
	pthread_mutex_unlock(&thread_count_mut);
	
	job_node job;
	
	while(1) {
		while (srb->get_job(&job) < 0) { 
			sem_wait(&job_sem);
		}
		
		pthread_mutex_lock(&thread_count_mut);
		srb->idle_thread_count--;
		pthread_mutex_unlock(&thread_count_mut);
		
		//pthread_mutex_lock(&handle_mut);
		
		job.sa->invoke (job.msgid, job.req, job.response_exp);
		COMM::release(job.req);
		
		//pthread_mutex_unlock(&handle_mut);
		
		pthread_mutex_lock(&thread_count_mut);
		srb->idle_thread_count++;
		pthread_mutex_unlock(&thread_count_mut);
        
		if (srb->thread_count > thread_pool_size)
			break;
	}
    
	pthread_mutex_lock(&thread_count_mut);
	srb->thread_count--;
	srb->idle_thread_count--;
	pthread_mutex_unlock(&thread_count_mut);
	
	return (void*)0;
}

#endif

COMM::SRB::MsgId
COMM::SRB::invoke_async (SRBRequest *req, Boolean response_exp, SRBCallback *cb, MsgId msgid) 
{
	if (msgid == 0)
		msgid = new_msgid();
	
	SRBInvokeRec *rec = 0;
	if (response_exp) 
	{
		rec = create_invoke();
		rec->init_invoke (this, msgid, req, response_exp, cb);
		add_invoke (rec);
	}
	
	ServiceAdapter *sa = get_sa (req->svc_name()); //PSA�� ����
	if (!sa) 
	{
		req->set_sys_exception (COMM::SERVICE_NOT_EXIST, req->svc_name ());
		answer_invoke (msgid, InvokeSysEx, req);
		return msgid;
	}
	
	if (FALSE)
	{
		sa->invoke (msgid, req, response_exp);
	}
	else
	{
		job_node job;
		job.sa = sa;	//servie PSA
		job.msgid = msgid;
		job.req = SRBRequest::_duplicate (req);
		job.response_exp = response_exp;
		
		add_job (&job); //PSA ���� 
		
		if (thread_count < thread_pool_size && idle_thread_count <= 0)
		{
			assert(idle_thread_count == 0);
			
#ifdef _WINDOWS
			if (_beginthread(invoke_thread_win, 0, this) <= 0)
				printf("(E) Thread Create Error \n");
#else
			pthread_t id;
			
			if (pthread_create(&id, NULL, invoke_thread, this) != 0)
				printf("(E) Thread Create Error \n");
			pthread_detach(id);
#endif
		}
	}
	
	return response_exp ? msgid : 0;
}

COMM::InvokeStatus
COMM::SRB::invoke (SRBRequest *req, Boolean reply_exp)
{
	MsgId id = invoke_async (req, reply_exp);
	if (!reply_exp)
		return InvokeOk;
	assert (id != 0);
	COMM::Boolean r = wait (id);
	assert (r);
	SRBRequest *dummy;
	return get_invoke_reply (id, dummy);
}

void
COMM::SRB::cancel (MsgId id) {
	SRBInvokeRec *rec = get_invoke (id);
	if (rec) {
	/*TODO
	if (rec->sa())
	rec->sa()->cancel (id);
		*/
		del_invoke (id);
	}
}

COMM::Boolean
COMM::SRB::wait (MsgId id, Long tmout)
 {
	SRBInvokeRec *rec = get_invoke (id);
	if (tmout == 0)
	{
		if (!rec || rec->completed()) 
		{
			return TRUE;
		}
	}
	
	Timeout t (_disp, tmout);
	
	while (42) 
	{
		if (!rec || rec->completed())
			return TRUE;
		if (t.done())
			return FALSE;
		
		if (!_is_running) 
		{
			_disp->run (FALSE);
		}
		else
		{
#if 0
#ifdef _WINDOWS 
			EnterCriticalSection(&wait_count_mut);
#else
			pthread_mutex_lock(&wait_count_mut);
#endif
			waiting_thread_count++;
#ifdef _WINDOWS
			LeaveCriticalSection(&wait_count_mut);
			WaitForSingleObject(wait_result_sem, INFINITE);
			EnterCriticalSection(&wait_count_mut);
#else
			pthread_cond_wait(&select_cond, &wait_count_mut);
#endif
			waiting_thread_count--;
#ifdef _WINDOWS
			LeaveCriticalSection(&wait_count_mut);
#else
			pthread_mutex_unlock(&wait_count_mut);
#endif
#endif
		}
		rec = get_invoke (id);
	}
}

int
COMM::SRB::answer_invoke (MsgId id, InvokeStatus stat, SRBRequest *req) {
	int ret = 1;
	SRBInvokeRec *rec = get_invoke (id);
	if (rec) {
		ret = 0;
		rec->set_answer_invoke (stat, req);
		if (rec->callback())
			rec->callback()->callback (this, rec->id(), 
			SRBCallback::Invoke);
	}
	return ret;
}

COMM::InvokeStatus
COMM::SRB::get_invoke_reply (MsgId id, SRBRequest *&r) {
	SRBInvokeRec *rec = get_invoke (id);
	assert (rec);
	
	InvokeStatus state;
	COMM::Boolean ret = rec->get_answer_invoke (state, r);
	assert (ret);
	del_invoke (id);
	
	return state;
}

#ifndef _WINDOWS
void handler(int sig, siginfo_t *siginfo, void *dummy) { }
#endif

COMM::SRB_ptr
COMM::SRB_init (int &argc, char **argv) 
{
#ifndef _WINDOWS
	struct sigaction sact;
	
	sact.sa_sigaction = handler;
	sact.sa_flags = SA_SIGINFO;
	sigemptyset(&sact.sa_mask);
	
	if(sigaction(SIGUSR1, &sact, NULL) < 0)
		printf("Sigaction Error...\n");
#else
#endif
	
	OSNet::sock_init ();  //WSAStartup() INIT
	
#ifdef _WINDOWS
	OSMisc::_init();	  //SignalMutex Creation
	SRBP::UnixProcess::_init();
#endif
	
	Boolean run_iisp_server = TRUE;  //TRUE : Server ����  FALSE : Serve ���� X
	Boolean run_iisp_proxy = TRUE;	 //TRUE : Client ����  FALSE : Client ���� X
	Boolean iisp_blocking = FALSE;	 //TRUE : Block socket FALSE : nonBlock socket
	vector<string> debug_level;
	vector<string> iispaddrs;
	COMM::UShort gisp_ver, iisp_ver;
	string gisp_ver_str = "1.0";
	string iisp_ver_str = "1.0";
	
	
	/************************************************************************************************************************************************/
	/*														Argument Parsing, option Setting														*/
	/*																																				*/
	/************************************************************************************************************************************************/
	
	SRBPGetOpt::OptMap opts;		// Ȯ���Ϸ��� opts�� option factor�� �߰��Ѵ�. 
    
	opts["-SRBNoIISPServer"]	= "arg-expected";
	opts["-SRBNoIISPProxy"]		= "arg-expected";
	opts["-SRBIISPAddr"]		= "arg-expected";
	opts["-SRBIISPBlocking"]	= "arg-expected";
	opts["-SRBDebug"]           = "arg-expected";
	opts["-SRBThreadPoolSize"]  = "arg-expected";
	SRBPGetOpt opt_parser (opts);
	if (!opt_parser.parse (argc, argv, TRUE)) 
	{
		exit(1);
	}
	const SRBPGetOpt::OptVec &o = opt_parser.opts (); // parsing�� Option�� ������.
	
	for (SRBPGetOpt::OptVec::const_iterator i = o.begin(); i != o.end(); ++i) 
	{
		string arg = (*i).first;	//option key
		string val = (*i).second;	//value
		if (arg == "-SRBNoIISPServer")
		{
			run_iisp_server = FALSE;
		} 
		else if (arg == "-SRBNoIISPProxy") 
		{
			run_iisp_proxy = FALSE;
		} 
		else if (arg == "-SRBIISPAddr") 
		{
			iispaddrs.push_back (val);	//server�� ���� Setting
		}
		else if (arg == "-SRBIISPBlocking") 
		{
			iisp_blocking = TRUE;
		} 
		else if (arg == "-SRBDebug") 
		{
			debug_level.push_back (val);
		} 
		else if (arg == "-SRBGISPVersion") 
		{
			//gisp_ver_str = val;
		} 
		else if (arg == "-SRBIISPVersion")
		{
			//iisp_ver_str = val;
		} 
		else if (arg == "-SRBThreadPoolSize") 
		{
			thread_pool_size =  atoi(val.c_str());
		}
	}
	
	// setup debugging
	for (COMM::ULong debugs=0; debugs<debug_level.size(); debugs++) 
	{
		size_t pos = debug_level[debugs].find ('=');
		string names, file;
		const char * fname;
		
		if (pos != (size_t) -1)  //-1�� unsigned int�� ����ȯ ffff
		{
			names = debug_level[debugs].substr (0, pos);
			file  = debug_level[debugs].substr (pos+1, (size_t) -1);
			fname = file.c_str();
		} 
		else 
		{
			names = debug_level[debugs];	//option Level�� ���.
			fname = 0;
		}
		
		while (names.length() > 0)
		{
			string name;
			if ((pos = names.find (',')) == (size_t) -1)  //�������� command�� ���� ���� Parsing
			{
				name = names;
				names = "";
			} 
			else
			{
				name  = names.substr (0, pos);
				names = names.substr (pos+1);
			}
			
			SRBP::Logger::Log (name.c_str(), 1, fname);	//Log Level Setting 
		}
	}
	
	
	
	/************************************************************************************************************************************************/
	/*																SRB CREATION 																    */
	/*																																				*/
	/************************************************************************************************************************************************/
	
	srb_instance = new SRB (argc, argv);
	
	gisp_ver = ((COMM::UShort)(gisp_ver_str[0] - '0') << 8) |(gisp_ver_str[2] - '0');
	iisp_ver = ((COMM::UShort)(iisp_ver_str[0] - '0') << 8) |(iisp_ver_str[2] - '0');
	
	COMM::ULong max_message_size = 1024*1024;
	

	/************************************************************************************************************************************************/
	/*																create IISP client 																*/
	/*																																				*/
	/************************************************************************************************************************************************/

	SRBP::IISPProxy::block (iisp_blocking);
	if (run_iisp_proxy)							//default value :: run_iisp_proxy = true; run_iisp_server = true; iisp_blocking = false;
	{
		iisp_proxy_instance = new SRBP::IISPProxy (srb_instance, gisp_ver,max_message_size);
	}
	

	/************************************************************************************************************************************************/
	/*																create IISP server 																*/
	/*																																				*/
	/************************************************************************************************************************************************/

	if (run_iisp_server) 
	{
		iisp_server_instance = new SRBP::IISPServer (srb_instance,iisp_ver,max_message_size);

		for (srbp_vec_size_type i = 0; i < iispaddrs.size(); ++i)
		{
			Address *addr = Address::parse (iispaddrs[i].c_str());
			if (!addr)
			{
				if (SRBP::Logger::IsLogged (SRBP::Logger::Error)) 
				{
					SRBP::Logger::Stream (SRBP::Logger::Error)
						<< "Error: SRB_init(): bad address: "
						<< iispaddrs[i] << endl;
				}
				exit (1);
			}
			if (!iisp_server_instance->listen (addr))
			{
				exit (1);
			}
			delete addr;
		}
		if (iispaddrs.size () == 0)
		{
			iisp_server_instance->listen ();
		}
	}
	
	// create PSA instance
	psa_instance = new SRBP::PSA (srb_instance);
	
	return srb_instance;
}

