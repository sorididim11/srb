
#include <COMM.h>
#include <stdio.h>
#include <string.h>
#ifdef HAVE_ANSI_CPLUSPLUS_HEADERS
#include <iostream>
#include <iomanip>
#else
#include <iostream.h>
#include <iomanip.h>
#endif
#include <srbp/template_impl.h>

COMM::Buffer::Buffer (void *b)
{
    // readonly buffer with given contents
    _len = _wptr = 0x7fffffff;
    _rptr = 0;
    _ralignbase = _walignbase = 0;
    _buf = (Octet *)b;
    _readonly = TRUE;
}

COMM::Buffer::Buffer (ULong sz)
{
    // read/write buffer with given initial size
    if (sz < MINSIZE)
        sz = MINSIZE;
    _buf = alloc (sz);
    _len = sz;
    _rptr = _wptr = 0;
    _ralignbase = _walignbase = 0;
    _readonly = FALSE;
}

COMM::Buffer::Buffer (const Buffer &b)
{
    _buf = alloc (b._len);
    memcpy (_buf, b._buf, b._len);
    _len = b._len;
    _rptr = b._rptr;
    _wptr = b._wptr;
    _ralignbase = b._ralignbase;
    _walignbase = b._walignbase;
    _readonly = FALSE;
}

COMM::Buffer::~Buffer ()
{
    if (!_readonly)
        free (_buf);
}

COMM::Buffer &
COMM::Buffer::operator= (const Buffer &b)
{
    if (this != &b) {
	assert (!_readonly && !b._readonly);
	free (_buf);
	_buf = alloc (b._len);
	memcpy (_buf, b._buf, b._len);
	_len = b._len;
	_rptr = b._rptr;
	_wptr = b._wptr;
	_ralignbase = b._ralignbase;
	_walignbase = b._walignbase;
    }
    return *this;
}

COMM::Boolean
COMM::Buffer::operator== (const Buffer &b)
{
#ifdef DEBUG_BUF
    assert (!_readonly && !b._readonly);
#endif
    return length() == b.length() && !memcmp (data(), b.data(), length());
}

COMM::Octet *
COMM::Buffer::alloc (ULong sz)
{
    Octet *b = (Octet *)::malloc (sz);
#ifdef DEBUG_BUF
    assert (b);
#endif
    return b;
}

COMM::Octet *
COMM::Buffer::realloc (Octet *b, ULong osz, ULong nsz)
{
    Octet *nb = (Octet *)::realloc ((void *)b, nsz);
#ifdef DEBUG_BUF
    assert (nb);
#endif
    return nb;
}

void
COMM::Buffer::free (Octet *b)
{
    ::free ((void *)b);
}

void
COMM::Buffer::reset (ULong sz)
{
    _rptr = 0;
    _ralignbase = _walignbase = 0;
    if (!_readonly) {
        _wptr = 0;
        if (sz < MINSIZE)
            sz = MINSIZE;
        if (_len < sz) {
	    free (_buf);
            _buf = alloc (sz);
            _len = sz;
        }
    }
}

void
COMM::Buffer::doresize (ULong needed)
{
#ifdef DEBUG_BUF
    assert (!_readonly);
#endif
    if (_wptr + needed > _len) {
        ULong nlen = (_len < RESIZE_THRESH)
            ? (2*_len)
            : (_len + RESIZE_INCREMENT);
        if (_wptr + needed > nlen)
            nlen = _wptr + needed;
	_buf = realloc (_buf, _len, nlen);
        _len = nlen;
    }
}

COMM::Boolean
COMM::Buffer::peek (void *b, ULong blen)
{
    if (_wptr - _rptr < blen)
        return FALSE;
    memcpy (b, &_buf[_rptr], blen);
    return TRUE;
}

COMM::Boolean
COMM::Buffer::peek (Octet &o)
{
    if (_wptr == _rptr)
        return FALSE;
    o = _buf[_rptr];
    return TRUE;
}

COMM::Boolean
COMM::Buffer::get (Octet &o)
{
    if (_wptr == _rptr)
        return FALSE;
    o = _buf[_rptr++];
    return TRUE;
}

COMM::Boolean
COMM::Buffer::get (void *b, ULong l)
{
    if (_wptr - _rptr < l)
	return FALSE;
    memcpy (b, &_buf[_rptr], l);
    _rptr += l;
    return TRUE;
}

COMM::Boolean
COMM::Buffer::get1 (void *p)
{
    if (_wptr == _rptr)
        return FALSE;
    *(Octet *)p = _buf[_rptr++];
    return TRUE;
}

COMM::Boolean
COMM::Buffer::get2 (void *p)
{
    // assert (((_rptr - _ralignbase) % 2) == 0);
    if (_rptr+2 > _wptr)
	return FALSE;
    // assume that pointers can be cast to long
    if (!((_rptr | (long)p)&1)) {
	*(COMM::Short *)p = (COMM::Short &)_buf[_rptr];
	_rptr += 2;
    } else {
	*((Octet * &)p)++ = _buf[_rptr++];
	*(Octet *)p = _buf[_rptr++];
    }
    return TRUE;
}

COMM::Boolean
COMM::Buffer::get4 (void *p)
{
    // assert (((_rptr - _ralignbase) % 4) == 0);
    if (_rptr+4 > _wptr)
		return FALSE;
	
    if (!((_rptr | (long)p)&3))
	{
		*(COMM::Long *)p = (COMM::Long &)_buf[_rptr];
		_rptr += 4;
    } 
	else 
	{
		*((Octet * &)p)++ = _buf[_rptr++];
		*((Octet * &)p)++ = _buf[_rptr++];
		*((Octet * &)p)++ = _buf[_rptr++];
		*(Octet *)p = _buf[_rptr++];
    }
    return TRUE;
}

COMM::Boolean
COMM::Buffer::get8 (void *p)
{
    // assert (((_rptr - _ralignbase) % 8) == 0);
    if (_rptr+8 > _wptr)
	return FALSE;
    if (!((_rptr | (long)p)&7)) {
	*(COMM::LongLong *)p = (COMM::LongLong &)_buf[_rptr];
	_rptr += 8;
    } else {
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*(Octet *)p = _buf[_rptr++];
    }
    return TRUE;
}

// get 16 bytes with 8 byte alignment
COMM::Boolean
COMM::Buffer::get16 (void *p)
{
    // assert (((_rptr - _ralignbase) % 8) == 0);
    if (_rptr+16 > _wptr)
	return FALSE;
    if (!((_rptr | (long)p)&7)) {
	*((COMM::LongLong * &)p)++ = (COMM::LongLong &)_buf[_rptr];
	_rptr += 8;
	*(COMM::LongLong *)p = (COMM::LongLong &)_buf[_rptr];
	_rptr += 8;
    } else {
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*((Octet * &)p)++ = _buf[_rptr++];
	*(Octet *)p = _buf[_rptr++];
    }
    return TRUE;
}

void
COMM::Buffer::replace (const void *o, ULong blen)
{
#ifdef DEBUG_BUF
    assert (!_readonly);
#endif
    reset (blen);
    memcpy (&_buf[_wptr], o, blen);
    _wptr += blen;
}

void
COMM::Buffer::replace (Octet o)
{
#ifdef DEBUG_BUF
    assert (!_readonly);
#endif
    reset (1);
    _buf[_wptr++] = o;
}

void
COMM::Buffer::put (const void *o, ULong l)
{
#ifdef DEBUG_BUF
    assert (!_readonly);
#endif
    resize (l);
    memcpy (&_buf[_wptr], o, l);
    _wptr += l;
}

void
COMM::Buffer::put (Octet o)
{
#ifdef DEBUG_BUF
    assert (!_readonly);
#endif
    resize (1);
    _buf[_wptr++] = o;
}

void
COMM::Buffer::put1 (const void *p)
{
    assert (!_readonly);
    resize (1);
    _buf[_wptr++] = *(const Octet *)p;
}

void
COMM::Buffer::put2 (const void *p)
{
#ifdef DEBUG_BUF
    assert (!_readonly && _wptr >= _walignbase);
#endif
    resize (2);
    COMM::Octet *b = _buf + _wptr;
    if (!(((long)b | (long)p)&1)) {
	//*((COMM::Short *&)b)++ = *(const COMM::Short *)p; // AIX Compiler bug
	*((COMM::Short *&)b) = *(const COMM::Short *)p;
	b += 2;
    } else {
	*b++ = *((const Octet * &)p)++;
	*b++ = *(const Octet *)p;
    }
    _wptr = b - _buf;
}

void
COMM::Buffer::put4 (const void *p)
{
#ifdef DEBUG_BUF
    assert (!_readonly && _wptr >= _walignbase);
#endif
    resize (4);
    COMM::Octet *b = _buf + _wptr;
    if (!(((long)b | (long)p)&3)) {
	//*((COMM::Long * &)b)++ = *(const COMM::Long *)p; // AIX Compiler bug
	*((COMM::Long * &)b) = *(const COMM::Long *)p;
	b += 4;
    } else {
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *(const Octet *)p;
    }
    _wptr = b - _buf;
}

void
COMM::Buffer::put8 (const void *p)
{
#ifdef DEBUG_BUF
    assert (!_readonly && _wptr >= _walignbase);
#endif
    resize (8);
    COMM::Octet *b = _buf + _wptr;
    if (!(((long)b | (long)p)&7)) {
	//*((COMM::LongLong *&)b)++ = *(const COMM::LongLong *)p; // AIX Compiler bug
	*((COMM::LongLong *&)b) = *(const COMM::LongLong *)p;
	b += 8;
    } else {
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *(const Octet *)p;
    }
    _wptr = b - _buf;
}

// put 16 bytes with 8 byte alignment
void
COMM::Buffer::put16 (const void *p)
{
#ifdef DEBUG_BUF
    assert (!_readonly && _wptr >= _walignbase);
#endif
    resize (16);
    COMM::Octet *b = _buf + _wptr;
    if (!(((long)b | (long)p)&7)) {
	// AIX Compiler bug
	//*((COMM::LongLong *&)b)++ = *((const COMM::LongLong * &)p)++;
	//*((COMM::LongLong *&)b)++ = *(const COMM::LongLong *)p;
	*((COMM::LongLong *&)b) = *((const COMM::LongLong * &)p);
	b += 8;
	*((COMM::LongLong *&)b) = *((const COMM::LongLong *)p + 8);
	b += 8;
    } else {
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *((const Octet * &)p)++;
	*b++ = *(const Octet *)p;
    }
    _wptr = b - _buf;
}

void
COMM::Buffer::dump (const char * desc, ostream &o) const
{
  COMM::ULong i=_rptr, j;
  char temp[256];
  int l=0;

  while (i < _wptr) {
    sprintf (temp, "%10s  ", (i==_rptr) ? desc : "");
    o << temp;

    for (j=0; j<16 && i+j<_wptr; j++) {
      sprintf (temp, "%02x ", _buf[i+j]);
      o << temp;
    }

    for (; j<16; j++) {
      o << "   ";
    }

    o << " ";

    for (j=0; j<16 && i+j<_wptr; j++) {
      /*
       * Printable ISOLatin1 characters according to the Red Book
       */
      if ((_buf[i+j] >= 0040 && _buf[i+j] <= 0176) ||
	  (_buf[i+j] >= 0220 && _buf[i+j] != 0231 && _buf[i+j] != 0234)) {
	temp[j] = _buf[i+j];
      }
      else {
	temp[j] = '.';
      }
    }

    temp[j] = '\0';
    o << temp << endl;

    i += j;

    if (++l == 16 && i < _wptr) {
      o << endl;
      l = 0;
    }
  }
}
