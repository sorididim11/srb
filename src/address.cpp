
#include <COMM.h>
#ifndef _WINDOWS
#include <string.h>
#endif
#include <srbp/os-net.h>
#include <srbp/impl.h>
#include <srbp/template_impl.h>
#include <srbp/util.h>


/************************* misc dtors *******************************/


COMM::AddressParser::~AddressParser ()
{
}

COMM::Address::~Address ()
{
}


/*************************** Address ********************************/


#ifdef HAVE_NAMESPACE
namespace COMM {
	vector<COMM::AddressParser *> *Address::parsers = 0;
};
#else
vector<COMM::AddressParser *> *COMM::Address::parsers = 0;
#endif

void
COMM::Address::copy (Address *a)
{
}

COMM::Address *
COMM::Address::parse (const char *_a)
{
    if (!parsers)
		return 0;
	
    string rest, proto, a = _a;
	
    Long pos = a.find (":");
    if (pos < 0) 
	{
		proto = a;
		rest = "";
    }
	else //ã�Ҵ�. 
	{
		proto = a.substr (0, pos);
		rest = a.substr (pos+1);
    }
	
    ULong i;
    for (i = 0; i < parsers->size(); ++i) 
	{
		if ((*parsers)[i]->has_proto(proto.c_str()))
			break;
    }
    if (i >= parsers->size())
		return 0;

    Address *addr = (*parsers)[i]->parse (rest.c_str(), proto.c_str());
    return addr;
}

void
COMM::Address::register_parser (AddressParser *ap)
{
    if (!parsers)
		parsers = new vector<COMM::AddressParser *>;
    parsers->push_back (ap);
}

void
COMM::Address::unregister_parser (AddressParser *ap)
{
    if (!parsers)
		return;
    
    for (ULong i = 0; i < parsers->size(); ) {
		if ((*parsers)[i] == ap)
			parsers->erase (parsers->begin() + i);
		else
			++i;
    }
}


/****************************** LocalAddress ****************************/


SRBP::LocalAddress::LocalAddress ()
{
}

SRBP::LocalAddress::~LocalAddress ()
{
}

string
SRBP::LocalAddress::stringify () const
{
    return "local:";
}

const char *
SRBP::LocalAddress::proto () const
{
    return "local";
}

COMM::Transport *
SRBP::LocalAddress::make_transport () const
{
    assert (0);
    return NULL;
}

COMM::TransportServer *
SRBP::LocalAddress::make_transport_server () const
{
    assert (0);
    return NULL;
}
/*
COMM::IORProfile *
SRBP::LocalAddress::make_ior_profile (COMM::Octet *key,
COMM::ULong keylen,
const COMM::MultiComponent &,
COMM::UShort version) const
{
return new LocalProfile (key, keylen);
}
*/
COMM::Boolean
SRBP::LocalAddress::is_local () const
{
    return TRUE;
}

COMM::Address *
SRBP::LocalAddress::clone () const
{
    return new LocalAddress;
}

COMM::Long
SRBP::LocalAddress::compare (const COMM::Address &a) const
{
    return strcmp (proto(), a.proto());
}

COMM::Boolean
SRBP::LocalAddress::operator== (const COMM::Address &a) const
{
    return compare (a) == 0;
}

COMM::Boolean
SRBP::LocalAddress::operator< (const COMM::Address &a) const
{
    return compare (a) < 0;
}


/*************************** LocalAddressParser *************************/


SRBP::LocalAddressParser::LocalAddressParser ()
{
    COMM::Address::register_parser (this);
}

SRBP::LocalAddressParser::~LocalAddressParser ()
{
    COMM::Address::unregister_parser (this);
}

COMM::Address *
SRBP::LocalAddressParser::parse (const char *a, const char *) const
{
    return new LocalAddress;
}

COMM::Boolean
SRBP::LocalAddressParser::has_proto (const char *p) const
{
    return !strcmp ("local", p);
}

static SRBP::LocalAddressParser local_address_parser;


/****************************** InetAddress *****************************/

COMM::Boolean SRBP::InetAddress::_resolve = FALSE;

SRBP::InetAddress::InetAddress (const char *host, COMM::UShort port,Family fam): _port (port), _host (host ? host : ""), _family (fam)
{
}

SRBP::InetAddress::InetAddress (const vector<COMM::Octet> &ip,COMM::UShort port, Family fam): _port (port), _ipaddr (ip), _family (fam)
{
}

SRBP::InetAddress::InetAddress (struct sockaddr_in &sin, Family fam)
: _family (fam)
{
    sockaddr (sin);
}

SRBP::InetAddress::~InetAddress ()
{
}

COMM::Boolean
SRBP::InetAddress::resolve_ip () const
{
    if (_ipaddr.size() > 0)
		return TRUE;
    if (_host.length() == 0)
		return FALSE;
	
    InetAddress &me = (InetAddress &)*this;
	
    COMM::ULong addr = ::inet_addr ((char *)_host.c_str());
    if (addr != (COMM::ULong)-1L || _host == string("255.255.255.255")) 
	{
		me._ipaddr.insert (me._ipaddr.begin(),
			(COMM::Octet *)&addr,
			(COMM::Octet *)&addr + sizeof (COMM::ULong));
		return TRUE;
    }
	
    struct hostent *hent = ::gethostbyname ((char *)_host.c_str());
    if (hent)
	{
        me._ipaddr.insert (me._ipaddr.begin(),(COMM::Octet *)hent->h_addr,(COMM::Octet *)hent->h_addr + hent->h_length);
        return TRUE;
    }
	
    if (SRBP::Logger::IsLogged (SRBP::Logger::Warning)) 
	{
		SRBP::Logger::Stream (SRBP::Logger::Warning)
			<< "Warning: cannot resolve hostname '" << _host
			<< "' into an IP address." << endl;
    }
    return FALSE;
}

COMM::Boolean
SRBP::InetAddress::resolve_host () const
{
    if (_host.length() > 0)
		return TRUE;
    if (_ipaddr.size() == 0)
		return FALSE;
	
    InetAddress &me = (InetAddress &)*this;
	
    if (_resolve) 
	{
		// XXX vector elements need not be continuous ?!
		assert (_ipaddr.size() < 2 || &_ipaddr[0] + 1 == &_ipaddr[1]);
		struct hostent *hent = ::gethostbyaddr ((char *)&_ipaddr.front(),
			_ipaddr.size(), AF_INET);
		if (hent) {
			string s = hent->h_name;
			if ((int)s.find (".") < 0) {
				// official name is not the FQDN. search the alias list ...
				for (int i = 0; hent->h_aliases[i]; ++i) {
					s = hent->h_aliases[i];
					if ((int)s.find (".") >= 0) {
						me._host = s;
						break;
					}
				}
			} else {
				me._host = s;
			}
		}
    }
	
    if (me._host.length() == 0)
	{
		// no FQDN found or no database entry. use dotted decimal ...
		me._host = "";
		for (srbp_vec_size_type i = 0; i < _ipaddr.size(); ++i) 
		{
			if (i > 0)
				me._host += ".";
			me._host += xdec (_ipaddr[i]);
		}
    }
    return TRUE;
}

string
SRBP::InetAddress::stringify () const
{
    string s = proto();
    s += ":";
    COMM::Boolean r = resolve_host ();
	
    if (r) 
	{
		s += _host;
    }
    else
	{
		s += "(oops)";
    }
	
    s += ":";
    s += xdec (_port);
    return s;
}

const char *
SRBP::InetAddress::proto () const
{
    switch (_family) 
	{
    case STREAM:
		return "inet";
    case DGRAM:
		return "inet-dgram";
    default:
		assert (0);
		return 0;
    }
}

COMM::Transport *
SRBP::InetAddress::make_transport () const
{
    switch (_family) {
    case STREAM:
		return new TCPTransport;
    default:
		assert (0);
		return 0;
    }
}

COMM::TransportServer *
SRBP::InetAddress::make_transport_server () const
{
    switch (_family) {
    case STREAM:
		return new TCPTransportServer;
    default:
		assert (0);
		return 0;
    }
}

COMM::Boolean
SRBP::InetAddress::is_local () const
{
    return FALSE;
}

COMM::Address *
SRBP::InetAddress::clone () const
{
    return new InetAddress (*this);
}

COMM::Long
SRBP::InetAddress::compare (const COMM::Address &a) const
{
    COMM::Long r = strcmp (proto(), a.proto());
    if (r)
		return r;
	
    const InetAddress &he = (const InetAddress &)a;
    if (_port != he._port)
        return (COMM::Long)_port - (COMM::Long)he._port;
	
    // need not compare _family, because each family has a different
    // protocol identifier.
	
    COMM::Boolean r1 = resolve_ip();
    COMM::Boolean r2 = he.resolve_ip();
	
    /*
	* avoid assertion here and declare invalid addresses "less" than
	* valid ones
	*/
	
    if (!r1 && !r2) {
		return 0;
    }
    else if (!r1 && r2) {
		return -1;
    }
    else if (r1 && !r2) {
		return 1;
    }
	
    return srbp_vec_compare (_ipaddr, he._ipaddr);
}

COMM::Boolean
SRBP::InetAddress::operator== (const COMM::Address &a) const
{
    return compare (a) == 0;
}

COMM::Boolean
SRBP::InetAddress::operator< (const COMM::Address &a) const
{
    return compare (a) < 0;
}

COMM::Boolean
SRBP::InetAddress::valid () const
{
    return resolve_host() && resolve_ip();
}

const char *
SRBP::InetAddress::host () const
{
    COMM::Boolean r = resolve_host();
    assert (r);
    return _host.c_str();
}

void
SRBP::InetAddress::host (const char *h)
{
    _ipaddr.erase (_ipaddr.begin(), _ipaddr.end());
    _host = h;
}

const vector<COMM::Octet> &
SRBP::InetAddress::ipaddr () const
{
    COMM::Boolean r = resolve_ip ();
    assert (r);
    return _ipaddr;
}

void
SRBP::InetAddress::ipaddr (const vector<COMM::Octet> &ip)
{
    _host = "";
    _ipaddr = ip;
}

COMM::UShort
SRBP::InetAddress::port () const
{
    return _port;
}

void
SRBP::InetAddress::port (COMM::UShort p)
{
    _port = p;
}

SRBP::InetAddress::Family
SRBP::InetAddress::family () const
{
    return _family;
}

void
SRBP::InetAddress::family (Family fam)
{
    _family = fam;
}

struct sockaddr_in
SRBP::InetAddress::sockaddr () const
{
    COMM::Boolean r = resolve_ip();
    assert (r);
	
    struct sockaddr_in sin;
    memset (&sin, 0, sizeof (sin));
	
    sin.sin_family = AF_INET;
    sin.sin_port = htons (_port);
	
    sin.sin_addr.s_addr= inet_addr( "127.0.0.1");
    
    // XXX vector elements need not be continuous ?!
    assert (_ipaddr.size() < 2 || &_ipaddr[0] + 1 == &_ipaddr[1]);
    //assert (_ipaddr.size() == sizeof (sin.sin_addr.s_addr));
    //memcpy (&sin.sin_addr.s_addr, &_ipaddr[0], _ipaddr.size());
    return sin;
}

void
SRBP::InetAddress::sockaddr (struct sockaddr_in &sin)
{
    _ipaddr.erase (_ipaddr.begin(), _ipaddr.end());
    _ipaddr.insert (_ipaddr.begin(),(COMM::Octet *)&sin.sin_addr.s_addr,(COMM::Octet *)(&sin.sin_addr.s_addr+1));
    _port = ntohs (sin.sin_port);
    _host = "";
}

string SRBP::InetAddress::hname;

string
SRBP::InetAddress::hostname ()
{
    if (hname.length() == 0) {
        char buf[200];
        int r = gethostname (buf, 200);
		assert (r == 0);
        /*
		* some OSes do not return an FQDN. So we get the ip address for the
		* hostname and resolve that address into the FQDN...
		*/
        InetAddress a1 (buf, 0);
#ifndef __CYGWIN32__
        InetAddress a2 (a1.ipaddr());
        hname = a2.host();
#else
		/*
		* with CDK beta 19 resolving IP addresses into host names
		* hangs forever (even for this machines' IP address) if there
		* is no name server. therefore we use dotted decimal notation.
		*/
		const vector<COMM::Octet> &ipaddr = a1.ipaddr();
		for (srbp_vec_size_type i = 0; i < ipaddr.size(); ++i) {
			if (i > 0)
				hname += ".";
			hname += xdec (ipaddr[i]);
		}
#endif
    }
    return hname;
}

vector<COMM::Octet> SRBP::InetAddress::hid;

vector<COMM::Octet>
SRBP::InetAddress::hostid ()
{
    if (hid.size() == 0) {
        char buf[200];
        int r = gethostname (buf, 200);
		assert (r == 0);
		
        InetAddress a (buf, 0);
		hid = a.ipaddr();
    }
    return hid;
}

COMM::Boolean
SRBP::InetAddress::samehosts (const string &h1, const string &h2)
{
    // XXX what if h1 is an alias for h2 ???
    return h1 == h2;
}

void
SRBP::InetAddress::resolve (COMM::Boolean r)
{
    _resolve = r;
}

COMM::Boolean
SRBP::InetAddress::resolve ()
{
    return _resolve;
}


/*************************** InetAddressParser *************************/


SRBP::InetAddressParser::InetAddressParser ()
{
    COMM::Address::register_parser (this);
}

SRBP::InetAddressParser::~InetAddressParser ()
{
    COMM::Address::unregister_parser (this);
}

COMM::Address *
SRBP::InetAddressParser::parse (const char *str, const char *proto) const
{
    string s (str);
    COMM::Long pos = s.find (":");
    if (pos < 0)
		return 0;
	
    SRBP::InetAddress::Family family;
    if (!strcmp (proto, "inet") || !strcmp (proto, "inet-stream"))
		family = SRBP::InetAddress::STREAM;
    else if (!strcmp (proto, "inet-dgram"))
		family = SRBP::InetAddress::DGRAM;
    else
		return 0;
	
    string host = s.substr (0, pos);
    string port = s.substr (pos+1);
    COMM::UShort portnum = atoi (port.c_str());
	
    InetAddress *ia;
	
    if (host.length() > 0) 
	{
		ia = new InetAddress (host.c_str(), portnum, family);
    }
    else 
	{
		ia = new InetAddress ("0.0.0.0", portnum, family);
    }
    if (!ia->valid()) 
	{
		delete ia;
		return 0;
    }
    return ia;
}

COMM::Boolean
SRBP::InetAddressParser::has_proto (const char *p) const
{
    return!strcmp ("inet", p) ||!strcmp ("inet-stream", p) ||!strcmp ("inet-dgram", p);
}

static SRBP::InetAddressParser inet_address_parser;
