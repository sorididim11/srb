#include <iostream>

#include <COMM.h>

using namespace std;

class TestStub : public COMM::SRBMarshaller, public COMM::SRBDemarshaller 
{
	COMM::StaticRequest *_req;
	COMM::ULong val;
	COMM::Char str[5];
	COMM::String_var str2;
public:
	TestStub (StaticRequest *);
	void print_values ();
	void marshal (COMM::DataEncoder *);
	COMM::Boolean demarshal (COMM::DataDecoder *);
};

TestStub::TestStub (COMM::StaticRequest *req) 
{
	_req = req;
}

void
TestStub::print_values () 
{
	if (_req->get_exception_type () == COMM::NO_EXCEPTION) 
	{
		cout << "val  = [" << val << "]" << endl;
		cout << "str  = [" << str << "]" << endl;
		cout << "str2 = [" << str2.in() << "]" << endl;
	}
	else 
	{
		cout << "Exception occurred!!" << endl;
		cout << "val  = [" << val << "]" << endl;
		cout << "str2 = [" << str2.in() << "]" << endl;
	}
}

/*
 * Called when a request is started during the invocation.
 */
void
TestStub::marshal (COMM::DataEncoder *ec) 
{
	ec->put_ulong (10);
	ec->put_chars_raw ("test", 4);
	ec->put_string ("Hello[�ѱ�]");
}

/*
 * Called when a response is arrived during the invocation.
 */
COMM::Boolean
TestStub::demarshal (COMM::DataDecoder *dc) 
{
	switch (_req->get_exception_type ()) 
	{
	case COMM::NO_EXCEPTION:
		str[4] = 0;
		dc->get_ulong (val);
		dc->get_chars_raw (str, 4);
		dc->get_string (str2.out());
		return TRUE;
	case COMM::SYSTEM_EXCEPTION:
		dc->get_ulong (val);
		dc->get_string (str2.out());
		return TRUE;
	case COMM::USER_EXCEPTION:
		return TRUE;
	default:
		return FALSE;
	}
}

int	main (int argc, char *argv[]) 
{
	COMM::SRB_ptr srb = COMM::SRB_init(argc, argv);

	COMM::SRBConn *conn = srb->create_connection ("localhost", 12000);
	
	COMM::StaticRequest *req = conn->create_request ("my-svc");

	TestStub *stub = new TestStub (req);

	/*
	 * Set marshaller and demarshaller of the request.
	 */
	req->set_marshaller (stub, stub);

	int loop_count = 1;

	if(argc > 1)
		loop_count = atoi (argv[1]);

	for (int i=0 ; i<loop_count ; i++) 
	{
		cout << "***** loop count = " << i << endl;
		req->invoke ();
		stub->print_values ();
	}

	COMM::release (req);

	srb->close_connection (conn);
	
	srb->destroy ();

	return 0;
}
