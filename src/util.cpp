
#include <COMM.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#ifdef HAVE_ANSI_CPLUSPLUS_HEADERS
#include <iostream>
#include <fstream>
#include <sstream>
#else
#include <iostream.h>
#include <fstream.h>
#include <strstream.h>
#endif
#include <srbp/impl.h>
#include <srbp/template_impl.h>
#include <srbp/util.h>
#include <srbp/os-misc.h>
#include <srbp/os-math.h>


/******************************* assert *******************************/


void
srbp_assert (const char *file, int line)
{
    cout << file << ":" << line << ": assertion failed" << endl;
    abort ();
}


/****************************** strerror ******************************/


#ifndef HAVE_STRERROR
extern "C" {
	
    extern char *sys_errlist[];
    extern int sys_nerr;
	
    char *
		strerror (int err)
    {
		assert (err >= 0 && err < sys_nerr);
		return sys_errlist[err];
    }
}
#endif

string
xstrerror (int err)
{
    char *cp = strerror (err);
	
    if (cp)
        return string(cp);
	
    // Cygnus CDK returnes (char *)0 for some errnos
    string lasterr;
    lasterr = "error ";
    lasterr += xdec (errno);
    return lasterr;
}

string
xdec (int i)
{
#ifdef HAVE_ANSI_CPLUSPLUS_HEADERS
    ostringstream ostr;
    ostr << i;
    return ostr.str();
#else
    ostrstream ostr;
    ostr << i << ends;
    string s = ostr.str();
    ostr.rdbuf()->freeze (0);
    return s;
#endif
}

string
xdec (long i)
{
#ifdef HAVE_ANSI_CPLUSPLUS_HEADERS
    ostringstream ostr;
    ostr << i;
    return ostr.str();
#else
    ostrstream ostr;
    ostr << i << ends;
    string s = ostr.str();
    ostr.rdbuf()->freeze (0);
    return s;
#endif
}

string
xdec (OSMisc::longlong i)
{
#ifdef HAVE_ANSI_CPLUSPLUS_HEADERS
    ostringstream ostr;
#if defined(_WINDOWS) && !defined(__MINGW32__)
    ostr << long(i);
#else
    ostr << (long)i;
#endif
    return ostr.str();
#else
    ostrstream ostr;
    ostr << i << ends;
    string s = ostr.str();
    ostr.rdbuf()->freeze (0);
    return s;
#endif
}

string
xdec (unsigned int i)
{
#ifdef HAVE_ANSI_CPLUSPLUS_HEADERS
    ostringstream ostr;
    ostr << i;
    return ostr.str();
#else
    ostrstream ostr;
    ostr << i << ends;
    string s = ostr.str();
    ostr.rdbuf()->freeze (0);
    return s;
#endif
}

string
xdec (unsigned long i)
{
#ifdef HAVE_ANSI_CPLUSPLUS_HEADERS
    ostringstream ostr;
    ostr << i;
    return ostr.str();
#else
    ostrstream ostr;
    ostr << i << ends;
    string s = ostr.str();
    ostr.rdbuf()->freeze (0);
    return s;
#endif
}

string
xdec (OSMisc::ulonglong i)
{
#ifdef HAVE_ANSI_CPLUSPLUS_HEADERS
    ostringstream ostr;
#if defined(_WINDOWS) && !defined(__MINGW32__)
    ostr << unsigned long(i);
#else
    ostr << (unsigned long)i;
#endif
    return ostr.str();
#else
    ostrstream ostr;
    ostr << i << ends;
    string s = ostr.str();
    ostr.rdbuf()->freeze (0);
    return s;
#endif
}

size_t
xwcslen (const wchar_t *s)
{
    size_t len = 0;
    while (*s++)
		++len;
    return len;
}

int
xwcscmp (const wchar_t *s1, const wchar_t *s2)
{
    while (*s1 && *s1 == *s2) {
		++s1; ++s2;
    }
    if (*s1 == *s2)
		return 0;
    if (*s1 < *s2)
		return -1;
    return 1;
}

wchar_t *
xwcscpy (wchar_t *_d, const wchar_t *s)
{
    wchar_t *d = _d;
	
    while ((*d++ = *s++))
		;
    return _d;
}

wchar_t *
xwcsncpy (wchar_t *_d, const wchar_t *s, size_t n)
{
    wchar_t *d = _d;
	
    int i = (int)n;
    while (--i >= 0 && (*d++ = *s++))
		;
    return _d;
}

char *
srbp_url_encode (const COMM::Octet * ptr, COMM::ULong len)
{
	string res;
	
	for (COMM::ULong i=0; i<len; i++) {
		// US-ASCII alphanumeric characters are not escaped
		if ((*ptr >= 'a' && *ptr <= 'z') || (*ptr >= 'A' && *ptr <= 'Z') ||
			(*ptr >= '0' && *ptr <= '9')) {
			res += *ptr++;
			continue;
		}
		// Also not escaped
		switch (*ptr) {
		case ';':
		case '/':
		case ':':
		case '?':
		case '@':
		case '&':
		case '=':
		case '+':
		case '$':
		case ',':
		case '-':
		case '.':
		case '!':
		case '~':
		case '*':
		case '\'':
		case '(':
		case ')':
			res += *ptr++;
			break;
			
		default:
			{
				COMM::Octet x = *ptr++;
				res += '%';
				res += (char) srbp_to_xdigit (x>>4);
				res += (char) srbp_to_xdigit (x&15);
			}
		}
	}
	
	return COMM::string_dup (res.c_str());
}

COMM::Octet *
srbp_url_decode (const char * ptr, COMM::ULong & len)
{
	COMM::Octet * str = (COMM::Octet *) COMM::string_alloc (strlen (ptr));
	COMM::Octet * res = str;
	
	len = 0;
	
	while (*ptr) {
		if (*ptr == '%') {
			if ((ptr[1] < '0' || ptr[1] > '9') && (ptr[1] < 'a' || ptr[1] > 'f') ||
				(ptr[2] < '0' || ptr[2] > '9') && (ptr[2] < 'a' || ptr[2] > 'f')) {
				COMM::string_free ((char *) str);
				return NULL;
			}
			*res = (char) (srbp_from_xdigit(ptr[1])<<4 | srbp_from_xdigit(ptr[2]));
			ptr += 3;
		}
		else {
			*res = *ptr++;
		}
		res++;
		len++;
	}
	
	/*
	* Null-terminate the result so that it can be used as a string. The
	* null is deliberately not added to the length, because it isn't part
	* of the string.
	*/
	
	*res = 0;
	
	return str;
}

COMM::ULong
srbp_string_hash (const char *s, COMM::ULong max)
{
    if (max == 0)
		return 0;
	
    COMM::ULong g, v = 0;
    while (*s) {
		v = (v << 4) + *s++;
		if ((g = v & 0xf0000000)) {
			v = v ^ (g >> 24);
			v = v ^ g;
		}
    }
    return v % max;
}


/***************************** SRBPDebug ******************************/


SRBP::Logger * SRBP::Logger::_instance = 0;
ostream ** SRBP::Logger::_out = 0;

SRBP::Logger::Logger ()
{
	assert (!_instance);
	_instance = this;
	
	_out = new ostream * [All];
	
	for (int i=0; i<All; i++) {
		_out[i] = 0;
	}
	
	Log (Error);
}

SRBP::Logger::~Logger ()
{
	_instance = 0;
	
	for (int i=0; i<All; i++) {
		if (_out[i] != &cout && _out[i] != &cerr) {
			delete _out[i];
		}
	}
	
	delete [] _out;
}

void
SRBP::Logger::Log (MessageType msg, COMM::Boolean onoff,
				   const char * file)
{
	if (!_instance)
	{
		new Logger;
		assert (_instance);
	}
	
	if (msg == All) 
	{
		for (int i=0; i<All; i++)
		{
			Log ((MessageType) i, onoff, file);
		}
		return;
	}
	
	if (_out[msg] != &cout && _out[msg] != &cerr)
	{
		delete _out[msg];
	}
	
	if (onoff)
	{
		if (file)
		{
			_out[msg] = new ofstream (file, ios::app);
		}
		else 
		{
			_out[msg] = &cerr;
		}
	}
	else {
		_out[msg] = 0;
	}
}

void
SRBP::Logger::Log (const char * msg, COMM::Boolean onoff,
				   const char * file)
{
	if (strcmp (msg, "Info") == 0) {
		Log (Info, onoff, file);
	}
	else if (strcmp (msg, "Warning") == 0) {
		Log (Warning, onoff, file);
	}
	else if (strcmp (msg, "Error") == 0) {
		Log (Error, onoff, file);
	}
	else if (strcmp (msg, "GISP") == 0) {
		Log (GISP, onoff, file);
	}
	else if (strcmp (msg, "IISP") == 0) {
		Log (IISP, onoff, file);
	}
	else if (strcmp (msg, "Transport") == 0) {
		Log (Transport, onoff, file);
	}
	else if (strcmp (msg, "All") == 0) {
		Log (All, onoff, file);
	}
	else {
		if (IsLogged (Warning)) {
			Stream (Warning)
				<< "Warning: No such debug level" << msg << " is available." << endl;
		}
	}
}

COMM::Boolean
SRBP::Logger::IsLogged (MessageType msg)
{
	if (!_instance) 
	{
		return 0;
	}
	return (_out[msg] != 0);
}

ostream &
SRBP::Logger::Stream (MessageType msg)
{
	assert (_instance);
	assert (_out[msg]);
	return *_out[msg];
}


/***************************** SRBPGetOpt ******************************/


SRBPGetOpt::SRBPGetOpt (const OptMap &opts)
: _in_opts (opts)
{
}

SRBPGetOpt::~SRBPGetOpt ()
{
}

COMM::Boolean
SRBPGetOpt::parse (int &argc, char *argv[], COMM::Boolean ignore)
{
    vector<int> erase;
    vector<string> args;
	
    int i;
	for (i = 1; i < argc; ++i)
		args.push_back (argv[i]);
	
	if (!parse (args, erase, ignore))
		return FALSE;
	
    int nargc = 0;
	for (i = 0; i < argc; ++i) 
	{
		int j = erase[0]+1;
		if (erase.size() > 0 && i == erase[0]+1)
		{
			erase.erase (erase.begin());
		}
		else
		{
			argv[nargc++] = argv[i];
		}
    }
	if (nargc < argc)
		argv[nargc] = 0;
	
	argc = nargc;
	
    return TRUE;
}

COMM::Boolean
SRBPGetOpt::parse (const vector<string> &argv, vector<int> &erase,COMM::Boolean ignore)
{
    for (srbp_vec_size_type i = 0; i < argv.size(); ++i) 
	{
		string arg = argv[i];
		if (arg == "--") 
		{
			erase.push_back (i);
			break;
		}
		if (!ignore && (arg.size() == 0 || arg[0] != '-')) 
		{
			break;
		}
		OptMap::const_iterator it = _in_opts.find (arg); // option Map에서 option을 찾는다. 
		if (it == _in_opts.end())  //option Map에 없는 경우 
		{
			int pos = 0;
			if (arg.length() > 2)
			{
				// -Oval ??
				arg = arg.substr (0, 2);
				pos = 2;
				it = _in_opts.find (arg);
			}
			if (it == _in_opts.end() && (pos = argv[i].find ("=")) > 0) 
			{
				// --long-opt=something ??
				arg = argv[i].substr (0, pos);
				++pos;
				it = _in_opts.find (arg);
			}
			if (it != _in_opts.end())
			{
				if ((*it).second.length() == 0) 
				{
					cerr << "unexpected argument for option " << arg << endl;
					return FALSE;
				}
				_out_opts.push_back (make_pair (arg, argv[i].substr (pos)));
				erase.push_back (i);
			} 
			else if (!ignore) 
			{
				cerr << "unknown option: " << argv[i] << endl;
				return FALSE;
			}
		}
		else		//Option Map에 arument와 동일한 명령이 있는 경우 
		{
			erase.push_back (i);	//현재 Index 저장 
			if ((*it).second.length() > 0)  //second == map option value 
			{
				// -O val
				// --long-opt val
				if (++i == argv.size()) 
				{
					cerr << "missing argument for option " << arg << endl;
					return FALSE;
				}
				_out_opts.push_back (make_pair (arg, argv[i])); // 현재 -Command와 다음 argu를 연결 후 map에 저장 
				erase.push_back (i);
			} 
			else
			{
				// -O
				// --long-opt
				_out_opts.push_back (make_pair (arg, string("")));
			}
		}
    }
    return TRUE;
}

COMM::Boolean
SRBPGetOpt::parse (const vector<string> &argv, COMM::Boolean ignore)
{
    vector<int> erase;
    return parse (argv, erase, ignore);
}

COMM::Boolean
SRBPGetOpt::parse (const string &_filename, COMM::Boolean ignore)
{
    string fn = _filename;
	
    if (fn[0] == '~') {
        char *s = getenv ("HOME");
        if (!s) {
            return TRUE;
        }
        string home = s;
        fn.replace (0, 1, home);
    }
    ifstream in (fn.c_str());
    if (!in)
		return TRUE;
	
    char line[10000], *lptr, *tok;
    vector<string> argv;
	
    while (42) {
		if (in.getline(line, sizeof(line)).eof())
			break;
		if (!line[0])
			continue;
		lptr = line;
		while ((tok = ::strtok (lptr, " \t"))) {
			if (lptr && *tok == '#')
				break;
			argv.push_back (tok);
			lptr = 0;
		}
    }
    return parse (argv, ignore);
}

const SRBPGetOpt::OptVec &
SRBPGetOpt::opts () const
{
    return _out_opts;
}


/************************** repo id matcher ***************************/


COMM::Boolean
srbp_fnmatch (const char *s, const char *p)
{
    register int scc;
    int ok, lc;
    int c, cc;
	
    for (;;) {
		scc = *s++ & 0177;
		switch ((c = *p++)) {
		case '[':
			ok = 0;
			lc = 077777;
			while ((cc = *p++)) {
				if (cc == ']') {
					if (ok)
						break;
					return FALSE;
				}
				if (cc == '-') {
					if (lc <= scc && scc <= *p++)
						ok++;
				} else
					if (scc == (lc = cc))
						ok++;
			}
			if (cc == 0)
				if (ok)
					p--;
				else
					return FALSE;
				continue;
				
		case '*':
			if (!*p)
				return TRUE;
			s--;
			do {
				if (srbp_fnmatch(s, p))
					return TRUE;
			} while (*s++);
			return FALSE;
			
		case 0:
			return (scc == 0);
			
		default:
			if (c != scc)
				return FALSE;
			continue;
			
		case '?':
			if (scc == 0)
				return FALSE;
			continue;
		}
    }
}


/****************************** Process ******************************/


SRBP::Process::~Process ()
{
}

SRBP::ProcessCallback::~ProcessCallback ()
{
}


/***************************** SharedLib ******************************/


COMM::Boolean
SRBP::SharedLib::init ()
{
#ifdef HAVE_EXPLICIT_CTORS
    void (*gblctor) () = (void (*) ())symbol ("_GLOBAL__DI");
    assert (gblctor);
    (*gblctor) ();
#endif
    COMM::Boolean (*initfn) (const char *) =
		(COMM::Boolean (*) (const char *))symbol ("srbp_module_init");
    if (!initfn)
		return FALSE;
    return (*initfn) (SRBP_VERSION);
}

void
SRBP::SharedLib::exit ()
{
    void (*exitfn) () = (void (*) ())symbol ("srbp_module_exit");
    if (exitfn)
		(*exitfn) ();
#ifdef HAVE_EXPLICIT_CTORS
    void (*gbldtor) () = (void (*) ())symbol ("_GLOBAL__DD");
    assert (gbldtor);
    (*gbldtor) ();
#endif
}

SRBP::SharedLib::~SharedLib ()
{
}


/********************** Floating point converters *********************/


struct IeeeLDouble 
{
#ifdef HAVE_BYTEORDER_BE
    unsigned int s : 1;
    unsigned int e : 15;
    unsigned int f1 : 16;
    unsigned int f2 : 32;
    unsigned int f3 : 32;
    unsigned int f4 : 32;
#else
    unsigned int f4 : 32;
    unsigned int f3 : 32;
    unsigned int f2 : 32;
    unsigned int f1 : 16;
    unsigned int e : 15;
    unsigned int s : 1;
#endif
};

#define LDBL_EXP_BITS  15
#define LDBL_EXP_BIAS  16384
#define LDBL_EXP_MAX   ((1L << LDBL_EXP_BITS) - 1 - LDBL_EXP_BIAS)
#define LDBL_EXP_MIN   (1 - LDBL_EXP_BIAS)
#define LDBL_FRC1_BITS 16
#define LDBL_FRC2_BITS (LDBL_FRC1_BITS+32)
#define LDBL_FRC3_BITS (LDBL_FRC2_BITS+32)
#define LDBL_FRC_BITS  (LDBL_FRC3_BITS+32)

struct IeeeDouble 
{
#ifdef HAVE_BYTEORDER_BE
    unsigned int s : 1;
    unsigned int e : 11;
    unsigned int f1 : 20;
    unsigned int f2 : 32;
#else
    unsigned int f2 : 32;
    unsigned int f1 : 20;
    unsigned int e : 11;
    unsigned int s : 1;
#endif
};

#define DBL_EXP_BITS  11
#define DBL_EXP_BIAS  1023
#define DBL_EXP_MAX   ((1L << DBL_EXP_BITS) - 1 - DBL_EXP_BIAS)
#define DBL_EXP_MIN   (1 - DBL_EXP_BIAS)
#define DBL_FRC1_BITS 20
#define DBL_FRC2_BITS 32
#define DBL_FRC_BITS  (DBL_FRC1_BITS + DBL_FRC2_BITS)

struct IeeeFloat {
#ifdef HAVE_BYTEORDER_BE
    unsigned int s : 1;
    unsigned int e : 8;
    unsigned int f : 23;
#else
    unsigned int f : 23;
    unsigned int e : 8;
    unsigned int s : 1;
#endif
};

#define FLT_EXP_BITS 8
#define FLT_EXP_BIAS 127
#define FLT_EXP_MAX  ((1L << FLT_EXP_BITS) - 1 - FLT_EXP_BIAS)
#define FLT_EXP_MIN  (1 - FLT_EXP_BIAS)
#define FLT_FRC_BITS 23


void
srbp_ieee2ldouble (COMM::Octet ieee[16], COMM::LongDouble &d)
{
    IeeeLDouble &ie = (IeeeLDouble &)*ieee;
	
    if (ie.e == 0) {
		if (ie.f1 == 0 && ie.f2 == 0 && ie.f3 == 0 && ie.f4 == 0) {
			// zero
			d = 0.0;
		} else {
			// denormalized number
			d  = ldexpl ((long double)ie.f1, -LDBL_FRC1_BITS + LDBL_EXP_MIN);
			d += ldexpl ((long double)ie.f2, -LDBL_FRC2_BITS + LDBL_EXP_MIN);
			d += ldexpl ((long double)ie.f3, -LDBL_FRC3_BITS + LDBL_EXP_MIN);
			d += ldexpl ((long double)ie.f4, -LDBL_FRC_BITS  + LDBL_EXP_MIN);
			if (ie.s)
				d = -d;
		}
    } else if (ie.e == LDBL_EXP_MAX + LDBL_EXP_BIAS) {
		if (ie.f1 == 0 && ie.f2 == 0 && ie.f3 == 0 && ie.f4 == 0) {
			// +/- infinity
			d = OSMath::infinityl (ie.s);
		} else {
			// not a number
			//TODO truevoid d = OSMath::nanl ();
		}
    } else {
		// normalized number
		d = ldexpl (ldexpl ((long double)ie.f1, -LDBL_FRC1_BITS) +
			ldexpl ((long double)ie.f2, -LDBL_FRC2_BITS) +
			ldexpl ((long double)ie.f3, -LDBL_FRC3_BITS) +
			ldexpl ((long double)ie.f4, -LDBL_FRC_BITS) +
			1.0,
			ie.e - LDBL_EXP_BIAS);
		if (ie.s)
			d = -d;
    }
}

void
srbp_ldouble2ieee (COMM::Octet ieee[16], COMM::LongDouble d)
{
    IeeeLDouble &ie = (IeeeLDouble &)*ieee;
	
    if (OSMath::is_nanl (d)) {
		// XXX not a number (what is signaling NAN ???)
		ie.s = 0;
		ie.e = LDBL_EXP_MAX + LDBL_EXP_BIAS;
		ie.f1 = 1;
		ie.f2 = 1;
		ie.f3 = 1;
		ie.f4 = 1;
    } else if (OSMath::is_infinityl (d)) {
		// +/- infinity
		ie.s = (d < 0);
		ie.e = LDBL_EXP_MAX + LDBL_EXP_BIAS;
		ie.f1 = 0;
		ie.f2 = 0;
		ie.f3 = 0;
		ie.f4 = 0;
    } else if (d == 0.0) {
		// zero
		ie.s = 0;
		ie.e = 0;
		ie.f1 = 0;
		ie.f2 = 0;
		ie.f3 = 0;
		ie.f4 = 0;
    } else {
		// finite number
		int exp;
		long double frac = frexpl (fabsl (d), &exp);
		
		while (frac < 1.0 && exp >= LDBL_EXP_MIN) {
			frac = ldexpl (frac, 1);
			--exp;
		}
		if (exp < LDBL_EXP_MIN) {
			// denormalized number (or zero)
			frac = ldexpl (frac, exp - LDBL_EXP_MIN);
			exp = 0;
		} else {
			// normalized number
			assert (1.0 <= frac && frac < 2.0);
			assert (LDBL_EXP_MIN <= exp && exp <= LDBL_EXP_MAX);
			
			exp += LDBL_EXP_BIAS;
            frac -= 1.0;
		}
		ie.s = (d < 0);
		ie.e = exp;
		ie.f1 = (unsigned long)ldexpl (frac, LDBL_FRC1_BITS);
		ie.f2 = (unsigned long)ldexpl (frac, LDBL_FRC2_BITS);
		ie.f3 = (unsigned long)ldexpl (frac, LDBL_FRC3_BITS);
		ie.f4 = (unsigned long)ldexpl (frac, LDBL_FRC_BITS);
    }
}

void
srbp_ieee2double (COMM::Octet ieee[8], COMM::Double &d)
{
    IeeeDouble &ie = (IeeeDouble &)*ieee;
	
    if (ie.e == 0) {
		if (ie.f1 == 0 && ie.f2 == 0) {
			// zero
			d = 0.0;
		} else {
			// denormalized number
			d  = ldexp ((double)ie.f1, -DBL_FRC1_BITS + DBL_EXP_MIN);
			d += ldexp ((double)ie.f2, -DBL_FRC_BITS  + DBL_EXP_MIN);
			if (ie.s)
				d = -d;
		}
    } else if (ie.e == DBL_EXP_MAX + DBL_EXP_BIAS) {
		if (ie.f1 == 0 && ie.f2 == 0) {
			// +/- infinity
			d = OSMath::infinity (ie.s);
		} else {
			// not a number
			d = OSMath::nan ();
		}
    } else {
		// normalized number
		d = ldexp (ldexp ((double)ie.f1, -DBL_FRC1_BITS) +
			ldexp ((double)ie.f2, -DBL_FRC_BITS) +
			1.0,
			ie.e - DBL_EXP_BIAS);
		if (ie.s)
			d = -d;
    }
}

void
srbp_double2ieee (COMM::Octet ieee[8], COMM::Double d)
{
    IeeeDouble &ie = (IeeeDouble &)*ieee;
	
    if (OSMath::is_nan (d)) {
		// XXX not a number (what is signaling NAN ???)
		ie.s = 0;
		ie.e = DBL_EXP_MAX + DBL_EXP_BIAS;
		ie.f1 = 1;
		ie.f2 = 1;
    } else if (OSMath::is_infinity (d)) {
		// +/- infinity
		ie.s = (d < 0);
		ie.e = DBL_EXP_MAX + DBL_EXP_BIAS;
		ie.f1 = 0;
		ie.f2 = 0;
    } else if (d == 0.0) {
		// zero
		ie.s = 0;
		ie.e = 0;
		ie.f1 = 0;
		ie.f2 = 0;
    } else {
		// finite number
		int exp;
		double frac = frexp (fabs (d), &exp);
		
		while (frac < 1.0 && exp >= DBL_EXP_MIN) {
			frac = ldexp (frac, 1);
			--exp;
		}
		if (exp < DBL_EXP_MIN) {
			// denormalized number (or zero)
			frac = ldexp (frac, exp - DBL_EXP_MIN);
			exp = 0;
		} else {
			// normalized number
			assert (1.0 <= frac && frac < 2.0);
			assert (DBL_EXP_MIN <= exp && exp <= DBL_EXP_MAX);
			
			exp += DBL_EXP_BIAS;
			frac -= 1.0;
		}
		ie.s = (d < 0);
		ie.e = exp;
		ie.f1 = (unsigned long)ldexp (frac, DBL_FRC1_BITS);
		ie.f2 = (unsigned long)ldexp (frac, DBL_FRC_BITS);
    }
}

void
srbp_ieee2float (COMM::Octet ieee[4], COMM::Float &f)
{
    IeeeFloat &ie = (IeeeFloat &)*ieee;
	
    if (ie.e == 0) {
		if (ie.f == 0) {
			// zero
			f = 0.0;
		} else {
			// denormalized number
			f = ldexp ((double)ie.f, -FLT_FRC_BITS + FLT_EXP_MIN);
			if (ie.s)
				f = -f;
		}
    } else if (ie.e == FLT_EXP_MAX + FLT_EXP_BIAS) {
		if (ie.f == 0) {
			// +/- infinity
			f = OSMath::infinity (ie.s);
		} else {
			// not a number
			f = OSMath::nan ();
		}
    } else {
		// normalized number
		f = ldexp (ldexp ((double)ie.f, -FLT_FRC_BITS) + 1.0,
			ie.e - FLT_EXP_BIAS);
		if (ie.s)
			f = -f;
    }
}

void
srbp_float2ieee (COMM::Octet ieee[4], COMM::Float f)
{
    IeeeFloat &ie = (IeeeFloat &)*ieee;
	
    if (OSMath::is_nan (f)) {
		// XXX not a number (what is signaling NAN ???)
		ie.s = 0;
		ie.e = FLT_EXP_MAX + FLT_EXP_BIAS;
		ie.f = 1;
    } else if (OSMath::is_infinity (f)) {
		// +/- infinity
		ie.s = (f < 0);
		ie.e = FLT_EXP_MAX + FLT_EXP_BIAS;
		ie.f = 0;
    } else if (f == 0.0) {
		// zero
		ie.s = 0;
		ie.e = 0;
		ie.f = 0;
    } else {
		// finite number
		int exp;
		double frac = frexp (fabs (f), &exp);
		
		while (frac < 1.0 && exp >= FLT_EXP_MIN) {
			frac = ldexp (frac, 1);
			--exp;
		}
		if (exp < FLT_EXP_MIN) {
			// denormalized number (or zero)
			frac = ldexp (frac, exp - FLT_EXP_MIN);
			exp = 0;
		} else {
			// normalized number
			assert (1.0 <= frac && frac < 2.0);
			assert (FLT_EXP_MIN <= exp && exp <= FLT_EXP_MAX);
			
			exp += FLT_EXP_BIAS;
			frac -= 1.0;
		}
		ie.s = (f < 0);
		ie.e = exp;
		ie.f = (unsigned long)ldexp (frac, FLT_FRC_BITS);
    }
}
