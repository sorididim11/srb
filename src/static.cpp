
#include <COMM.h>
#include <string.h>
//#include <srbp/throw.h>
#include <srbp/util.h>
#include <srbp/impl.h>
#include <srbp/template_impl.h>


/******************************* StaticRequest ********************************/


COMM::StaticRequest::StaticRequest (COMM::SRB_ptr srb, COMM::DataEncoder* ec,COMM::DataDecoder* dc,
									const char *svcname,
									const COMM::Address* addr,
									const COMM::Address* peer) 
{
	_srb = srb;
	_ec = ec;
	_dc = dc;
	_svcname = svcname;
	_addr = addr->clone ();
	_peer = peer->clone ();
	_msgid = 0;
}

COMM::StaticRequest::~StaticRequest () 
{
	delete _addr;
	delete _peer;
}

const COMM::Address *
COMM::StaticRequest::addr () 
{
	return _addr;
}

const COMM::Address *
COMM::StaticRequest::peer () 
{
	return _peer;
}

const char *
COMM::StaticRequest::svc_name () 
{
	return _svcname;
}

COMM::DataEncoder *
COMM::StaticRequest::get_encoder () 
{
	return _ec;
}

COMM::DataDecoder *
COMM::StaticRequest::get_decoder ()
 {
	return _dc;
}

void
COMM::StaticRequest::set_marshaller (COMM::SRBMarshaller *ma,COMM::SRBDemarshaller *dma)
 {
	_ma = ma;
	_dma = dma;
}

void
COMM::StaticRequest::marshal (COMM::DataEncoder *ec) {
	_ma->marshal (ec);
}

COMM::Boolean
COMM::StaticRequest::demarshal (COMM::DataDecoder *dc) {
	return _dma->demarshal (dc);
}

void 
COMM::StaticRequest::set_sys_exception (COMM::SysExceptionCode ex_code) {
	assert (0);
}

void 
COMM::StaticRequest::set_sys_exception (COMM::SysExceptionCode ex_code, 
		const char *ex_msg) {
	assert (0);
}

void 
COMM::StaticRequest::set_exception_type (COMM::ExceptionType ex_type) {
	_ex_type = ex_type;
}

COMM::ExceptionType 
COMM::StaticRequest::get_exception_type () {
	return _ex_type;
}

COMM::Boolean
COMM::StaticRequest::copy_out_data (SRBRequest *r) {
	if (this == r)
		return TRUE;

	/*TODO
	COMM::Exception *ex;
	if (!r->get_out_args (_res, &_args, ex))
		return FALSE;
	if (ex)
		exception (ex);
		*/
	return TRUE;
}

COMM::Boolean
COMM::StaticRequest::copy_in_data (SRBRequest *r) {
	assert (0);
	return FALSE;
}

void
COMM::StaticRequest::invoke () 
{
//	COMM::SRBRequest *dummy;
//	COMM::ULong msgid;

	assert (!_msgid);

	COMM::InvokeStatus rs = _srb->invoke (this, TRUE);

	switch (rs) 
	{
	case COMM::InvokeOk:
		break;
	case COMM::InvokeUsrEx:
		break;
	case COMM::InvokeSysEx:
		break;
	default:
		assert (0);
	}
}

