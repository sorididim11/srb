
#include <COMM.h>
#ifndef _WINDOWS
#include <string.h>
#include <pthread.h>
#endif
#include <srbp/template_impl.h>
#include <srbp/util.h>

/*************************** MagicChecker ****************************/


void
COMM::MagicChecker::_check () const {
	/*TODO
	if (!this || magic != SRBP_OBJ_MAGIC) {
		srbp_throw (BAD_PARAM());
	}
	*/
}

void
COMM::MagicChecker::_check (const COMM::Exception &ex) const {
	/*TODO
    if (!this || magic != SRBP_OBJ_MAGIC) {
	((COMM::Exception&)ex)._raise();
    }
    */
}

COMM::Boolean
COMM::MagicChecker::_check_nothrow () const {
	/*TODO
    if (!this || magic != SRBP_OBJ_MAGIC) {
      if (SRBP::Logger::IsLogged (SRBP::Logger::Warning)) {
	SRBP::Logger::Stream (SRBP::Logger::Warning)
	  << "invalid object reference" << endl;
      }
      return FALSE;
    }
    */
    return TRUE;
}


/************************* ServerlessObject **************************/


#ifdef _WINDOWS
extern CRITICAL_SECTION so_mut;
#else
//extern pthread_mutex_t so_mut = PTHREAD_MUTEX_INITIALIZER;
#endif

COMM::ServerlessObject::~ServerlessObject () { }

void
COMM::ServerlessObject::_ref () 
{
#ifdef _WINDOWS
	EnterCriticalSection(&so_mut);
#else
	pthread_mutex_lock(&so_mut);
#endif

	_check ();
	++refs;
    
#ifdef _WINDOWS
	LeaveCriticalSection(&so_mut);
#else
	pthread_mutex_unlock(&so_mut);
#endif
}

COMM::Boolean
COMM::ServerlessObject::_deref () 
{
#ifdef _WINDOWS
	EnterCriticalSection(&so_mut);
#else
	pthread_mutex_lock(&so_mut);
#endif

	COMM::Boolean ret = _check_nothrow() && --refs <= 0;
    
#ifdef _WINDOWS
	LeaveCriticalSection(&so_mut);
#else
	pthread_mutex_unlock(&so_mut);
#endif  
	return ret;
}

COMM::Long
COMM::ServerlessObject::_refcnt () const 
{
	_check ();
	return refs;
}

