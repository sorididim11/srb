
#include <COMM.h>
#ifndef _WINDOWS
#include <string.h>
#endif
#include <srbp/util.h>
#include <srbp/template_impl.h>


/************************* String support **************************/

/*
 * initializing the string here saves a lot of code elsewhere:
 * structs, unions, sequences, arrays and valuetypes all require
 * that their string members are initialized to the empty string.
 *
 * By using a special value for the empty string here, we don't need
 * to do a string_dup("") whenever a String_var is allocated. This
 * saves a lot of time if a lot of strings are used, i.e. a
 * sequence<string>.
 */

static const char * the_empty_string = "";

COMM::String_var::String_var ()
  : _str ((char *) the_empty_string)
{
}

COMM::String_var::String_var (char *s)
{
  _str = s;
}

COMM::String_var::String_var (const char *s)
{
  _str = COMM::string_dup (s);
}

COMM::String_var::String_var (const String_var &s)
{
  if (s._str == the_empty_string) {
    _str = s._str;
  }
  else {
    _str = COMM::string_dup (s._str);
  }
}

COMM::String_var::~String_var ()
{
  if (_str != the_empty_string) {
    COMM::string_free (_str);
  }
}

COMM::String_var &
COMM::String_var::operator= (char *s)
{
  if (_str != the_empty_string) {
    COMM::string_free (_str);
  }
  _str = s;
  return *this;
}

COMM::String_var &
COMM::String_var::operator= (const char *s)
{
  if (_str != the_empty_string) {
    COMM::string_free (_str);
  }
  _str = COMM::string_dup (s);
  return *this;
}

COMM::String_var &
COMM::String_var::operator= (const String_var &s)
{
  if (this != &s) {
    if (_str != the_empty_string) {
      COMM::string_free (_str);
    }
    
    if (s._str == the_empty_string) {
      _str = s._str;
    }
    else {
      _str = COMM::string_dup (s._str);
    }
  }
  return *this;
}

COMM::Boolean
COMM::String_var::operator== (const String_var &s) const
{
  if (!s._str || !_str) {
    return !s._str && !_str;
  }
  return !strcmp (s._str, _str);
}

char *
COMM::String_var::_retn ()
{
  char *s = _str;
  _str = 0;
  
  if (s == the_empty_string) {
    s = COMM::string_dup ("");
  }
  
  return s;
}

const char *
COMM::String_var::in () const
{
  return _str;
}

char *&
COMM::String_var::out ()
{
  if (_str != the_empty_string) {
    COMM::string_free (_str);
  }
  _str = 0;
  return _str;
}

char *&
COMM::String_var::inout ()
{
  if (_str == the_empty_string) {
    _str = COMM::string_dup ("");
  }

  return _str;
}


COMM::String_out::String_out (char*& s)
    : _str (s)
{
    _str = 0;
}

COMM::String_out::String_out (String_var& s)
    : _str (s._str)
{
  if (_str != the_empty_string) {
    COMM::string_free (_str);
  }
  _str = 0;
}

COMM::String_out::String_out (const String_out& s)
    : _str (s._str)
{
}

COMM::String_out&
COMM::String_out::operator= (const String_out& s)
{
    _str = s._str;
    return *this;
}

COMM::String_out&
COMM::String_out::operator= (char* s)
{
    _str = s;
    return *this;
}

COMM::String_out&
COMM::String_out::operator= (const char* s)
{
    _str = COMM::string_dup (s);
    return *this;
}


char *
COMM::string_alloc (ULong len)
{
  char *s = new char[len+1];
  s[0] = 0;
  return s;
}

char *
COMM::string_dup (const char *s2)
{
  if (!s2)
    return 0;

  char *s = new char[strlen (s2) + 1];
  strcpy (s, s2);
  return s;
}

char *
COMM::string_ndup (const char *s2, ULong len)
{
  if (!s2)
    return 0;
  
  char *s = new char[len+1];
  strncpy (s, s2, len);
  s[len] = 0;
  return s;
}

void
COMM::string_free (char *s)
{
  if (s)
    delete[] s;
}


/************************* WString support **************************/


COMM::WString_var::WString_var ()
{
    // initializing the string here saves a lot of code elsewhere:
    // structs, unions, sequences, arrays and valuetypes all require
    // that their string members are initialized to the empty string
    _str = COMM::wstring_dup (L"");
}

COMM::WString_var::WString_var (wchar_t *s)
{
    _str = s;
}

COMM::WString_var::WString_var (const wchar_t *s)
{
    _str = COMM::wstring_dup (s);
}

COMM::WString_var::WString_var (const WString_var &s)
{
    _str = COMM::wstring_dup (s._str);
}

COMM::WString_var::~WString_var ()
{
    COMM::wstring_free (_str);
}

COMM::WString_var &
COMM::WString_var::operator= (wchar_t *s)
{
    COMM::wstring_free (_str);
    _str = s;
    return *this;
}

COMM::WString_var &
COMM::WString_var::operator= (const wchar_t *s)
{
    COMM::wstring_free (_str);
    _str = COMM::wstring_dup (s);
    return *this;
}

COMM::WString_var &
COMM::WString_var::operator= (const WString_var &s)
{
    if (this != &s) {
	COMM::wstring_free (_str);
	_str = COMM::wstring_dup (s._str);
    }
    return *this;
}

COMM::Boolean
COMM::WString_var::operator== (const WString_var &s) const
{
    if (!s._str || !_str)
        return !s._str && !_str;
    return !xwcscmp (s._str, _str);
}

wchar_t *
COMM::WString_var::_retn ()
{
    wchar_t *s = _str;
    _str = 0;
    return s;
}

const wchar_t *
COMM::WString_var::in () const
{
    return _str;
}

wchar_t *&
COMM::WString_var::out ()
{
    COMM::wstring_free (_str);
    _str = 0;
    return _str;
}

wchar_t *&
COMM::WString_var::inout ()
{
    return _str;
}


COMM::WString_out::WString_out (wchar_t*& s)
    : _str (s)
{
    _str = 0;
}

COMM::WString_out::WString_out (WString_var& s)
    : _str (s._str)
{
    COMM::wstring_free (_str);
    _str = 0;
}

COMM::WString_out::WString_out (const WString_out& s)
    : _str (s._str)
{
}

COMM::WString_out&
COMM::WString_out::operator= (const WString_out& s)
{
    _str = s._str;
    return *this;
}

COMM::WString_out&
COMM::WString_out::operator= (wchar_t* s)
{
    _str = s;
    return *this;
}

COMM::WString_out&
COMM::WString_out::operator= (const wchar_t* s)
{
    _str = COMM::wstring_dup (s);
    return *this;
}



wchar_t *
COMM::wstring_alloc (ULong len)
{
    wchar_t *s = new wchar_t[len+1];
    s[0] = 0;
    return s;
}

wchar_t *
COMM::wstring_dup (const wchar_t *s2)
{
    if (!s2)
        return 0;
    wchar_t *s = new wchar_t[xwcslen (s2) + 1];
    xwcscpy (s, s2);
    return s;
}

wchar_t *
COMM::wstring_ndup (const wchar_t *s2, ULong len)
{
    if (!s2)
        return 0;
    wchar_t *s = new wchar_t[len+1];
    xwcsncpy (s, s2, len);
    s[len] = 0;
    return s;
}

void
COMM::wstring_free (wchar_t *s)
{
    if (s)
        delete[] s;
}
