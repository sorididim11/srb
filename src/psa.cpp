
#include <COMM.h>

#include <srbp/impl.h>


/************************** PSA ****************************/


SRBP::PSA::PSA (COMM::SRB_ptr srb) 
{
	_srb = srb;

	_srb->register_sa (this);  //SRB�� ��� 
}

SRBP::PSA::~PSA () {
	_srb->unregister_sa (this);

	ServiceMap::iterator i;
	for(i = _services.begin () ; i != _services.end () ;++i)
		delete (*i).second;
}

void
SRBP::PSA::add_service (const char *svcname, COMM::SRBService *svc) 
{
	_services[svcname] = svc; //Map key : svcname , value : SRBService
}

void
SRBP::PSA::del_service (const char *svcname) 
{
	ServiceMap::iterator i = _services.find (svcname);
	if (i != _services.end ())
		_services.erase (i);
}

COMM::SRBService *
SRBP::PSA::get_service (const char *svcname) 
{
	ServiceMap::iterator i = _services.find (svcname);
	return (i == _services.end ()) ? 0 : (*i).second;
}

COMM::Boolean
SRBP::PSA::has_service (const char *svcname) 
{
	ServiceMap::iterator i = _services.find (svcname);
	return (i == _services.end ()) ? FALSE : TRUE;
}

COMM::Boolean
SRBP::PSA::is_local () const 
{
	return TRUE;
}

COMM::Boolean
SRBP::PSA::invoke (MsgId msgid, COMM::SRBRequest *req, COMM::Boolean response_exp)
 {
	COMM::SRBService *svc = get_service (req->svc_name ());

	if(!svc) 
	{
		/*TODO
		COMM::SERVICE_NOT_EXIST ex;
		req->set_out_args (&ex);
		*/
		answer_invoke (msgid, req, COMM::InvokeSysEx);
		return FALSE;
	}

	req->set_marshaller (svc->marshaller (), svc->demarshaller ());
	req->demarshal (req->get_decoder ());

	if (!svc->doservice ()) 
	{
		/*TODO
		COMM::SERVICE_EXCEPTION ex;
		req->set_out_args (&ex);
		*/
		answer_invoke (msgid, req, COMM::InvokeUsrEx);
		return FALSE;
	}

	answer_invoke (msgid, req, COMM::InvokeOk);

	return TRUE;
}

int
SRBP::PSA::answer_invoke (COMM::SRB::MsgId msgid, COMM::SRBRequest *sreq, COMM::InvokeStatus stat) 
{
	int ret;
	ret = _srb->answer_invoke (msgid, stat, sreq);
	return ret;
}

void
SRBP::PSA::shutdown (COMM::Boolean wait_for_completion) 
{
	_srb->answer_shutdown (this);
}


