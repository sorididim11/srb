
#include <COMM.h>

static char *sysExceptionMsg[] = {
	"SERVICE_NOT_EXIST",
	"EX_CODE_NOT_DEFINED"
};

string
COMM::get_sys_exception_name (COMM::SysExceptionCode ex_code) {
	string ex_msg;

	if (ex_code < NUM_OF_EX_CODES)
		ex_msg = sysExceptionMsg[ex_code];
	else {
		ex_msg = sysExceptionMsg[NUM_OF_EX_CODES];
		ex_msg += ": ";
		ex_msg += ex_code;
	}

	return ex_msg;
}

string
COMM::get_sys_exception_name (COMM::SysExceptionCode ex_code, const char *msg) {
	string ex_msg;

	if (ex_code < NUM_OF_EX_CODES) {
		ex_msg = sysExceptionMsg[ex_code];
		ex_msg += ": ";
		ex_msg += msg;
	} else {
		ex_msg = sysExceptionMsg[NUM_OF_EX_CODES];
		ex_msg += ": ";
		ex_msg += msg;
	}

	return ex_msg;
}

