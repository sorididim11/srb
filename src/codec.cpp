
#include <COMM.h>
#ifndef _WINDOWS
#include <string.h>
#endif
#include <srbp/impl.h>
#include <srbp/template_impl.h>
#include <srbp/util.h>

/**************************** DataEncoder ******************************/


COMM::DataEncoder::DataEncoder () 
{
    buf = new Buffer;
    dofree_buf = TRUE;
}

COMM::DataEncoder::DataEncoder (Buffer *b, Boolean dofree_b) 
{
    buf = b;
    dofree_buf = dofree_b;
}

COMM::DataEncoder::~DataEncoder () 
{
    if (dofree_buf)
		delete buf;
}

void
COMM::DataEncoder::put_buffer (const Buffer &b) 
{
    buf->put (b.data(), b.length());
}

void
COMM::DataEncoder::put_octets (const void *data, ULong len) 
{
    buf->put (data, len);
}

void
COMM::DataEncoder::put_string (const string &s)
{
    put_string (s.c_str());
}

void
COMM::DataEncoder::put_string_raw (const string &s)
{
    put_string_raw (s.c_str());
}

void
COMM::DataEncoder::enumeration (ULong val)
{
    put_ulong (val);
}

void
COMM::DataEncoder::struct_begin () { }

void
COMM::DataEncoder::struct_end () { }

void
COMM::DataEncoder::arr_begin () { }

void
COMM::DataEncoder::arr_end () { }

void
COMM::DataEncoder::buffer (Buffer *b, Boolean release)
{
    if (dofree_buf)
		delete buf;
    buf = b;
    dofree_buf = release;
}

COMM::ByteOrder
COMM::DataEncoder::byteorder () const
{
#if defined(HAVE_BYTEORDER_BE)
    return COMM::BigEndian;
#else
    return COMM::LittleEndian;
#endif
}

void
COMM::DataEncoder::byteorder (COMM::ByteOrder bo)
{
    assert (bo == byteorder ());
}


/**************************** DataDecoder ******************************/


COMM::DataDecoder::DataDecoder (Buffer *b, Boolean dofree_b)
{
    buf = b;
    dofree_buf = dofree_b;
}

COMM::DataDecoder::~DataDecoder ()
{
    if (dofree_buf)
		delete buf;
}

COMM::Boolean
COMM::DataDecoder::get_octets (void *data, ULong len) 
{
    return buf->get (data, len);
}

COMM::Boolean
COMM::DataDecoder::get_string_stl (string &str) 
{
    char *s;
    if (!get_string (s))
        return FALSE;
    str = s;
    COMM::string_free (s);
    return TRUE;
}

COMM::Boolean
COMM::DataDecoder::get_string_raw_stl (string &str) 
{
    char *s;
    if (!get_string_raw (s))
        return FALSE;
    str = s;
    COMM::string_free (s);
    return TRUE;
}

COMM::Boolean
COMM::DataDecoder::enumeration (ULong &val) 
{
    return get_ulong (val);
}

COMM::Boolean
COMM::DataDecoder::struct_begin ()
{
    return TRUE;
}

COMM::Boolean
COMM::DataDecoder::struct_end () 
{
    return TRUE;
}

COMM::Boolean
COMM::DataDecoder::arr_begin ()
{
    return TRUE;
}

COMM::Boolean
COMM::DataDecoder::arr_end () 
{
    return TRUE;
}

void
COMM::DataDecoder::buffer (Buffer *b, Boolean release)
{
    if (dofree_buf)
		delete buf;
    buf = b;
    dofree_buf = release;
}

COMM::ByteOrder
COMM::DataDecoder::byteorder () const {
#if defined(HAVE_BYTEORDER_BE)
    return COMM::BigEndian;
#else
    return COMM::LittleEndian;
#endif
}

void
COMM::DataDecoder::byteorder (COMM::ByteOrder bo) {
    assert (bo == byteorder());
}



/**************************** CDREncoder ******************************/

static inline void swap2 (void *d, const void *s)
{
    ((COMM::Octet *)d)[0] = ((COMM::Octet *)s)[1];
    ((COMM::Octet *)d)[1] = ((COMM::Octet *)s)[0];
}

static inline void swap4 (void *d, const void *s)
{
    ((COMM::Octet *)d)[0] = ((COMM::Octet *)s)[3];
    ((COMM::Octet *)d)[1] = ((COMM::Octet *)s)[2];
    ((COMM::Octet *)d)[2] = ((COMM::Octet *)s)[1];
    ((COMM::Octet *)d)[3] = ((COMM::Octet *)s)[0];
}

static inline void swap8 (void *d, const void *s)
{
    ((COMM::Octet *)d)[0] = ((COMM::Octet *)s)[7];
    ((COMM::Octet *)d)[1] = ((COMM::Octet *)s)[6];
    ((COMM::Octet *)d)[2] = ((COMM::Octet *)s)[5];
    ((COMM::Octet *)d)[3] = ((COMM::Octet *)s)[4];
    ((COMM::Octet *)d)[4] = ((COMM::Octet *)s)[3];
    ((COMM::Octet *)d)[5] = ((COMM::Octet *)s)[2];
    ((COMM::Octet *)d)[6] = ((COMM::Octet *)s)[1];
    ((COMM::Octet *)d)[7] = ((COMM::Octet *)s)[0];
}

static inline void swap16 (void *d, const void *s)
{
    ((COMM::Octet *)d)[0]  = ((COMM::Octet *)s)[15];
    ((COMM::Octet *)d)[1]  = ((COMM::Octet *)s)[14];
    ((COMM::Octet *)d)[2]  = ((COMM::Octet *)s)[13];
    ((COMM::Octet *)d)[3]  = ((COMM::Octet *)s)[12];
    ((COMM::Octet *)d)[4]  = ((COMM::Octet *)s)[11];
    ((COMM::Octet *)d)[5]  = ((COMM::Octet *)s)[10];
    ((COMM::Octet *)d)[6]  = ((COMM::Octet *)s)[9];
    ((COMM::Octet *)d)[7]  = ((COMM::Octet *)s)[8];
    ((COMM::Octet *)d)[8]  = ((COMM::Octet *)s)[7];
    ((COMM::Octet *)d)[9]  = ((COMM::Octet *)s)[6];
    ((COMM::Octet *)d)[10] = ((COMM::Octet *)s)[5];
    ((COMM::Octet *)d)[11] = ((COMM::Octet *)s)[4];
    ((COMM::Octet *)d)[12] = ((COMM::Octet *)s)[3];
    ((COMM::Octet *)d)[13] = ((COMM::Octet *)s)[2];
    ((COMM::Octet *)d)[14] = ((COMM::Octet *)s)[1];
    ((COMM::Octet *)d)[15] = ((COMM::Octet *)s)[0];
}

SRBP::CDREncoder::CDREncoder () 
{
#ifdef HAVE_BYTEORDER_BE
	mach_bo = COMM::BigEndian;
#else
	mach_bo = COMM::LittleEndian;
#endif
	data_bo = mach_bo;
}

SRBP::CDREncoder::CDREncoder (COMM::Buffer *b, COMM::Boolean dofree_b,COMM::ByteOrder _bo)
: COMM::DataEncoder (b, dofree_b) 
{
#ifdef HAVE_BYTEORDER_BE
    mach_bo = COMM::BigEndian;
#else
    mach_bo = COMM::LittleEndian;
#endif
    data_bo = (_bo == COMM::DefaultEndian) ? mach_bo : _bo;
}

SRBP::CDREncoder::~CDREncoder () { }

COMM::DataEncoder *
SRBP::CDREncoder::clone () const {
    return new CDREncoder (new COMM::Buffer (*buf), TRUE, data_bo);
}

COMM::DataEncoder *
SRBP::CDREncoder::clone (COMM::Buffer *b, COMM::Boolean dofree_b) const {
    return new CDREncoder (b, dofree_b, data_bo);
}

COMM::DataDecoder *
SRBP::CDREncoder::decoder () const {
    return new CDRDecoder (new COMM::Buffer (*buf), TRUE, data_bo);
}

COMM::DataDecoder *
SRBP::CDREncoder::decoder (COMM::Buffer *b, COMM::Boolean dofree_b) const {
    return new CDRDecoder (b, dofree_b, data_bo);
}

void
SRBP::CDREncoder::put_short (COMM::Short s) {
	buf->walign (2);
    if (mach_bo == data_bo) {
		buf->put2 (&s);
    } else {
		COMM::Short s2;
		swap2 (&s2, &s);
		buf->put2 (&s2);
    }
}

void
SRBP::CDREncoder::put_ushort (COMM::UShort us) {
	buf->walign (2);
    if (mach_bo == data_bo) {
		buf->put2 (&us);
    } else {
		COMM::UShort us2;
		swap2 (&us2, &us);
		buf->put2 (&us2);
    }
}

void
SRBP::CDREncoder::put_long (COMM::Long l) {
	buf->walign (4);
    if (mach_bo == data_bo) {
		buf->put4 (&l);
    } else {
		COMM::Long l2;
		swap4 (&l2, &l);
		buf->put4 (&l2);
    }
}

void
SRBP::CDREncoder::put_longlong (COMM::LongLong l) {
	buf->walign (8);
    if (mach_bo == data_bo) {
		buf->put8 (&l);
    } else {
		COMM::LongLong l2;
		swap8 (&l2, &l);
		buf->put8 (&l2);
    }
}

void
SRBP::CDREncoder::put_ulong (COMM::ULong ul) {
	buf->walign (4);
    if (mach_bo == data_bo) {
		buf->put4 (&ul);
    } else {
		COMM::ULong ul2;
		swap4 (&ul2, &ul);
		buf->put4 (&ul2);
    }
}

void
SRBP::CDREncoder::put_ulonglong (COMM::ULongLong ul) {
	buf->walign (8);
    if (mach_bo == data_bo) {
		buf->put8 (&ul);
    } else {
		COMM::ULongLong ul2;
		swap8 (&ul2, &ul);
		buf->put8 (&ul2);
    }
}

void
SRBP::CDREncoder::put_float (COMM::Float f) {
	buf->walign (4);
#ifdef HAVE_IEEE_FP
    if (mach_bo == data_bo) {
		buf->put4 (&f);
    } else {
		COMM::Float f2;
		swap4 (&f2, &f);
		buf->put4 (&f2);
    }
#else
    COMM::Octet b[4];
    srbp_float2ieee (b, f);
    if (mach_bo == data_bo) {
		buf->put4 (b);
    } else {
		COMM::Octet b2[4];
		swap4 (b2, b);
		buf->put4 (b2);
    }
#endif
}

void
SRBP::CDREncoder::put_double (COMM::Double d) {
	buf->walign (8);
#ifdef HAVE_IEEE_FP
    if (mach_bo == data_bo) {
		buf->put8 (&d);
    } else {
		COMM::Double d2;
		swap8 (&d2, &d);
		buf->put8 (&d2);
    }
#else
    COMM::Octet b[8];
    srbp_double2ieee (b, d);
    if (mach_bo == data_bo) {
		buf->put8 (b);
    } else {
		COMM::Octet b2[8];
		swap8 (b2, b);
		buf->put8 (b2);
    }
#endif
}

void
SRBP::CDREncoder::put_longdouble (COMM::LongDouble d)
{
	buf->walign (8);
#if defined(HAVE_IEEE_FP) && SIZEOF_LONG_DOUBLE == 16
    if (mach_bo == data_bo) {
		buf->put16 (&d);
    } else {
		COMM::LongDouble d2;
		swap16 (&d2, &d);
		buf->put16 (&d2);
    }
#else
    COMM::Octet b[16];
    srbp_ldouble2ieee (b, d);
    if (mach_bo == data_bo) {
		buf->put16 (b);
    } else {
		COMM::Octet b2[16];
		swap16 (b2, b);
		buf->put16 (b2);
    }
#endif
}

void
SRBP::CDREncoder::put_char (COMM::Char c)
{
	//if (!conv) {
    buf->put1 (&c);
    return;
	//}
	//conv->put_char (*this, c);
}

void
SRBP::CDREncoder::put_char_raw (COMM::Char c)
{
    buf->put1 (&c);
}

void
SRBP::CDREncoder::put_wchar (COMM::WChar c)
{
	//if (!conv) {
    buf->put (&c, sizeof (COMM::WChar));
    return;
	//}
	//conv->put_wchar (*this, c);
}

void
SRBP::CDREncoder::put_octet (COMM::Octet o) {
    buf->put1 (&o);
}

void
SRBP::CDREncoder::put_boolean (COMM::Boolean b) {
    buf->put1 (&b);
}

void
SRBP::CDREncoder::put_shorts (const COMM::Short *p, COMM::ULong l)
{
    buf->walign (2);
    if (mach_bo == data_bo) {
		buf->put (p, 2*l);
    } else {
		buf->resize (2*l);
		COMM::Short *d = (COMM::Short *)buf->wdata();
		for (COMM::Long i = l; --i >= 0; ++d, ++p)
			swap2 (d, p);
		buf->wseek_rel (2*l);
    }
}

void
SRBP::CDREncoder::put_ushorts (const COMM::UShort *p, COMM::ULong l)
{
    buf->walign (2);
    if (mach_bo == data_bo) {
		buf->put (p, 2*l);
    } else {
		buf->resize (2*l);
		COMM::UShort *d = (COMM::UShort *)buf->wdata();
		for (COMM::Long i = l; --i >= 0; ++d, ++p)
			swap2 (d, p);
		buf->wseek_rel (2*l);
    }
}

void
SRBP::CDREncoder::put_longs (const COMM::Long *p, COMM::ULong l)
{
    buf->walign (4);
    if (mach_bo == data_bo) {
		buf->put (p, 4*l);
    } else {
		buf->resize (4*l);
		COMM::Long *d = (COMM::Long *)buf->wdata();
		for (COMM::Long i = l; --i >= 0; ++d, ++p)
			swap4 (d, p);
		buf->wseek_rel (4*l);
    }
}

void
SRBP::CDREncoder::put_longlongs (const COMM::LongLong *p, COMM::ULong l)
{
    buf->walign (8);
    if (mach_bo == data_bo) {
		buf->put (p, 8*l);
    } else {
		buf->resize (8*l);
		COMM::LongLong *d = (COMM::LongLong *)buf->wdata();
		for (COMM::Long i = l; --i >= 0; ++d, ++p)
			swap8 (d, p);
		buf->wseek_rel (8*l);
    }
}

void
SRBP::CDREncoder::put_ulongs (const COMM::ULong *p, COMM::ULong l)
{
    buf->walign (4);
    if (mach_bo == data_bo) {
		buf->put (p, 4*l);
    } else {
		buf->resize (4*l);
		COMM::ULong *d = (COMM::ULong *)buf->wdata();
		for (COMM::Long i = l; --i >= 0; ++d, ++p)
			swap4 (d, p);
		buf->wseek_rel (4*l);
    }
}

void
SRBP::CDREncoder::put_ulonglongs (const COMM::ULongLong *p, COMM::ULong l)
{
    buf->walign (8);
    if (mach_bo == data_bo) {
		buf->put (p, 8*l);
    } else {
		buf->resize (8*l);
		COMM::ULongLong *d = (COMM::ULongLong *)buf->wdata();
		for (COMM::Long i = l; --i >= 0; ++d, ++p)
			swap8 (d, p);
		buf->wseek_rel (8*l);
    }
}

void
SRBP::CDREncoder::put_floats (const COMM::Float *p, COMM::ULong l)
{
    for (COMM::Long i = l; --i >= 0; ++p)
		put_float (*p);
}

void
SRBP::CDREncoder::put_doubles (const COMM::Double *p, COMM::ULong l)
{
    for (COMM::Long i = l; --i >= 0; ++p)
		put_double (*p);
}

void
SRBP::CDREncoder::put_longdoubles (const COMM::LongDouble *p, COMM::ULong l)
{
    for (COMM::Long i = l; --i >= 0; ++p)
		put_longdouble (*p);
}

void
SRBP::CDREncoder::put_chars (const COMM::Char *p, COMM::ULong l)
{
	//if (!conv) {
    buf->put (p, l);
    return;
	//}
	//conv->put_chars (*this, p, l);
}

void
SRBP::CDREncoder::put_chars_raw (const COMM::Char *p, COMM::ULong l) {
    buf->put (p, l);
}

void
SRBP::CDREncoder::put_wchars (const COMM::WChar *p, COMM::ULong l)
{
	//if (!conv) {
    buf->put (p, l * sizeof (COMM::WChar));
    return;
	//}
	//conv->put_wchars (*this, p, l);
}

void
SRBP::CDREncoder::put_booleans (const COMM::Boolean *p, COMM::ULong l)
{
    buf->put (p, l);
}

void
SRBP::CDREncoder::put_string (const char *s)
{
	//if (!conv) {
    COMM::ULong len = strlen (s) + 1;
    put_ulong (len);
    put_octets (s, len);
    return;
	//}
	
	//conv->put_string (*this, s, 0);
}

void
SRBP::CDREncoder::put_string_raw (const char *s)
{
    COMM::ULong len = strlen (s) + 1;
    put_ulong (len);
    put_octets (s, len);
}

void
SRBP::CDREncoder::put_wstring (const wchar_t *s)
{
    //if (!conv) {
	COMM::ULong len = xwcslen (s) + 1;
	
	put_ulong (len);
	while (len--) {
		put_ushort ((COMM::UShort) *s++);
	}
	return;
    //}
    //conv->put_wstring (*this, s, 0);
}

COMM::ByteOrder
SRBP::CDREncoder::byteorder () const {
    return data_bo;
}

void
SRBP::CDREncoder::byteorder (COMM::ByteOrder _bo) {
    data_bo = _bo;
}

COMM::ULong
SRBP::CDREncoder::max_alignment () const {
    return 8;
}


/**************************** CDRDecoder ******************************/


SRBP::CDRDecoder::CDRDecoder (): COMM::DataDecoder (new COMM::Buffer, TRUE) 
{
#ifdef HAVE_BYTEORDER_BE
    mach_bo = COMM::BigEndian;
#else
    mach_bo = COMM::LittleEndian;
#endif
    data_bo = mach_bo;
}

SRBP::CDRDecoder::CDRDecoder (COMM::Buffer *b, COMM::Boolean dofree_b,
							  COMM::ByteOrder _bo)
							  : COMM::DataDecoder (b, dofree_b) {
#ifdef HAVE_BYTEORDER_BE
    mach_bo = COMM::BigEndian;
#else
    mach_bo = COMM::LittleEndian;
#endif
    data_bo = (_bo == COMM::DefaultEndian) ? mach_bo : _bo;
}

SRBP::CDRDecoder::~CDRDecoder ()
{
}

COMM::Boolean
SRBP::CDRDecoder::get_short (COMM::Short &s) {
	if (!buf->ralign (2) || !check_chunk ()) {
		return FALSE;
	}
	
    if (data_bo == mach_bo)
		return buf->get2 (&s);
	
    COMM::Short s2;
    if (!buf->get2 (&s2))
		return FALSE;
    swap2 (&s, &s2);
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_ushort (COMM::UShort &us) {
	if (!buf->ralign (2) || !check_chunk ()) {
		return FALSE;
	}
	
    if (data_bo == mach_bo)
		return buf->get2 (&us);
	
    COMM::UShort us2;
    if (!buf->get2 (&us2))
		return FALSE;
    swap2 (&us, &us2);
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_long (COMM::Long &l) {
	if (!buf->ralign (4) || !check_chunk ()) {
		return FALSE;
	}
	
    if (data_bo == mach_bo)
		return buf->get4 (&l);
	
    COMM::Long l2;
    if (!buf->get4 (&l2))
		return FALSE;
    swap4 (&l, &l2);
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_longlong (COMM::LongLong &l) {
	if (!buf->ralign (8) || !check_chunk ()) {
		return FALSE;
	}
	
    if (data_bo == mach_bo)
		return buf->get8 (&l);
	
    COMM::LongLong l2;
    if (!buf->get8 (&l2))
		return FALSE;
    swap8 (&l, &l2);
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_ulong (COMM::ULong &ul) 
{
	if (!buf->ralign (4) || !check_chunk ()) 
	{
		return FALSE;
	}
	
    if (data_bo == mach_bo)
		return buf->get4 (&ul);
	
    COMM::ULong ul2;
    if (!buf->get4 (&ul2))
		return FALSE;
    swap4 (&ul, &ul2);
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_ulonglong (COMM::ULongLong &ul) {
	if (!buf->ralign (8) || !check_chunk ()) {
		return FALSE;
	}
	
    if (data_bo == mach_bo)
		return buf->get8 (&ul);
	
    COMM::ULongLong ul2;
    if (!buf->get8 (&ul2))
		return FALSE;
    swap8 (&ul, &ul2);
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_float (COMM::Float &f) {
	if (!buf->ralign (4) || !check_chunk ()) {
		return FALSE;
	}
	
#ifdef HAVE_IEEE_FP
    if (data_bo == mach_bo)
		return buf->get4 (&f);
	
    COMM::Float f2;
    if (!buf->get4 (&f2))
		return FALSE;
    swap4 (&f, &f2);
#else
    COMM::Octet b[4];
    if (data_bo == mach_bo) {
		if (!buf->get4 (b))
			return FALSE;
		srbp_ieee2float (b, f);
		return TRUE;
    }
	
    COMM::Octet b2[4];
    if (!buf->get4 (b2))
		return FALSE;
    swap4 (&b, &b2);
    srbp_ieee2float (b, f);
#endif
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_double (COMM::Double &d) {
	if (!buf->ralign (8) || !check_chunk ()) {
		return FALSE;
	}
	
#ifdef HAVE_IEEE_FP
    if (data_bo == mach_bo)
		return buf->get8 (&d);
	
    COMM::Double d2;
    if (!buf->get8 (&d2))
		return FALSE;
    swap8 (&d, &d2);
#else
    COMM::Octet b[8];
    if (data_bo == mach_bo) {
		if (!buf->get8 (b))
			return FALSE;
		srbp_ieee2double (b, d);
		return TRUE;
    }
	
    COMM::Octet b2[8];
    if (!buf->get8 (b2))
		return FALSE;
    swap8 (&b, &b2);
    srbp_ieee2double (b, d);
#endif
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_longdouble (COMM::LongDouble &d) {
	if (!buf->ralign (8) || !check_chunk ()) {
		return FALSE;
	}
	
#if defined(HAVE_IEEE_FP) && SIZEOF_LONG_DOUBLE == 16
    if (data_bo == mach_bo)
		return buf->get16 (&d);
	
    COMM::LongDouble d2;
    if (!buf->get16 (&d2))
		return FALSE;
    swap16 (&d, &d2);
#else
    COMM::Octet b[16];
    if (data_bo == mach_bo) {
		if (!buf->get16 (b))
			return FALSE;
		srbp_ieee2ldouble (b, d);
		return TRUE;
    }
	
    COMM::Octet b2[16];
    if (!buf->get16 (b2))
		return FALSE;
    swap16 (&b, &b2);
    srbp_ieee2ldouble (b, d);
#endif
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_char (COMM::Char &c) {
	//if (!conv) {
    return buf->get1 (&c);
	//}
	
	//return conv->get_char (*this, c);
}

COMM::Boolean
SRBP::CDRDecoder::get_char_raw (COMM::Char &c) {
    return buf->get1 (&c);
}

COMM::Boolean
SRBP::CDRDecoder::get_wchar (COMM::WChar &c) {
	//if (!conv) {
    return buf->get (&c, sizeof (COMM::WChar));
	//}
	
	//return conv->get_wchar (*this, c);
}

COMM::Boolean
SRBP::CDRDecoder::get_octet (COMM::Octet &o) 
{
    return buf->get1 (&o);
}

COMM::Boolean
SRBP::CDRDecoder::get_boolean (COMM::Boolean &b) {
    return buf->get1 (&b);
}

COMM::Boolean
SRBP::CDRDecoder::get_shorts (COMM::Short *p, COMM::ULong l) {
    if (!buf->ralign (2) || !check_chunk())
		return FALSE;
	
    if (data_bo == mach_bo)
		return buf->get (p, 2*l);
	
    if (buf->length() < 2*l)
		return FALSE;
	
    COMM::Short *s = (COMM::Short *)buf->data();
    for (COMM::Long i = l; --i >= 0; ++p, ++s)
		swap2 (p, s);
    buf->rseek_rel (2*l);
	
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_ushorts (COMM::UShort *p, COMM::ULong l) {
    if (!buf->ralign (2) || !check_chunk ())
		return FALSE;
	
    if (data_bo == mach_bo)
		return buf->get (p, 2*l);
	
    if (buf->length() < 2*l)
		return FALSE;
	
    COMM::UShort *s = (COMM::UShort *)buf->data();
    for (COMM::Long i = l; --i >= 0; ++p, ++s)
		swap2 (p, s);
    buf->rseek_rel (2*l);
	
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_longs (COMM::Long *p, COMM::ULong l) {
    if (!buf->ralign (4) || !check_chunk ())
		return FALSE;
	
    if (data_bo == mach_bo)
		return buf->get (p, 4*l);
	
    if (buf->length() < 4*l)
		return FALSE;
	
    COMM::Long *s = (COMM::Long *)buf->data();
    for (COMM::Long i = l; --i >= 0; ++p, ++s)
		swap4 (p, s);
    buf->rseek_rel (4*l);
	
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_longlongs (COMM::LongLong *p, COMM::ULong l) {
    if (!buf->ralign (8) || !check_chunk ())
		return FALSE;
	
    if (data_bo == mach_bo)
		return buf->get (p, 8*l);
	
    if (buf->length() < 8*l)
		return FALSE;
	
    COMM::LongLong *s = (COMM::LongLong *)buf->data();
    for (COMM::Long i = l; --i >= 0; ++p, ++s)
		swap8 (p, s);
    buf->rseek_rel (8*l);
	
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_ulongs (COMM::ULong *p, COMM::ULong l) {
    if (!buf->ralign (4) || !check_chunk ())
		return FALSE;
	
    if (data_bo == mach_bo)
		return buf->get (p, 4*l);
	
    if (buf->length() < 4*l)
		return FALSE;
	
    COMM::ULong *s = (COMM::ULong *)buf->data();
    for (COMM::Long i = l; --i >= 0; ++p, ++s)
		swap4 (p, s);
    buf->rseek_rel (4*l);
	
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_ulonglongs (COMM::ULongLong *p, COMM::ULong l) {
    if (!buf->ralign (8) || !check_chunk ())
		return FALSE;
	
    if (data_bo == mach_bo)
		return buf->get (p, 8*l);
	
    if (buf->length() < 8*l)
		return FALSE;
	
    COMM::ULongLong *s = (COMM::ULongLong *)buf->data();
    for (COMM::Long i = l; --i >= 0; ++p, ++s)
		swap8 (p, s);
    buf->rseek_rel (8*l);
	
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_floats (COMM::Float *p, COMM::ULong l) {
    for (COMM::Long i = l; --i >= 0; ++p) {
		if (!get_float (*p))
			return FALSE;
    }
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_doubles (COMM::Double *p, COMM::ULong l) {
    for (COMM::Long i = l; --i >= 0; ++p) {
		if (!get_double (*p))
			return FALSE;
    }
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_longdoubles (COMM::LongDouble *p, COMM::ULong l) {
    for (COMM::Long i = l; --i >= 0; ++p) {
		if (!get_longdouble (*p))
			return FALSE;
    }
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_chars (COMM::Char *p, COMM::ULong l) {
	//if (!conv) {
    return buf->get (p, l);
	//}
	//return conv->get_chars (*this, p, l);
}

COMM::Boolean
SRBP::CDRDecoder::get_chars_raw (COMM::Char *p, COMM::ULong l) 
{
    return buf->get (p, l);
}

COMM::Boolean
SRBP::CDRDecoder::get_wchars (COMM::WChar *p, COMM::ULong l) {
	//if (!conv) {
    return buf->get (p, l * sizeof (COMM::WChar));
	//}
	//return conv->get_wchars (*this, p, l);
}

COMM::Boolean
SRBP::CDRDecoder::get_booleans (COMM::Boolean *p, COMM::ULong l) {
    return buf->get (p, l);
}

COMM::Boolean
SRBP::CDRDecoder::get_string (COMM::String_out s) {
	//if (!conv) {
    COMM::ULong len;
    if (!get_ulong (len))
        return FALSE;
    if (len == 0 || len > buf->length())
		return FALSE;
	
    char * p = COMM::string_alloc (len-1);
    if (!buf->get (p, len)) {
		COMM::string_free (p);
		return FALSE;
    }
    s = p;
    return TRUE;
	//}
	
	//return conv->get_string (*this, s, 0);
}

COMM::Boolean
SRBP::CDRDecoder::get_string_raw (COMM::String_out s) {
    COMM::ULong len;
    if (!get_ulong (len))
        return FALSE;
    if (len == 0 || len > buf->length())
		return FALSE;
	
    s = COMM::string_alloc (len-1);
    if (!buf->get (s.ptr(), len)) {
		COMM::string_free (s);
		return FALSE;
    }
    if (s[len-1] != 0) {
		COMM::string_free (s);
		return FALSE;
    }
    return TRUE;
}

COMM::Boolean
SRBP::CDRDecoder::get_wstring (COMM::WString_out s) {
	//if (!conv) {
    COMM::ULong len;
    if (!get_ulong (len))
        return FALSE;
    if (len == 0 || len*sizeof(COMM::UShort) > buf->length())
		return FALSE;
	
    COMM::WChar * p = COMM::wstring_alloc (len-1);
    COMM::UShort u;
    COMM::ULong i;
	
    for (i=0; i<len; i++) {
		if (!get_ushort (u)) {
			COMM::wstring_free (p);
			return FALSE;
		}
		p[i] = u;
    }
	
    if (p[len-1] != 0) {
		COMM::wstring_free (p);
		return FALSE;
    }
	
    s = p;
    return TRUE;
	//}
	
	//return conv->get_wstring (*this, s, 0);
}

COMM::DataDecoder *
SRBP::CDRDecoder::clone () const {
    return new CDRDecoder (new COMM::Buffer (*buf), TRUE, data_bo);
}

COMM::DataDecoder *
SRBP::CDRDecoder::clone (COMM::Buffer *b, COMM::Boolean dofree_b) const {
    return new CDRDecoder (b, dofree_b, data_bo);
}

COMM::DataEncoder *
SRBP::CDRDecoder::encoder () const {
	return new CDREncoder (new COMM::Buffer (*buf), TRUE, data_bo);
}

COMM::DataEncoder *
SRBP::CDRDecoder::encoder (COMM::Buffer *b, COMM::Boolean dofree_b) const {
    return new CDREncoder (b, dofree_b, data_bo);
}

COMM::ByteOrder
SRBP::CDRDecoder::byteorder () const {
    return data_bo;
}

void
SRBP::CDRDecoder::byteorder (COMM::ByteOrder _bo)
{
    data_bo = _bo;
}

COMM::ULong
SRBP::CDRDecoder::max_alignment () const {
    return 8;
}


