
#include <COMM.h>
#ifndef _WINDOWS
#include <string.h>
#endif
#include <errno.h>
#include <srbp/impl.h>
#include <srbp/template_impl.h>
#include <srbp/util.h>
#include <srbp/os-misc.h>
#include <srbp/os-net.h>

#ifdef _WINDOWS
extern CRITICAL_SECTION handle_mut;
#else
#include <pthread.h>
//extern pthread_mutex_t handle_mut = PTHREAD_MUTEX_INITIALIZER;
#endif

int handle_running = 0;


/**************************** misc dtors *******************************/


COMM::Dispatcher::~Dispatcher ()
{
}

COMM::DispatcherCallback::~DispatcherCallback ()
{
}


/****************************** Timeout *********************************/


COMM::Timeout::Timeout (Dispatcher *d, Long tm)
: _ready (tm == 0), _disp (d), _have_tmout (FALSE)
{
    if (tm > 0) {
		_have_tmout = TRUE;
		_disp->tm_event (this, tm);
    }
}

COMM::Timeout::~Timeout ()
{
    if (_have_tmout)
		_disp->remove (this, Dispatcher::Timer);
}

void
COMM::Timeout::callback (Dispatcher *d, Event ev)
{
    switch (ev) {
    case COMM::Dispatcher::Timer:
		_have_tmout = FALSE;
        _ready = TRUE;
		break;
		
    case COMM::Dispatcher::Moved:
		_disp = d;
		break;
    }
}


/************************** SelectDispatcher *****************************/

class SignalBlocker {
    OSMisc::SigState _sigs;
    bool _blocked;
public:
    void block ()
    {
		if (!_blocked) {
			_blocked = true;
			OSMisc::block_sigs (_sigs);
		}
    }
    void unblock ()
    {
		if (_blocked) {
			_blocked = false;
			OSMisc::restore_sigs (_sigs);
		}
    }
    SignalBlocker ()
    {
		_blocked = false;
		block();
    }
    ~SignalBlocker ()
    {
		unblock();
    }
};

SRBP::SelectDispatcher::SelectDispatcher (): last_update (0), init (TRUE), locked (0), modified (FALSE) 
{
	FD_ZERO (&curr_wset);
	FD_ZERO (&curr_rset);
	FD_ZERO (&curr_xset);
	fd_max = 0;
}

SRBP::SelectDispatcher::~SelectDispatcher () {
	list<FileEvent>::iterator i;
	for (i = fevents.begin(); i != fevents.end(); ++i)
		(*i).cb->callback (this, Remove);
	
	list<TimerEvent>::iterator j;
	for (j = tevents.begin(); j != tevents.end(); ++j)
		(*j).cb->callback (this, Remove);
}

COMM::Long
SRBP::SelectDispatcher::gettime () const {
	OSMisc::TimeVal ct = OSMisc::gettime();
	return ct.tv_sec*1000+ct.tv_usec/1000;
}

void
SRBP::SelectDispatcher::sleeptime (OSMisc::TimeVal &tm) 
{
	if (tevents.size() == 0) 
	{
		// wdh: changed sleeptime to 1 second
		tm.tv_sec =  1;
		tm.tv_usec = 0;
		return;
	}
	update_tevents();
	COMM::Long t = (tevents.front().delta > 0 ? tevents.front().delta : 0);
	
	tm.tv_sec = t / 1000L;
	tm.tv_usec = (t % 1000L) * 1000L;
}

void
SRBP::SelectDispatcher::update_fevents () 
{
	modified = TRUE;
	
	FD_ZERO (&curr_rset);
	FD_ZERO (&curr_wset);
	FD_ZERO (&curr_xset);
	fd_max = 0;
	
	list<FileEvent>::iterator i;
	for (i = fevents.begin(); i != fevents.end(); ++i) 
	{
		if (!(*i).deleted) 
		{
			switch ((*i).event) 
			{
			case Read:
				FD_SET ((*i).fd, &curr_rset);
				break;
			case Write:
				FD_SET ((*i).fd, &curr_wset);
				break;
			case Except:
				FD_SET ((*i).fd, &curr_xset);
				break;
			default:
				assert (0);
			}
			if ((*i).fd > fd_max)
				fd_max = (*i).fd;
		}
	}
}

void
SRBP::SelectDispatcher::update_tevents () 
{
	COMM::Long curr = gettime();
	if (init || tevents.size() == 0 || curr - last_update < 0) 
	{
		last_update = curr;
		init = FALSE;
	} else {
		tevents.front().delta -= (curr - last_update);
		last_update = curr;
	}
}

void
SRBP::SelectDispatcher::handle_tevents () 
{
	SignalBlocker __sb;
	
	if (tevents.size() == 0)
		return;
	
	update_tevents ();
	while (tevents.size() > 0 && tevents.front().delta <= 0) 
	{
		TimerEvent t = tevents.front();
		tevents.pop_front();
		if (tevents.size() > 0)
			tevents.front().delta += t.delta;
		
		__sb.unblock();
		t.cb->callback (this, t.event);
		__sb.block();
		
		update_tevents ();
	}
}

void
SRBP::SelectDispatcher::handle_fevents (FDSet &rset, FDSet &wset, FDSet &xset) 
{
#ifdef _WINDOWS 
	EnterCriticalSection(&handle_mut);
#else
	pthread_mutex_lock(&handle_mut);
#endif
	
	handle_running = 1;
	// already signal safe because of lock()/unlock()
	
	lock ();
	list<FileEvent>::iterator i;
	
	for (i = fevents.begin(); i != fevents.end(); ++i) 
	{
		if (!(*i).deleted) 
		{
			switch ((*i).event)  //type == FileEvent
			{
			case Read:
				if (FD_ISSET ((*i).fd, &rset))  //������ �ִٸ� 
					(*i).cb->callback (this, Read);
				break;
			case Write:
				if (FD_ISSET ((*i).fd, &wset))
					(*i).cb->callback (this, Write);
				break;
			case Except:
				if (FD_ISSET ((*i).fd, &xset))
					(*i).cb->callback (this, Except);
				break;
			default:
				assert (0);
			}
		}
	}
	
	unlock ();
	
	handle_running = 0;
	
#ifdef _WINDOWS 
	LeaveCriticalSection(&handle_mut);
#else
	pthread_mutex_unlock(&handle_mut);
#endif
}

void
SRBP::SelectDispatcher::lock () 
{
	if (!locked)
		modified = FALSE;
	++locked;
}

void
SRBP::SelectDispatcher::unlock () {
	if (--locked > 0)
		return;
	assert (locked == 0);
	
	if (modified) {
		list<FileEvent>::iterator i;
		bool again;
		
		do {
			again = false;
			for (i = fevents.begin(); i != fevents.end(); i++) {
				if ((*i).deleted) {
					fevents.erase (i);
					again = true;
					break;
				}
			}
		} while (again);
	}
}

COMM::Boolean
SRBP::SelectDispatcher::islocked () const
{
    return locked > 0;
}

void
SRBP::SelectDispatcher::rd_event (COMM::DispatcherCallback *cb,COMM::Long fd) 
{
	SignalBlocker __sb;
	
	fevents.push_back (FileEvent (Read, fd, cb));
	update_fevents ();
}

void
SRBP::SelectDispatcher::wr_event (COMM::DispatcherCallback *cb,
								  COMM::Long fd) {
	SignalBlocker __sb;
	
	fevents.push_back (FileEvent (Write, fd, cb));
	update_fevents ();
}

void
SRBP::SelectDispatcher::ex_event (COMM::DispatcherCallback *cb,
								  COMM::Long fd) {
	SignalBlocker __sb;
	
	fevents.push_back (FileEvent (Except, fd, cb));
	update_fevents ();
}

void
SRBP::SelectDispatcher::tm_event (COMM::DispatcherCallback *cb,
								  COMM::ULong tmout)
{
    SignalBlocker __sb;
	
    assert ((COMM::Long)tmout >= 0);
    TimerEvent t (Timer, tmout, cb);
	
    update_tevents ();
    list<TimerEvent>::iterator i;
    for (i = tevents.begin(); i != tevents.end(); ++i) {
		if ((*i).delta <= t.delta) {
			t.delta -= (*i).delta;
		} else {
			(*i).delta -= t.delta;
			break;
		}
    }
    tevents.insert (i, t);
}

void
SRBP::SelectDispatcher::remove (COMM::DispatcherCallback *cb, Event e) {
	SignalBlocker __sb;
	
	if (e == All || e == Timer) {
		list<TimerEvent>::iterator i, next;
		bool again;
		
		do {
			again = false;
			for (i = tevents.begin(); i != tevents.end(); i++) {
				next = i;
				++next;
				if ((*i).cb == cb) {
					COMM::Long delta = (*i).delta;
					if (next != tevents.end())
						(*next).delta += delta;
					tevents.erase (i);
					again = true;
					break;
				}
			}
		} while (again);
	}
	
	if (e == All || e == Read || e == Write || e == Except) {
		list<FileEvent>::iterator i;
		bool again;
		
		do {
			again = false;
			for (i = fevents.begin(); i != fevents.end(); i++) {
				if ((*i).cb == cb && (e == All || 
					(*i).event == e)) {
					if (islocked()) {
						(*i).deleted = TRUE;
					} else {
						fevents.erase (i);
						again = true;
						break;
					}
				}
			}
		} while (again);
		
		update_fevents ();
	}
}

int select_running = 0;

void
SRBP::SelectDispatcher::run (COMM::Boolean infinite) 
{
	FDSet rset, wset, xset;
	OSMisc::TimeVal tm;
	
	do {
		
#if 0
#ifdef _WINDOWS 
		EnterCriticalSection(&select_mut);
#else
		pthread_mutex_lock(&select_mut);
#endif
#endif
		
		select_running = getpid(); //���� Process ID 
		
RESTART :
		{
			SignalBlocker __sb;
			
			rset = curr_rset;		//SRBInit():: Init���� rd_Event���� ���� 
			wset = curr_wset;
			xset = curr_xset;
			sleeptime (tm);
		}
		
		errno = 0;
		
		int r = ::select (fd_max+1,(select_addr_t)&rset,(select_addr_t)&wset,(select_addr_t)&xset,&tm);
		
		if(r < 0) goto RESTART; // EBADF(errno=9) : when callback between svrs in SunOS 5.8
		
		assert (r >= 0 || errno == EINTR || errno == EAGAIN ||errno == EWOULDBLOCK);
		
		
		if (r < 0 && errno == EINTR) goto RESTART; 
		
		if (r > 0)		//������ �ִٸ� 
			handle_fevents (rset, wset, xset);
		
		handle_tevents ();
		
		select_running = 0;
		
#if 0
#ifdef _WINDOWS
		LeaveCriticalSection(&select_mut);
#else
		pthread_mutex_unlock(&select_mut);
#endif
#endif
		
	} while (infinite);
}

void
SRBP::SelectDispatcher::move (COMM::Dispatcher *disp)
{
    SignalBlocker __sb;
	
    assert (!islocked());
	
    list<FileEvent>::iterator i;
    for (i = fevents.begin(); i != fevents.end(); ++i) {
		switch ((*i).event) {
		case Read:
            (*i).cb->callback (disp, COMM::Dispatcher::Moved);
			disp->rd_event ((*i).cb, (*i).fd);
			break;
		case Write:
            (*i).cb->callback (disp, COMM::Dispatcher::Moved);
			disp->wr_event ((*i).cb, (*i).fd);
			break;
		case Except:
            (*i).cb->callback (disp, COMM::Dispatcher::Moved);
			disp->ex_event ((*i).cb, (*i).fd);
			break;
		default:
			break;
		}
    }
    fevents.erase (fevents.begin(), fevents.end());
    update_fevents ();
	
    update_tevents ();
    COMM::Long tmout = 0;
    list<TimerEvent>::iterator j;
    for (j = tevents.begin(); j != tevents.end(); ++j) {
		tmout += (*j).delta;
		if (tmout < 0)
			tmout = 0;
        (*j).cb->callback (disp, COMM::Dispatcher::Moved);
		disp->tm_event ((*j).cb, tmout);
    }
    tevents.erase (tevents.begin(), tevents.end());
}

COMM::Boolean
SRBP::SelectDispatcher::idle () const
{
	SignalBlocker __sb;
	FDSet rset, wset, xset;
	OSMisc::TimeVal tm;
	
	/*
	* Any pending file events?
	*/
	
	if (fevents.size() > 0) {
		rset = curr_rset;
		wset = curr_wset;
		xset = curr_xset;
		tm.tv_sec = 0;
		tm.tv_usec = 0;
		
		int r = ::select (fd_max+1,
			(select_addr_t)&rset,
			(select_addr_t)&wset,
			(select_addr_t)&xset,
			&tm);
		assert (r >= 0 || errno == EINTR || errno == EAGAIN ||
            errno == EWOULDBLOCK);
		
		if (r > 0) {
			return FALSE;
		}
	}
	
	/*
	* No? Then what about pending timer events?
	*/
	
	if (tevents.size()) {
		// Discard const for update_tevents()
		((SelectDispatcher *) this)->update_tevents ();
		if (tevents.front().delta <= 0) {
			return FALSE;
		}
	}
	
	/*
	* Then we're idle ...
	*/
	
	return TRUE;
}
