
#include <COMM.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#ifdef HAVE_DLFCN_H
#include <dlfcn.h>
#endif
#ifdef HAVE_DL_H
#include <dl.h>
#endif
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <srbp/impl.h>
#include <srbp/template_impl.h>
#include <srbp/util.h>
#include <srbp/os-math.h>


/****************************** OSMath ********************************/


SRBP_Double OSMath::_infinity = 0;
SRBP_LongDouble OSMath::_long_infinity = 0;
SRBP_Double OSMath::_notanumber = 0;
SRBP_LongDouble OSMath::_long_notanumber = 0;


/**************************** UnixProcess *****************************/


#ifdef HAVE_NAMESPACE
namespace SRBP {
  SRBP::UnixProcess::ListProcess UnixProcess::_procs;
};
#else
SRBP::UnixProcess::ListProcess SRBP::UnixProcess::_procs;
#endif

void
SRBP::UnixProcess::signal_handler (int sig)
{
    int status;
    while (42) {
	COMM::Long pid = ::waitpid (-1, &status, WNOHANG);
        if (pid < 0 && errno == EINTR)
            continue;
	if (pid <= 0)
	    break;

	ListProcess::iterator i;
	for (i = _procs.begin(); i != _procs.end(); ++i) {
	    if (pid == (*i)->_pid) {
		if (WIFEXITED ((status))) {
		    (*i)->_exit_status = WEXITSTATUS ((status));
		} else {
		    (*i)->_exit_status = 1000;
		}
		if ((*i)->_cb) {
/*TODO truevoid
		    COMM::SRB_ptr srb = COMM::SRB_instance("srbp-local-srb");
		    COMM::Dispatcher *disp = srb->dispatcher();
		    disp->remove (*i, COMM::Dispatcher::Timer);
		    disp->tm_event (*i, 0);
*/
		}
		break;
	    }
	}
    }
    ::signal (SIGCHLD, signal_handler);
}

SRBP::UnixProcess::UnixProcess (const char *cmd, SRBP::ProcessCallback *cb)
{
    _exit_status = -1;
    _pid = 0;
    _detached = FALSE;
    _cb = cb;
    _procs.push_back (this);
    _args = cmd;
}

SRBP::UnixProcess::~UnixProcess ()
{
	cout<<"~UnixProcess is called"<<endl;
    for (ListProcess::iterator pos = _procs.begin(); pos != _procs.end(); ++pos) {
	if (*pos == this) {
	    _procs.erase (pos);
	    if (!_detached && !exited())
		terminate ();
	    return;
	}
    }
    assert (0);
}

COMM::Boolean
SRBP::UnixProcess::run ()
{
    ::signal (SIGCHLD, signal_handler);

    _pid = ::fork ();
    if (_pid == 0) {
	string command;
#ifndef __CYGWIN32__
	// with Cygwin32 using "exec" doesnt work for some strange reason
	command = "exec ";
#endif
	command += _args;
	::execl ("/bin/sh", "/bin/sh", "-c", command.c_str(), NULL);
	exit (1);
    }
    return _pid > 0;
}

COMM::Boolean
SRBP::UnixProcess::exited ()
{
    return _pid <= 0 || _exit_status >= 0;
}

COMM::Boolean
SRBP::UnixProcess::exit_status ()
{
    return _exit_status == 0;
}

void
SRBP::UnixProcess::terminate ()
{
    assert (_pid > 0);
    ::kill (_pid, SIGTERM);
}

void
SRBP::UnixProcess::detach ()
{
    _detached = TRUE;
    _cb = 0;
}

SRBP::UnixProcess::operator COMM::Boolean ()
{
    return _pid > 0;
}

void
SRBP::UnixProcess::callback (COMM::Dispatcher *disp,
			     COMM::DispatcherCallback::Event ev)
{
    if (ev == COMM::Dispatcher::Timer && _cb)
	_cb->callback (this, SRBP::ProcessCallback::Exited);
}


/*************************** UnixSharedLib ****************************/


#if defined(HAVE_DLOPEN) && defined(HAVE_DYNAMIC)

#ifndef RTLD_NOW
#define RTLD_NOW 1
#endif

#ifndef RTLD_GLOBAL
#define RTLD_GLOBAL 0
#endif

SRBP::UnixSharedLib::UnixSharedLib (const char *name)
{
    _name = name;
    _handle = ::dlopen (name, RTLD_NOW|RTLD_GLOBAL);
}

SRBP::UnixSharedLib::~UnixSharedLib ()
{
    if (_handle)
	::dlclose (_handle);
}

void *
SRBP::UnixSharedLib::symbol (const char *sym)
{
    assert (_handle);
    return ::dlsym (_handle, (char *)sym);
}

const char *
SRBP::UnixSharedLib::error ()
{
    const char *err = ::dlerror ();
    if (err)
	_error = err;
    return _error.c_str();
}

SRBP::UnixSharedLib::operator COMM::Boolean ()
{
    return !!_handle;
}

const char *
SRBP::UnixSharedLib::name ()
{
    return _name.c_str();
}

#elif defined(HAVE_SHL_LOAD) && defined(HAVE_DYNAMIC)

SRBP::UnixSharedLib::UnixSharedLib (const char *name)
{
    _name = name;
    _handle = ::shl_load (name, BIND_IMMEDIATE, 0L);
}

SRBP::UnixSharedLib::~UnixSharedLib ()
{
    if (_handle)
	::shl_unload ((shl_t)_handle);
}

void *
SRBP::UnixSharedLib::symbol (const char *sym)
{
    assert (_handle);

    void *value;
    if (::shl_findsym ((shl_t *)&_handle, (char *)sym, TYPE_PROCEDURE, 
		       &value) < 0) {
	string _sym = "_";
	_sym += sym;
	if (::shl_findsym ((shl_t *)&_handle, (char *)_sym.c_str(),
			   TYPE_PROCEDURE, &value) < 0)
	    return 0;
    }
    return value;
}

const char *
SRBP::UnixSharedLib::error ()
{
    _error = strerror (errno);
    return _error.c_str();
}

SRBP::UnixSharedLib::operator COMM::Boolean ()
{
    return !!_handle;
}

const char *
SRBP::UnixSharedLib::name ()
{
    return _name.c_str();
}

#else

SRBP::UnixSharedLib::UnixSharedLib (const char *name)
{
    _name = name;
    _handle = 0;
}

SRBP::UnixSharedLib::~UnixSharedLib ()
{
}

void *
SRBP::UnixSharedLib::symbol (const char *sym)
{
    return 0;
}

const char *
SRBP::UnixSharedLib::error ()
{
    return "no shlib support";
}

SRBP::UnixSharedLib::operator COMM::Boolean ()
{
    return FALSE;
}

const char *
SRBP::UnixSharedLib::name ()
{
    return _name.c_str();
}

#endif
