
#include <COMM.h>
#include <string.h>
#include <errno.h>
#include <srbp/os-net.h>
#include <srbp/impl.h>
#include <srbp/template_impl.h>
#include <srbp/util.h>
#define DUMP_HALF_LEN	512

static void 
dump_data2(char *buf, int start, int len)
{
	int i, j;
	
	for (i = 0; i < len; i += 16)
	{
		printf("%04X: ", i + start);
		for (j = 0; j < 16; j++)
		{
			if (i+j >= len)
				printf("   ");
			else
				printf("%02X ", (unsigned char)buf[i+j]);
		}
		for (j = 0; j < 16; j++)
		{
			if (i+j >= len)
				printf(" ");
			else if(isprint(buf[i+j]))
				printf("%c", buf[i+j]);
			else
				printf(".");
		}
		printf("\n");
	}
}
static void 
dump_data(char *buf, int len)
{
	int i;
	
	if (len <= DUMP_HALF_LEN*2)
		dump_data2(buf, 0, len);
	else {
		dump_data2(buf, 0, DUMP_HALF_LEN);
		printf("\n");
		i = len - DUMP_HALF_LEN;
		i = (i / 16 + 1) * 16;
		dump_data2((char *)(buf + i), i, len - i);
	}
}


/**************************** misc dtors *******************************/


COMM::TransportCallback::~TransportCallback ()
{
}

COMM::TransportServer::~TransportServer ()
{
}

COMM::TransportServerCallback::~TransportServerCallback ()
{
}


/*************************** Transport *******************************/


COMM::Transport::~Transport ()
{
}

COMM::Long
COMM::Transport::read (Buffer &b, Long len)
{
    b.resize (len);
    Long r = read (b.buffer()+b.wpos(), len);
    if (r > 0)
		b.wseek_rel (r);	//읽어들인 만큼 _wptr을 이동 
    return r;
}

COMM::Long
COMM::Transport::write (Buffer &b, Long len, Boolean eat)
{
    assert (len <= b.length());
    Long r = write (b.buffer()+b.rpos(), len);
    if (r > 0 && eat)
		b.rseek_rel (r);
    return r;
}

void
COMM::Transport::buffering (COMM::Boolean)
{
}

COMM::Boolean
COMM::Transport::isbuffering ()
{
    return FALSE;
}
/*
COMM::Principal_ptr
COMM::Transport::get_principal ()
{
return new COMM::Principal (this);
}
*/

/************************** TCPTransport *****************************/


SRBP::TCPTransport::TCPTransport (COMM::Long thefd)
{
    fd = thefd;
    if (fd < 0) 
	{
		fd = ::socket (PF_INET, SOCK_STREAM, 0);
		assert (fd >= 0);
    }
    OSNet::sock_block (fd, TRUE);
    OSNet::sock_ndelay (fd, TRUE);
	
    is_blocking = TRUE;
    is_buffering = FALSE;
    rdisp = wdisp = 0;
    rcb = wcb = 0;
    ateof = FALSE;
}

SRBP::TCPTransport::~TCPTransport ()
{
    if (rdisp && rcb) {
		rdisp->remove (this, COMM::Dispatcher::Read);
		rdisp = 0;
		rcb->callback (this, COMM::TransportCallback::Remove);
    }
    if (wdisp && wcb) {
		wdisp->remove (this, COMM::Dispatcher::Write);
		wdisp = 0;
		wcb->callback (this, COMM::TransportCallback::Remove);
    }
    OSNet::sock_close (fd);
}

void
SRBP::TCPTransport::rselect (COMM::Dispatcher *disp,COMM::TransportCallback *cb)
{
    if (rcb && rdisp) 
	{
		rdisp->remove (this, COMM::Dispatcher::Read);
		rdisp = 0;
		rcb = 0;
    }
    if (cb) 
	{
		disp->rd_event (this, fd);
		rdisp = disp;
		rcb = cb;
    }
}

void
SRBP::TCPTransport::wselect (COMM::Dispatcher *disp,
							 COMM::TransportCallback *cb)
{
    if (wcb && wdisp) {
		wdisp->remove (this, COMM::Dispatcher::Write);
		wdisp = 0;
		wcb = 0;
    }
    if (cb) {
		disp->wr_event (this, fd);
		wdisp = disp;
		wcb = cb;
    }
}

void
SRBP::TCPTransport::callback (COMM::Dispatcher *disp,COMM::Dispatcher::Event ev) 
{
	switch (ev) 
	{
	case COMM::Dispatcher::Read:
		assert (rcb);
		rcb->callback (this, COMM::TransportCallback::Read);
		break;
	case COMM::Dispatcher::Write:
		assert (wcb);
		wcb->callback (this, COMM::TransportCallback::Write);
		break;
	case COMM::Dispatcher::Remove:
		wdisp = rdisp = 0;
		wcb = rcb = 0;
		break;
	case COMM::Dispatcher::Moved:
		wdisp = rdisp = disp;
		break;
	default:
		assert (0);
	}
}

COMM::Boolean
SRBP::TCPTransport::bind (const COMM::Address *a)
{
    assert (!strcmp (a->proto(), "inet"));
    InetAddress *ia = (InetAddress *)a;
	
    struct sockaddr_in sin = ia->sockaddr();
    COMM::Long r = ::bind (fd, (socket_addr_t)&sin, sizeof (sin));
    if (r < 0) {
        OSNet::set_errno();
		err = xstrerror (errno);
		return FALSE;
    }
    return TRUE;
}

COMM::Boolean
SRBP::TCPTransport::connect (const COMM::Address *a)
{
    assert (!strcmp (a->proto(), "inet"));
    InetAddress *ia = (InetAddress *)a;
	
    if (!ia->valid()) {
		err = "invalid address";
		return FALSE;
    }
	
    struct sockaddr_in sin = ia->sockaddr();
    COMM::Long r = ::connect (fd, (socket_addr_t)&sin, sizeof (sin));
    if (r < 0) {
        OSNet::set_errno();
		err = xstrerror (errno);
		return FALSE;
    }
    return TRUE;
}

void
SRBP::TCPTransport::close ()
{
    OSNet::sock_close (fd);
    fd = ::socket (PF_INET, SOCK_STREAM, 0);
    assert (fd >= 0);
	
    OSNet::sock_ndelay (fd, TRUE);
	
    if (rdisp && rcb)
		rdisp->remove (this, COMM::Dispatcher::Read);
    if (wdisp && wcb)
		wdisp->remove (this, COMM::Dispatcher::Write);
	
    is_buffering = FALSE;
    is_blocking = TRUE;
    rdisp = wdisp = 0;
    rcb = wcb = 0;
    ateof = FALSE;
}

void
SRBP::TCPTransport::block (COMM::Boolean doblock)
{
    if (!!is_blocking != !!doblock) {
		is_blocking = doblock;
		OSNet::sock_block (fd, doblock);
    }
}

COMM::Boolean
SRBP::TCPTransport::isblocking ()
{
    return is_blocking;
}

void
SRBP::TCPTransport::buffering (COMM::Boolean dobuffering)
{
    if (!!is_buffering != !!dobuffering) {
		is_buffering = dobuffering;
		OSNet::sock_ndelay (fd, !dobuffering);
    }
}

COMM::Boolean
SRBP::TCPTransport::isbuffering ()
{
    return is_buffering;
}

COMM::Boolean
SRBP::TCPTransport::isreadable ()
{
    fd_set rset;
    struct timeval tm;
	
    FD_ZERO (&rset);
    FD_SET (fd, &rset);
	
    tm.tv_sec = 0;
    tm.tv_usec = 0;
	
    int r = ::select (fd+1, (select_addr_t)&rset, 0, 0, &tm);
    return r > 0;
}

COMM::Long
SRBP::TCPTransport::read (void *_b, COMM::Long len)
{
    COMM::Long todo = len;
    COMM::Octet *b = (COMM::Octet *)_b;
	
    while (todo > 0) 
	{
		COMM::Long r = OSNet::sock_read (fd, b, todo);
		if (r < 0) 
		{
            OSNet::set_errno();
			if (errno == EINTR)
				continue;
            // Cygnus CDK sometimes returns errno 0 when read would block
			if (errno == 0 || errno == EWOULDBLOCK || errno == EAGAIN || todo != len)
				break;
			err = xstrerror (errno);
			return r;
		} 
		else if (r == 0) 
		{
			ateof = TRUE;
			break;
		}
		b += r; // 다음 Packet 부분 읽어들이기 위해 Pointer 이동 
		todo -= r;
    }
	//dump_data((char *)_b, (len - todo));
    return len - todo;
}

COMM::Long
SRBP::TCPTransport::write (const void *_b, COMM::Long len)
{
    COMM::Long todo = len;
    COMM::Octet *b = (COMM::Octet *)_b;
    while (todo > 0) {
		COMM::Long r = OSNet::sock_write (fd, b, todo);
		
		if (r < 0) {
            OSNet::set_errno();
			if (errno == EINTR)
				continue;
            // Cygnus CDK sometimes returns errno 0 when read would block
			if (errno == 0 || errno == EWOULDBLOCK || errno == EAGAIN ||
                todo != len)
				break;
			err = xstrerror (errno);
			
			return r;
		} else if (r == 0) {
			break;
		}
		b += r;
		todo -= r;
    }
	//dump_data((char*)_b, len - todo);
    return len - todo;
}

const COMM::Address *
SRBP::TCPTransport::addr ()
{
    struct sockaddr_in sin;
    socket_size_t sz = sizeof (sin);
    COMM::Long r = ::getsockname (fd, (socket_addr_t)&sin, &sz);
    if (r < 0) {
        OSNet::set_errno();
		err = xstrerror (errno);
		return 0;
    }
    local_addr.sockaddr (sin);
    return &local_addr;
}

const COMM::Address *
SRBP::TCPTransport::peer ()
{
    struct sockaddr_in sin;
    socket_size_t sz = sizeof (sin);
    COMM::Long r = ::getpeername (fd, (socket_addr_t)&sin, &sz);
    if (r < 0)
	{
        OSNet::set_errno();
		err = xstrerror (errno);
        // XXX allow for peer() after disconnect ...
		//return 0;
    } 
	else 
	{
        peer_addr.sockaddr (sin);
    }
    return &peer_addr;
}

COMM::Boolean
SRBP::TCPTransport::bad () const
{
    return err.length() > 0;
}

COMM::Boolean
SRBP::TCPTransport::eof () const
{
    return ateof;
}

string
SRBP::TCPTransport::errormsg () const
{
    return err;
}


/************************ TCPTransportServer **************************/


SRBP::TCPTransportServer::TCPTransportServer ()
{
    OSNet::sock_init();
	
    fd = ::socket (PF_INET, SOCK_STREAM, 0);
    assert (fd >= 0);
	
    is_blocking = TRUE;
	
    OSNet::sock_reuse (fd, TRUE);
	
    listening = FALSE;
    adisp = 0;
    acb = 0;
}

SRBP::TCPTransportServer::~TCPTransportServer ()
{
    if (adisp && acb) {
		adisp->remove (this, COMM::Dispatcher::Read);
		adisp = 0;
		acb->callback (this, COMM::TransportServerCallback::Remove);
    }
    OSNet::sock_close (fd);
}

void
SRBP::TCPTransportServer::listen ()
{
    if (!listening) 
	{
		int r = ::listen (fd, 10);
		assert (r == 0);
		listening = TRUE;
    }
}

void
SRBP::TCPTransportServer::aselect (COMM::Dispatcher *disp,
								   COMM::TransportServerCallback *cb)
{
    if (acb && adisp) 
	{
		adisp->remove (this, COMM::Dispatcher::Read);
		adisp = 0;
		acb = 0;
    }
    if (cb) 
	{
		listen ();
		disp->rd_event (this, fd);
		adisp = disp;
		acb = cb;
    }
}

void
SRBP::TCPTransportServer::callback (COMM::Dispatcher *disp,COMM::Dispatcher::Event ev)
{
    switch (ev) 
	{
    case COMM::Dispatcher::Read:
		assert (acb);
		acb->callback (this, COMM::TransportServerCallback::Accept);		//Server에게 반응이 왔다는 것은 Connetion Request일 경우이다. 
		break;
    case COMM::Dispatcher::Remove:
		acb = 0;
		adisp = 0;
		break;
    case COMM::Dispatcher::Moved:
        adisp = disp;
        break;
    default:
		assert (0);
    }
}

COMM::Boolean
SRBP::TCPTransportServer::bind (const COMM::Address *a)
{
    assert (!strcmp (a->proto(), "inet"));
    InetAddress *ia = (InetAddress *)a;
	
    struct sockaddr_in sin = ia->sockaddr();
    COMM::Long r = ::bind (fd, (socket_addr_t)&sin, sizeof (sin));
    if (r < 0) 
	{
        OSNet::set_errno();
		err = xstrerror (errno);
		return FALSE;
    }
    return TRUE;
}

void
SRBP::TCPTransportServer::close ()
{
    OSNet::sock_close (fd);
    fd = ::socket (PF_INET, SOCK_STREAM, 0);
    assert (fd >= 0);
	
    is_blocking = TRUE;
	
    OSNet::sock_reuse (fd, TRUE);
	
    if (adisp && acb)
		adisp->remove (this, COMM::Dispatcher::Read);
	
    listening = FALSE;
    adisp = 0;
    acb = 0;
}

void
SRBP::TCPTransportServer::block (COMM::Boolean doblock)
{
    if (!!is_blocking != !!doblock) 
	{
		is_blocking = doblock;
		OSNet::sock_block (fd, doblock);
    }
}

COMM::Boolean
SRBP::TCPTransportServer::isblocking ()
{
    return is_blocking;
}

COMM::Transport *
SRBP::TCPTransportServer::accept ()
{
    listen ();
    COMM::Long newfd = ::accept (fd, 0, 0);
    if (newfd < 0) 
	{
        OSNet::set_errno();
		if (errno != EWOULDBLOCK && errno != EAGAIN)
			err = xstrerror (errno);
		return 0;
    }
    return new TCPTransport (newfd); //TCPTransport Creation for a Client 
}

const COMM::Address *
SRBP::TCPTransportServer::addr ()
{
    struct sockaddr_in sin;
    socket_size_t sz = sizeof (sin);
    COMM::Long r = ::getsockname (fd, (socket_addr_t)&sin, &sz);
    if (r < 0) {
        OSNet::set_errno();
		err = xstrerror (errno);
		return 0;
    }
    local_addr.sockaddr (sin);
    return &local_addr;
}

COMM::Boolean
SRBP::TCPTransportServer::bad () const
{
    return err.length() > 0;
}

string
SRBP::TCPTransportServer::errormsg () const
{
    return err;
}
