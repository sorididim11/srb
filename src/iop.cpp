
#include <COMM.h>
#ifndef _WINDOWS
#include <string.h>
#include <stdio.h>
#endif
#ifdef HAVE_ANSI_CPLUSPLUS_HEADERS
#include <iostream> 
#else
#include <iostream.h>
#endif
#include <srbp/impl.h>
#include <srbp/template_impl.h>
#include <srbp/util.h>

#ifndef _WINDOWS
#include <netdb.h>
#include <netinet/in.h>
#endif

#include <srbp/srb.h>

extern int select_running;

#ifdef _WINDOWS
extern CRITICAL_SECTION gisp_conn_mut;
extern CRITICAL_SECTION iisp_p_ir_mut;
extern CRITICAL_SECTION iisp_s_ir_mut;
#else
#include <pthread.h>
#include <semaphore.h>
 
#endif

/**************************************************************************/


SRBP::GISPConnCallback::~GISPConnCallback ()
{
}

/***************************** GISPInContext *****************************/


SRBP::GISPInContext::GISPInContext (GISPCodec *codec, COMM::Buffer *buf)
 {
    _buf = buf;
    _delete_buf = FALSE;
    _dc = codec->dc_proto()->clone (buf, 1);
    _delete_dc = TRUE;
}

SRBP::GISPInContext::~GISPInContext () 
{
    if (_delete_buf)
		delete _buf;
    if (_delete_dc)
		delete _dc;
}

void
SRBP::GISPInContext::buffer (COMM::Buffer *b) 
{
    if (_delete_buf)
		delete _buf;
	
    _dc->buffer (b, FALSE);
    _buf = b;
    _delete_buf = FALSE;
}

COMM::DataDecoder *
SRBP::GISPInContext::_retn ()
{
    // assert (_delete_buf);
    assert (_delete_dc);
    _delete_buf = FALSE;
    _delete_dc = FALSE;
    return _dc;
}


/***************************** GISPOutContext *****************************/


SRBP::GISPOutContext::GISPOutContext (GISPCodec *codec) 
{
    _buf = new COMM::Buffer;
    _delete_buf = TRUE;
    _ec = codec->ec_proto()->clone (_buf, 0);
    _delete_ec = TRUE;
}

SRBP::GISPOutContext::GISPOutContext (COMM::DataEncoder *ec)
{
    _buf = ec->buffer();
    _delete_buf = FALSE;
    _ec = ec;
    _delete_ec = FALSE;
}

SRBP::GISPOutContext::~GISPOutContext () 
{
    if (_delete_buf)
		delete _buf;
    if (_delete_ec)
		delete _ec;
}

void
SRBP::GISPOutContext::reset() 
{
    _buf->reset();
}

COMM::Buffer *
SRBP::GISPOutContext::_retn()
 {
    assert (_delete_buf);
    _delete_buf = FALSE;
    return _buf;
}


/******************************* GISPCodec *******************************/


SRBP::GISPCodec::GISPCodec (COMM::DataDecoder *_dc,COMM::DataEncoder *_ec,COMM::UShort gisp_ver): _dc_proto(_dc), _ec_proto(_ec)
{
    _gisp_ver = gisp_ver;
	
    if (_gisp_ver > 0x0102) 
	{
		_gisp_ver = 0x0102;
    }
	
    GISPOutContext ctx (this);
    _size_offset = put_header (ctx, GISP::Request);
    _headerlen = ctx.ec()->buffer()->length();
}

SRBP::GISPCodec::~GISPCodec ()
{
    delete _ec_proto;
    delete _dc_proto;
}

COMM::ULong
SRBP::GISPCodec::put_header (GISPOutContext &out, GISP::MsgType mt) 
{
	COMM::ULong key;
	
	COMM::DataEncoder *ec = out.ec();
	
	ec->struct_begin();
	{
		ec->arr_begin();
		{
			ec->put_chars_raw ((COMM::Char *)"GISP", 4);
		}
		ec->arr_end();
		
		ec->struct_begin();
		{
			ec->put_octet ((COMM::Octet)(_gisp_ver >> 8));
			ec->put_octet ((COMM::Octet)_gisp_ver);
		}
		ec->struct_end();
		
		ec->put_octet ((ec->byteorder() == COMM::LittleEndian)
			? GISP_BYTEORDER_BIT : 0);
		
		ec->put_octet (mt);
		key = ec->buffer()->wpos();
		ec->put_ulong (0);
	}
	ec->struct_end();
	
	return key;
}

COMM::Boolean
SRBP::GISPCodec::put_close_msg (GISPOutContext &out) 
{
	COMM::DataEncoder *ec = out.ec();
	COMM::ULong key = put_header (out, GISP::CloseConnection);
	put_size (out, key);
	return TRUE;
}

COMM::Boolean
SRBP::GISPCodec::put_error_msg (GISPOutContext &out) 
{
	COMM::DataEncoder *ec = out.ec();
	COMM::ULong key = put_header (out, GISP::MessageError);
	put_size (out, key);
	return TRUE;
}

#define check(exp) if (!(exp)) return FALSE

COMM::Boolean
SRBP::GISPCodec::get_header (GISPInContext &in, GISP::MsgType &mt,COMM::ULong &sz, COMM::Octet &flags) //HEADER CHECK
{
	COMM::DataDecoder *dc = in.dc();
	
	//	COMM::Boolean b;
	COMM::Octet o;
	//	COMM::Char c;
	
	check (dc->struct_begin());
	{
		check (dc->arr_begin());
		{
			COMM::Char magic[5];
			magic[4] = 0;
			
			check (dc->get_chars_raw (magic, 4) &&(!strcmp ((char*)magic, "GISP")));// 1.Magic Number check
		}
		check (dc->arr_end());
		
		check (dc->struct_begin());
		{
			COMM::Octet min, maj;
			COMM::UShort ver;
			
			check (dc->get_octet (maj));//2. version Check
			check (dc->get_octet (min));
			
			ver = ((maj << 8) | min);
			
			if (ver < _gisp_ver && maj == 1) 
			{
				if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
				{
					SRBP::Logger::Stream (SRBP::Logger::GISP)
						<< "GISP: peer requests GISP version "
						<< (int) maj << "." << (int) min
						<< " instead of "
						<< (_gisp_ver>>8) << "." 
						<< (_gisp_ver&255)
						<< ", downgrading." << endl;
				}
				_gisp_ver = ver;
			} 
			else if (ver > _gisp_ver) 
			{
				if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
				{
					SRBP::Logger::Stream (SRBP::Logger::GISP)
						<< "GISP: peer sends message "
						<< "using unsupported GISP version "
						<< (int) maj << "." << (int) min
						<< endl;
				}
				return 0;
			}
		}
		check (dc->struct_end());
		
		check (dc->get_octet (flags)); //3.Endian Check
		dc->byteorder ((flags & GISP_BYTEORDER_BIT) ? COMM::LittleEndian: COMM::BigEndian);
		
		check (dc->get_octet (o)); // 4. MessageType
		mt = (GISP::MsgType)o;		
		check (dc->get_ulong (sz));//5. MessageSize 
	}
	check (dc->struct_end());
	return TRUE;
}

COMM::Boolean
SRBP::GISPCodec::check_header (GISPInContext &in, GISP::MsgType &mt,COMM::ULong &sz, COMM::Octet &flags) 
{
    COMM::ULong pos = in.dc()->buffer()->rpos ();
    COMM::Boolean ret = get_header (in, mt, sz, flags); // header Info Parsing 
    in.dc()->buffer()->rseek_beg (pos);
    return ret;
}

COMM::Boolean
SRBP::GISPCodec::get_invoke_request (GISPInContext &in,COMM::ULong &req_id,COMM::Octet &response_flags,COMM::SRBRequest * &req) 
{
	COMM::DataDecoder *dc = in.dc();
	
	COMM::String_var svcname;
	
	check (dc->struct_begin ());
	{
		
		check (dc->get_ulong (req_id));
		check (dc->get_octet (response_flags));
		response_flags = (response_flags & 0x02) ? 1 : 0;
		COMM::Octet o[3];
		check (dc->get_octets (o, 3));
		check (dc->get_string_raw (svcname.out()));
	}
	check (dc->struct_end ());
	
	req = new GISPRequest (svcname, in._retn(), this);
	
	return TRUE;
}

COMM::Boolean
SRBP::GISPCodec::get_invoke_reply1 (GISPInContext &in,COMM::ULong &req_id,GISP::ReplyStatusType &stat) 
{
	COMM::DataDecoder *dc = in.dc();
	
	COMM::ULong k;
	
	check (dc->struct_begin ());
	{
		check (dc->get_ulong (req_id));
		check (dc->enumeration (k));
		stat = (GISP::ReplyStatusType)k;
	}
	check (dc->struct_end ());
	
	switch (stat) {
	case GISP::NO_EXCEPTION:
	case GISP::USER_EXCEPTION:
	case GISP::SYSTEM_EXCEPTION:
		break;
	default:
		if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::GISP)
				<< "GISP: got illegal reply status" << endl;
		}
		return FALSE;
	}
	
	dc->buffer()->ralign (dc->max_alignment());
	
	return TRUE;
}

COMM::Boolean
SRBP::GISPCodec::get_invoke_reply2 (GISPInContext &in,COMM::ULong req_id,GISP::ReplyStatusType stat,
									COMM::SRBRequest *req) 
{
	COMM::DataDecoder *dc = in.dc();
	
	switch (stat) {
	case GISP::NO_EXCEPTION:
	case GISP::USER_EXCEPTION:
	case GISP::SYSTEM_EXCEPTION:
		if (req) {
			req->set_exception_type ((COMM::ExceptionType)stat);
			return req->demarshal (dc);
		}
		break;
		
	default: {
		return FALSE;
		break;
			 }
	}
	return TRUE;
}

COMM::Boolean
SRBP::GISPCodec::get_cancel_request (GISPInContext &in, COMM::ULong &req_id)
{
	COMM::DataDecoder *dc = in.dc();
	
	check (dc->struct_begin());
	{
		check (dc->get_ulong (req_id));
	}
	check (dc->struct_end());
	
	return TRUE;
}

void
SRBP::GISPCodec::put_size (GISPOutContext &out, COMM::ULong key) 
{
	COMM::DataEncoder *ec = out.ec();
	
	COMM::ULong end_pos = ec->buffer()->wpos();
	ec->buffer()->wseek_beg (key);
	ec->put_ulong (end_pos - ec->buffer()->rpos() - _headerlen);
	ec->buffer()->wseek_beg (end_pos);
}

COMM::Boolean
SRBP::GISPCodec::put_invoke_request (GISPOutContext &out,COMM::ULong req_id,COMM::Octet response_flags,
									 COMM::SRBRequest *req) 
{
	COMM::DataEncoder *ec = out.ec();
	
	COMM::ByteOrder bo = ec->byteorder();
	
	COMM::ULong key = put_header (out, GISP::Request);
	
	ec->struct_begin ();
	{
		ec->put_ulong (req_id);
		ec->put_octet ((response_flags & 0x1) ? 0x03 : 0);
		ec->put_octets ((COMM::Octet*)"\0\0\0", 3);
		ec->put_string_raw (req->svc_name());
	}
	ec->struct_end ();
	
	req->marshal (ec);
	
	put_size (out, key);
	
	ec->byteorder (bo);
	
	return TRUE;
}

COMM::Boolean
SRBP::GISPCodec::put_invoke_reply (GISPOutContext &out,COMM::ULong req_id,GISP::ReplyStatusType stat,
								   COMM::SRBRequest *req) 
{
	COMM::DataEncoder *ec = out.ec();
	
	COMM::ByteOrder bo = ec->byteorder();
	/*TODO
	if (!strcmp (req->type(), "gisp"))
	ec->byteorder (((GISPRequest *)req)->output_byteorder());
	*/
	
	COMM::ULong key = put_header (out, GISP::Reply);
	
	ec->struct_begin ();
	{
		ec->put_ulong (req_id);
		ec->enumeration ((COMM::ULong)stat);
	}
	ec->struct_end ();
	
	switch (stat) 
	{
	case GISP::NO_EXCEPTION:
	case GISP::USER_EXCEPTION:
	case GISP::SYSTEM_EXCEPTION:
		ec->buffer()->walign (ec->max_alignment());
		req->marshal (ec);
		break;
		
	default:
		assert (0);
		break;
	}
	
	put_size (out, key);
	
	ec->byteorder (bo);
	return TRUE;
}

COMM::Boolean
SRBP::GISPCodec::get_close_msg (GISPInContext &in) 
{
	return TRUE;
}

COMM::Boolean
SRBP::GISPCodec::get_error_msg (GISPInContext &in) 
{
	return TRUE;
}


/****************************** GISPRequest *****************************/


SRBP::GISPRequest::GISPRequest (const char *svc, COMM::DataDecoder *indata,GISPCodec *c) 
{
	_codec = GISPCodec::_duplicate (c);
	
	_svcname = svc;
	_idc = indata;
	_istart = _idc->buffer()->rpos();
	
	_oec = _idc->encoder (&_obuf, FALSE);
	_ostart = 0;
	_is_except = FALSE;
}

SRBP::GISPRequest::~GISPRequest () 
{
	COMM::release (_codec);
	delete _idc;
	delete _oec;
}

const COMM::Address *
SRBP::GISPRequest::addr () 
{
	assert (0);
	return 0;
}

const COMM::Address *
SRBP::GISPRequest::peer () 
{
	assert (0);
	return 0;
}

const char *
SRBP::GISPRequest::svc_name () 
{
	return _svcname.c_str();
}

COMM::DataEncoder *
SRBP::GISPRequest::get_encoder () 
{
	return _oec;
}

COMM::DataDecoder *
SRBP::GISPRequest::get_decoder () 
{
	return _idc;
}

void
SRBP::GISPRequest::set_marshaller (COMM::SRBMarshaller *ma,COMM::SRBDemarshaller *dma) 
{
	_ma = ma;
	_dma = dma;
}

void
SRBP::GISPRequest::marshal (COMM::DataEncoder *ec) 
{
	if (_is_except) 
	{
		ec->put_ulong (_ex_code);
		ec->put_string_raw (_ex_msg.c_str ());
	}
	else
		_ma->marshal (ec);
}

COMM::Boolean
SRBP::GISPRequest::demarshal (COMM::DataDecoder *dc) 
{
	return _dma->demarshal (dc);
}

void 
SRBP::GISPRequest::set_sys_exception (COMM::SysExceptionCode ex_code) 
{
	_is_except = TRUE;
	_ex_code = ex_code;
	_ex_msg = COMM::get_sys_exception_name (ex_code);
}

void 
SRBP::GISPRequest::set_sys_exception (COMM::SysExceptionCode ex_code, const char *ex_msg) 
{
	_is_except = TRUE;
	_ex_code = ex_code;
	_ex_msg = COMM::get_sys_exception_name (ex_code, ex_msg);
}

void 
SRBP::GISPRequest::set_exception_type (COMM::ExceptionType ex_type) 
{
	assert (0);
}

COMM::ExceptionType 
SRBP::GISPRequest::get_exception_type () 
{
	assert (0);
	return COMM::NO_EXCEPTION;
}


COMM::Boolean
SRBP::GISPRequest::copy_out_data (COMM::SRBRequest *req) 
{
	if (this == req)
		return TRUE;
	
	assert (_oec);
	
	_oec->buffer()->reset();
	
	_oec->buffer()->rseek_beg (_ostart);
	
	//TODO req->get_out_args (_oec, _is_except);
	
	return TRUE;
}

COMM::Boolean
SRBP::GISPRequest::copy_in_data (COMM::SRBRequest *req) 
{
	assert (0);
	return FALSE;
}


/******************************* GISPConn *******************************/


SRBP::GISPConn::GISPConn (COMM::SRB_ptr srb, COMM::Transport *transp,GISPConnCallback *cb,GISPCodec *codec,COMM::Long tmout, COMM::ULong max_size) 
						  : _inctx (codec, new COMM::Buffer()) 
{
	_srb = srb;
	_disp = srb->dispatcher ();
	_transp = transp;
	_cb = cb;
	_codec = codec;
	_max_message_size = max_size;
	
	_inbuf = new COMM::Buffer;
	_inlen = _codec->header_length ();
	_inbufs = 0;
	
	_refcnt = 0;
	_idle_tmout = tmout;
	_have_tmout = FALSE;
	_have_wselect = FALSE;
	
	_transp->block (SRBP::IISPProxy::isblocking ());
	_transp->rselect (_disp, this);		//dispatcherdp ���� 
}

SRBP::GISPConn::~GISPConn () 
{
	_disp->remove (this, COMM::Dispatcher::Timer);
	_transp->rselect (_disp, 0);
	_transp->wselect (_disp, 0);
	delete _transp;
	delete _inbuf;
}

COMM::StaticRequest *
SRBP::GISPConn::create_request (const char *svcname) 
{
	return new COMM::StaticRequest (_srb, 
		_codec->ec_proto (),
		_codec->dc_proto (), 
		svcname,
		_transp->addr (),
		_transp->peer ());
}

void
SRBP::GISPConn::ref () 
{
#ifdef _WINDOWS
	EnterCriticalSection(&gisp_conn_mut);
#else
	pthread_mutex_lock(&gisp_conn_mut);
#endif
	
	++_refcnt;
	check_busy ();
    
#ifdef _WINDOWS
	LeaveCriticalSection(&gisp_conn_mut);
#else
	pthread_mutex_unlock(&gisp_conn_mut);
#endif
}

COMM::Boolean
SRBP::GISPConn::deref (COMM::Boolean all) 
{
	COMM::Boolean ret;
    
#ifdef _WINDOWS
	EnterCriticalSection(&gisp_conn_mut);
#else
	pthread_mutex_lock(&gisp_conn_mut);
#endif
    
	if (all)
		_refcnt = 0;
	else
		--_refcnt;
	check_idle ();
    
	ret = (_refcnt <= 0);
    
#ifdef _WINDOWS
	LeaveCriticalSection(&gisp_conn_mut);
#else
	pthread_mutex_unlock(&gisp_conn_mut);
#endif
    
	return (ret);
}

void
SRBP::GISPConn::do_read () 
{
	COMM::Long r;
	
	while (42) 
	{
		assert (_inlen > 0);			//_inlen �ʱ⿡�� header size = 12 , header Parsing �� Message size�� �˰� �Ǹ� messagesize ��ŭ �Ͼ����δ�. 
		
		r = _transp->read (*_inbuf, _inlen);         //  header , Message �κ� ������ �о� ���δ�. 
//P : 
#if 1
		if (r < 0) 
		{
		/*
		* workaround for a problem on Linux and Win32
		* (and probably other OSes as well):
		*   if you send something on a TCP connection that has
		*   been closed by the peer you will get an error on
		*   the next read() instead of beeing able to read
		*   the remaining data until you reach EOF.
			*/
			r = _transp->read (*_inbuf, _inlen);
		}
#endif
		if (r < 0 || (r == 0 && _transp->eof())) 
		{
			// connection broken or EOF
			_transp->rselect (_disp, 0);
			_transp->wselect (_disp, 0);
			_cb->callback (this, GISPConnCallback::Closed);
			break;
		}
		else if (r > 0) 
		{
			_inlen -= r;
			if (_inbuf->length() == _codec->header_length())		//header �϶� 
			{
				// header completely received
				assert (_inlen == 0);
				GISP::MsgType mt;
				_inctx.buffer (_inbuf);
				if (!_codec->check_header (_inctx, mt, _inlen, _inflags))  //header Pasing & check 
				{
					// garbled message, send it to input handler ...
					assert (!_inbufs);
					_inbufs = _inbuf;
					_inbuf = new COMM::Buffer;
					_inlen = _codec->header_length ();
					_cb->callback (this, 
						GISPConnCallback::InputReady);
					break;
				}
				
				if (_max_message_size > 0 && _inlen > _max_message_size) 
				{
					if (SRBP::Logger::IsLogged (SRBP::Logger::Transport)) 
					{
						_inbuf->dump ("In Data", SRBP::Logger::Stream 
							(SRBP::Logger::Transport));
					}
					if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
					{
						SRBP::Logger::Stream (SRBP::Logger::GISP)
							<< "GISP: message size (" 
							<< _inlen << " bytes) "
							<< "larger than threshold (" 
							<< _max_message_size
							<< ")" << endl;
					}
					_transp->rselect (_disp, 0);
					_transp->wselect (_disp, 0);
					_cb->callback (this, 
						GISPConnCallback::Closed);
					break;
				}
			}
			
			if (_inlen == 0)		//data �� �� 
			{
				// message completely received
				COMM::Octet mt;
				_inbuf->rseek_beg (7);
				_inbuf->get1 (&mt);
				_inbuf->rseek_beg (0);
				
				assert (!_inbufs);
				_inbufs = _inbuf;
				_inbuf = new COMM::Buffer;
				_inlen = _codec->header_length ();
				_cb->callback (this, GISPConnCallback::InputReady); // SRBCallback _cb
				break;
			}
		}
		else if (r == 0) 
		{
			break;//continue;
		}
		else 
		{
			assert (0);
		}
	}
}

void
SRBP::GISPConn::do_write () 
{
	while (42) 
	{
		assert (_outbufs.size() > 0);
		COMM::Buffer *b = _outbufs.front();
		COMM::Long r = _transp->write (*b, b->length());
		if (r > 0) 
		{
			if (b->length() == 0) 
			{
				// message completely sent
				delete b;
				_outbufs.pop_front();
				if (_outbufs.size() == 0) 
				{
					check_idle ();
					break;
				}
			}
		} 
		else if (r < 0) 
		{
			// connection broken
			_transp->rselect (_disp, 0);
			_transp->wselect (_disp, 0);
			_cb->callback (this, GISPConnCallback::Closed);
			break;
		}
		else if (r == 0) 
		{
			break;
		}
		else 
		{
			assert (0);
		}
	}
}

void
SRBP::GISPConn::callback (COMM::Transport *, COMM::TransportCallback::Event ev) 
{
	switch (ev) 
	{
	case Read:
		do_read ();
		break;
	case Write:
		do_write ();
		break;
	default:
		assert (0);
	}
}

void
SRBP::GISPConn::callback (COMM::Dispatcher *d, COMM::DispatcherCallback::Event ev) 
{
	switch (ev) 
	{
	case COMM::Dispatcher::Timer:
		_cb->callback (this, GISPConnCallback::Idle);
		break;
	case COMM::Dispatcher::Moved:
		_disp = d;
		break;
	default:
		assert (0);
	}
}

void
SRBP::GISPConn::check_idle () 
{
	if (_idle_tmout > 0 && _refcnt == 0 && _outbufs.size() == 0) 
	{
		if (_have_tmout)
			_disp->remove (this, COMM::Dispatcher::Timer);
		_disp->tm_event (this, _idle_tmout);
		_have_tmout = TRUE;
	}
	if (_have_wselect && _outbufs.size() == 0) {
		_transp->wselect (_disp, 0);
		_have_wselect = FALSE;
	}
}

void
SRBP::GISPConn::check_busy () 
{
	if (_have_tmout && (_refcnt > 0 || _outbufs.size() > 0)) 
	{
		_disp->remove (this, COMM::Dispatcher::Timer);
		_have_tmout = FALSE;
	}
	if (!_have_wselect && _outbufs.size() > 0) {
		_transp->wselect (_disp, this);
		_have_wselect = TRUE;
	}
}

void
SRBP::GISPConn::output (COMM::Buffer *b) 
{
	if (SRBP::Logger::IsLogged (SRBP::Logger::Transport)) 
	{
		b->dump ("Out Data", 
			SRBP::Logger::Stream (SRBP::Logger::Transport));
	}
	
	// try to write as much as possible immediatly
	if (_outbufs.size() == 0) 
	{
		int n =  _transp->write (*b, b->length());
		if (b->length() == 0) 
		{
			delete b;
			return;
		}
		_outbufs.push_back (b);
	} 
	else 
	{
		_outbufs.push_back (b);
		do_write (); 
	}
	
	check_busy ();
}

COMM::Buffer *
SRBP::GISPConn::input () 
{
	COMM::Buffer *b = _inbufs;
	_inbufs = 0;
	
	if (SRBP::Logger::IsLogged (SRBP::Logger::Transport)) 
	{
		b->dump ("In Data", SRBP::Logger::Stream (SRBP::Logger::Transport));
	}
	
	return b;
}

void
SRBP::GISPConn::flush () 
{
	COMM::Boolean isblock = _transp->isblocking();
	_transp->block (TRUE);
	while (_outbufs.size() > 0) 
	{
		COMM::Buffer *b = _outbufs.front();
		_outbufs.pop_front();
		int i = _transp->write (*b, b->length());
		delete b;
	}
	_transp->block (isblock);
}

void
SRBP::GISPConn::buffering (COMM::Boolean dobuffering)
{
    _transp->buffering (dobuffering);
}


/******************************* IISPProxy ******************************/


COMM::Boolean SRBP::IISPProxy::_isblocking = FALSE;

SRBP::IISPProxy::IISPProxy (COMM::SRB_ptr srb,COMM::UShort gisp_ver,COMM::ULong max_size) 
{
	_srb = srb;
	
	_cache_used = FALSE;
	_cache_rec = new IISPProxyInvokeRec;
	
	_max_message_size = max_size;
	_gisp_ver = gisp_ver;
	_srb->register_sa (this);	//srb�� Client ����.	server,proxy,psa Interface ServiceAdapter * type���� ���� 
}

SRBP::IISPProxy::~IISPProxy () 
{
	_srb->unregister_sa (this);
	
	for (MapAddrConn::iterator i0 = _conns.begin(); i0 != _conns.end(); ++i0)
		delete (*i0).second;
	
	for (MapIdConn::iterator i1 = _ids.begin(); i1 != _ids.end(); ++i1)
		delete (*i1).second;
	
	delete _cache_rec;
}

SRBP::IISPProxyInvokeRec *
SRBP::IISPProxy::create_invoke () 
{
#ifdef _WINDOWS
	EnterCriticalSection(&iisp_p_ir_mut);
#else
	pthread_mutex_lock(&iisp_p_ir_mut);
#endif
	
	if (!_cache_used) 
	{
		_cache_used = TRUE;
#ifdef _WINDOWS
        LeaveCriticalSection(&iisp_p_ir_mut);
#else
        pthread_mutex_unlock(&iisp_p_ir_mut);
#endif
		return _cache_rec;
	}
	
	SRBP::IISPProxyInvokeRec *ret = new IISPProxyInvokeRec;
    
#ifdef _WINDOWS
	LeaveCriticalSection(&iisp_p_ir_mut);
#else
	pthread_mutex_unlock(&iisp_p_ir_mut);
#endif
	
	return ret;
}

SRBP::IISPProxyInvokeRec *
SRBP::IISPProxy::get_invoke (MsgId id) 
{
#ifdef _WINDOWS
	EnterCriticalSection(&iisp_p_ir_mut);
#else
	pthread_mutex_lock(&iisp_p_ir_mut);
#endif
	
	if (_cache_used && _cache_rec->id() == id) 
	{
#ifdef _WINDOWS
        LeaveCriticalSection(&iisp_p_ir_mut);
#else
        pthread_mutex_unlock(&iisp_p_ir_mut);
#endif
		return _cache_rec;
	}
	MapIdConn::iterator i = _ids.find (id);
    
	SRBP::IISPProxyInvokeRec *ret = (i == _ids.end()) ? 0 : (*i).second;
    
#ifdef _WINDOWS
	LeaveCriticalSection(&iisp_p_ir_mut);
#else
	pthread_mutex_unlock(&iisp_p_ir_mut);
#endif
	
	return ret;
}

void
SRBP::IISPProxy::add_invoke (IISPProxyInvokeRec *rec) 
{
#ifdef _WINDOWS
	EnterCriticalSection(&iisp_p_ir_mut);
#else
	pthread_mutex_lock(&iisp_p_ir_mut);
#endif
	if (_cache_rec == rec) 
	{
#ifdef _WINDOWS
        LeaveCriticalSection(&iisp_p_ir_mut);
#else
        pthread_mutex_unlock(&iisp_p_ir_mut);
#endif
		return;
	}
	_ids[rec->id()] = rec;
	
#ifdef _WINDOWS
	LeaveCriticalSection(&iisp_p_ir_mut);
#else
	pthread_mutex_unlock(&iisp_p_ir_mut);
#endif
}

void
SRBP::IISPProxy::del_invoke (MsgId id) 
{
#ifdef _WINDOWS
	EnterCriticalSection(&iisp_p_ir_mut);
#else
	pthread_mutex_lock(&iisp_p_ir_mut);
#endif
	if (_cache_used && _cache_rec->id() == id) 
	{
		_cache_rec->free();
		_cache_used = FALSE;
#ifdef _WINDOWS
        LeaveCriticalSection(&iisp_p_ir_mut);
#else
        pthread_mutex_unlock(&iisp_p_ir_mut);
#endif
		return;
	}
	MapIdConn::iterator i = _ids.find (id);
	if (i != _ids.end()) 
	{
		delete (*i).second;
		_ids.erase (i);
	}
#ifdef _WINDOWS
	LeaveCriticalSection(&iisp_p_ir_mut);
#else
	pthread_mutex_unlock(&iisp_p_ir_mut);
#endif
}

void
SRBP::IISPProxy::abort_invoke (MsgId id) 
{
	// make invocation fail; notify srb ...
	if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
	{
		SRBP::Logger::Stream (SRBP::Logger::IISP)
			<< "GISP: invocation(" << id << ") aborted" << endl;
	}
	
	del_invoke (id);
	
	/*TODO
	switch (_srb->request_type (id)) {
	case COMM::RequestInvoke: {
	COMM::Object_var obj = new COMM::Object (new COMM::IOR);
	COMM::Request_var req = new COMM::Request (obj, "someop");
	LocalRequest srbreq (req);
	srbreq.set_out_args (
	new COMM::TRANSIENT (0, COMM::COMPLETED_MAYBE));
	_srb->answer_invoke (id, COMM::InvokeSysEx,
	COMM::Object::_nil(), &srbreq, 0);
	break;
	}
	case COMM::RequestLocate:
	_srb->answer_locate (id, COMM::LocateUnknown,
	COMM::Object::_nil(), 0);
	break;
	
	 case COMM::RequestBind:
	 _srb->answer_bind (id, COMM::LocateUnknown,
	 COMM::Object::_nil());
	 break;
	 
	  default:
	  assert (0);
	  }
	*/
}

void
SRBP::IISPProxy::redo_invoke (MsgId id) 
{
	// redo invocation ...
	if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
	{
		SRBP::Logger::Stream (SRBP::Logger::IISP)
			<< "GISP: invocation(" << id << ") redone" << endl;
	}
	
	/*TODO
	del_invoke (id);
	_srb->redo_request (id);
	*/
}

void
SRBP::IISPProxy::conn_error (GISPConn *conn, COMM::Boolean send_error) 
{
	if (!send_error) 
	{
		kill_conn (conn);
		return;
	}
	
	GISPOutContext out (conn->codec());
	conn->codec()->put_error_msg (out);
	conn->output (out._retn());
	// prepare shutdown (i.e. wait until MessageError has been sent)
	conn->deref (TRUE);
}

COMM::Boolean
SRBP::IISPProxy::handle_input (GISPConn *conn) 
{
	if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
	{
		SRBP::Logger::Stream (SRBP::Logger::IISP)
			<< "IISP: incoming data from "
			<< conn->transport()->peer()->stringify() << endl;
	}
	
	GISPInContext in (conn->codec(), conn->input());
	
	GISP::MsgType mt;
	COMM::ULong size;
	COMM::Octet flags;
	
	if (!conn->codec()->get_header (in, mt, size, flags)) 
	{
		if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::GISP)
				<< "GISP: cannot decode incoming header from "
				<< conn->transport()->peer()->stringify() << endl;
		}
		conn_error (conn, 0);
		return FALSE;
	}
	
	switch (mt) {
	case GISP::Reply:
		return handle_invoke_reply (conn, in);
		
	case GISP::CloseConnection:
		if (!conn->codec()->get_close_msg (in)) 
		{
			if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
			{
				SRBP::Logger::Stream (SRBP::Logger::GISP)
					<< "GISP: cannot decode CloseConnection from "
					<< conn->transport()->peer()->stringify() << endl;
			}
			conn_error (conn, 0);
		} 
		else 
		{
			if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
			{
				SRBP::Logger::Stream (SRBP::Logger::GISP)
					<< "GISP: incoming CloseConnection from "
					<< conn->transport()->peer()->stringify() << endl;
			}
			kill_conn (conn, TRUE);
		}
		return FALSE;
		
	case GISP::MessageError:
		if (!conn->codec()->get_error_msg (in)) 
		{
			if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
			{
				SRBP::Logger::Stream (SRBP::Logger::GISP)
					<< "GISP: cannot decode MessageError from "
					<< conn->transport()->peer()->stringify() << endl;
			}
			conn_error (conn, FALSE);
			break;
		}
		if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::GISP)
				<< "GISP: incoming MessageError from "
				<< conn->transport()->peer()->stringify() << endl;
		}
		kill_conn (conn);
		return FALSE;
		
	default:
		if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::GISP)
				<< "GISP: bad incoming message type (" << mt << ") from "
				<< conn->transport()->peer()->stringify() << endl;
		}
		conn_error (conn, 0);
		return FALSE;
	}
	
	return TRUE;
}

void
SRBP::IISPProxy::exec_invoke_reply (GISPInContext &in, COMM::ULong req_id,
									GISP::ReplyStatusType stat,
									COMM::SRBRequest *req,
									GISPConn *conn) 
{
	COMM::InvokeStatus srb_stat = COMM::InvokeOk;
	
	switch (stat) 
	{
	case GISP::NO_EXCEPTION:
		srb_stat = COMM::InvokeOk;
		break;
		
	case GISP::USER_EXCEPTION:
		srb_stat = COMM::InvokeUsrEx;
		break;
		
	case GISP::SYSTEM_EXCEPTION:
		srb_stat = COMM::InvokeSysEx;
		break;
		
	default:
		assert (0);
		break;
	}
	_srb->answer_invoke (req_id, srb_stat, req);
}

COMM::Boolean
SRBP::IISPProxy::handle_invoke_reply (GISPConn *conn, GISPInContext &in) 
{
	COMM::ULong req_id;
	GISP::ReplyStatusType stat;
	
	if (!conn->codec()->get_invoke_reply1 (in, req_id, stat)) 
	{
		if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::IISP)
				<< "GISP: cannot decode incoming Reply from "
				<< conn->transport()->peer()->stringify() << endl;
		}
		conn_error (conn);
		return FALSE;
	}
	
	if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
	{
		SRBP::Logger::Stream (SRBP::Logger::GISP)
			<< "GISP: incoming Reply from "
			<< conn->transport()->peer()->stringify()
			<< " for msgid " << req_id
			<< " status is " << (COMM::ULong) stat
			<< endl;
	}
	
	IISPProxyInvokeRec *rec = get_invoke (req_id);
	if (!rec) 
	{
		// has been canceled; cancel() did the deref() already
		return TRUE;
	}
	
	if (!conn->codec()->get_invoke_reply2 (in, req_id, stat, rec->request())) 
	{
		if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::IISP)
				<< "GISP: reply marshalling error for msgid " << req_id << endl;
		}
		if (rec->request()) 
		{
		/*TODO
		COMM::MARSHAL ex (0, COMM::COMPLETED_MAYBE);
		rec->request()->set_out_args (&ex);
		stat = GISP::SYSTEM_EXCEPTION;
			*/
		}
	}
	
	COMM::SRBRequest * srbreq = rec->request ();
	del_invoke (req_id);
	
	exec_invoke_reply (in, req_id, stat, srbreq, conn);
	
	conn->deref();
	
	return TRUE;
}

SRBP::GISPConn *
SRBP::IISPProxy::make_conn (const COMM::Address *addr,COMM::Boolean docreate,COMM::UShort version) 
{
	MapAddrConn::iterator i = _conns.find (addr);
	if (i != _conns.end()) 
	{
#if 0
		if ((*i).second->check_events()) 
		{ 
			while ((i = _conns.find (addr)) != _conns.end()) 
			{
				if (!(*i).second->check_events())
					return (*i).second;
			}
		} 
		else
#endif
			return (*i).second;
	}
	
	if (!docreate)
		return 0;
	
	if (version == 0 || version > _gisp_ver) 
	{
		version = _gisp_ver;
	}
	
	if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
	{
		SRBP::Logger::Stream (SRBP::Logger::GISP)
			<< "IISP: making new GISP "
			<< (version>>8) << "." << (version&255)
			<< " connection to " << addr->stringify()
			<< endl;
	}
	
	COMM::Transport *t = addr->make_transport();
	if (!t->connect (addr)) 
	{
		if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::GISP)
				<< "IISP: connect to " << addr->stringify()
				<< " failed: " << t->errormsg() << endl;
		}
		delete t;
		return 0;
	}
	
	GISPConn *conn = new GISPConn (_srb, t, this,new GISPCodec (new CDRDecoder, new CDREncoder, version),
			  0L /* no tmout */, _max_message_size);
	_conns[t->peer()] = conn;
	
#ifndef _WINDOWS
	if (select_running) kill(select_running, SIGUSR1);
#else
#endif
	
	return conn;
}

COMM::Boolean
SRBP::IISPProxy::invoke (MsgId msgid, COMM::SRBRequest *req, COMM::Boolean response_exp) 
{
	GISPConn *conn = make_conn (req->peer ());
	if (!conn) {
	/*TODO
	COMM::COMM_FAILURE ex;
	req->set_out_args (&ex);
		*/
		_srb->answer_invoke (msgid, COMM::InvokeSysEx, req);
		return FALSE;
	}
	
	if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
	{
		SRBP::Logger::Stream (SRBP::Logger::GISP)
			<< "GISP: sending Request to "
			<< conn->transport()->peer()->stringify() 
			<< " msgid is " << msgid << endl;
	}
	
	GISPOutContext out (conn->codec());
	
	if (!conn->codec()->put_invoke_request (out, msgid, response_exp, req)) 
	{
		/*TODO
		COMM::MARSHAL ex;
		req->set_out_args (&ex);
		*/
		_srb->answer_invoke (msgid, COMM::InvokeSysEx, req);
		return FALSE;
	}
	
	if (response_exp) 
	{
		conn->ref();
		IISPProxyInvokeRec *rec = create_invoke();
		rec->init (msgid, conn, req);
		add_invoke (rec);
	}
	conn->buffering (!response_exp);
	conn->output (out._retn());
	
	if (response_exp && _isblocking) 
	{
		conn->do_read ();
	}
	
	return TRUE;
}

int
SRBP::IISPProxy::answer_invoke (COMM::ULong, COMM::SRBRequest *, COMM::InvokeStatus) 
{
	assert (0);
	return 0;
}

void
SRBP::IISPProxy::shutdown (COMM::Boolean wait_for_completion) 
{
	_srb->answer_shutdown (this);
}

void
SRBP::IISPProxy::kill_conn (GISPConn *conn, COMM::Boolean redo) 
{
	COMM::Boolean again;
	
	do {
		again = FALSE;
		for (MapAddrConn::iterator i = _conns.begin(); i != _conns.end(); ++i) 
		{
			if ((*i).second == conn) 
			{
				_conns.erase (i);
				again = TRUE;
				break;
			}
		}
	} while (again);
	
	if (_cache_used && _cache_rec->conn() == conn) {
		if (redo) 
		{
			redo_invoke (_cache_rec->id());
		}
		else
		{
			abort_invoke (_cache_rec->id());
		}
	}
	
	do 
	{
		again = FALSE;
		IISPProxyInvokeRec *rec;
		for (MapIdConn::iterator i = _ids.begin(); i != _ids.end(); ++i) 
		{
			rec = (*i).second;
			if (rec->conn() != conn)
				continue;
			
			if (redo) {
				redo_invoke (rec->id());
			} else {
				abort_invoke (rec->id());
			}
			again = TRUE;
			break;
		}
	} while (again);
	
	delete conn;
}

COMM::Boolean
SRBP::IISPProxy::callback (GISPConn *conn, GISPConnCallback::Event ev) 
{
	COMM::Boolean ret = FALSE;
	
	switch (ev) 
	{
	case GISPConnCallback::InputReady:
		ret = handle_input (conn);
		return ret; 
		
	case GISPConnCallback::Idle:
		if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::IISP)
				<< "IISP: shutting down idle conn to "
				<< conn->transport()->peer()->stringify() << endl;
		}
		kill_conn (conn);
		return FALSE;
		
	case GISPConnCallback::Closed:
		if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::IISP)
				<< "IISP: connection to "
				<< conn->transport()->peer()->stringify() 
				<< " closed or broken" << endl;
		}
		kill_conn (conn);
		return FALSE;
		
	default:
		assert (0);
	}
	
	return TRUE;
}

COMM::Boolean
SRBP::IISPProxy::has_service (const char *svcname) 
{
	if (_conns.size () > 0)
		return TRUE;
	else
		return FALSE;
}

COMM::Boolean
SRBP::IISPProxy::is_local () const 
{
	return FALSE;
}


/************************* IISPServerInvokeRec ************************/


SRBP::IISPServerInvokeRec::IISPServerInvokeRec () 
{
	_conn = 0;
	_req = 0;
}


SRBP::IISPServerInvokeRec::~IISPServerInvokeRec () 
{
	COMM::release (_req);
	
	// XXX do not free connection
}

void
SRBP::IISPServerInvokeRec::free () 
{
	COMM::release (_req);
	
	_conn = 0;
	_req = 0;
}

void
SRBP::IISPServerInvokeRec::init_invoke ( GISPConn *conn, MsgId reqid,MsgId srbid, COMM::SRBRequest *req) 
{
	_conn = conn;
	_srbid = srbid;
	_reqid = reqid;
	_req = req;
}


/******************************* IISPServer *****************************/


SRBP::IISPServer::IISPServer (COMM::SRB_ptr srb,COMM::UShort iisp_ver,COMM::ULong max_size) 
{
	_srb = srb;
	
	_cache_used = FALSE;
	_cache_rec = new IISPServerInvokeRec;
	
	_iisp_ver = iisp_ver;
	_max_message_size = max_size;
	
	_srb->register_sa (this);  //SRB�� ���� Interface ServiceAdapter
}

SRBP::IISPServer::~IISPServer () 
{
	_srb->unregister_sa (this);
}

COMM::Boolean
SRBP::IISPServer::listen (COMM::Address *addr) 
{
	COMM::TransportServer *tserv = addr->make_transport_server ();
	if (!tserv->bind (addr)) 
	{
		if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::IISP)
				<< "IISP: cannot bind to " << addr->stringify() << ": "
				<< tserv->errormsg() << endl;
		}
		return FALSE;
	}
	tserv->block (SRBP::IISPProxy::isblocking());
	tserv->aselect (_srb->dispatcher(), this);		//server socket listen() �� dispatcher�� socket ���� 
	
	if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
	{
		SRBP::Logger::Stream (SRBP::Logger::IISP)
			<< "IISP: server listening on "
			<< tserv->addr()->stringify()
			<< " IISP version "
			<< (_iisp_ver >> 8) << "." << (_iisp_ver & 255)
			<< endl;
	}
	
	_tservers.push_back (tserv);		//IISPServer�� TransportServer ���� 
	
	return TRUE;
}

COMM::Boolean
SRBP::IISPServer::listen () 
{
	InetAddress addr ("0.0.0.0", 0);
	return listen (&addr);
}

void
SRBP::IISPServer::callback (COMM::SRB_ptr, MsgId msgid,COMM::SRBCallback::Event ev) 
{
	switch (ev) 
	{
	case COMM::SRBCallback::Invoke:
		handle_invoke_reply (msgid);
		break;
		
	default:
		assert (0);
	}
}


COMM::Boolean
SRBP::IISPServer::callback (GISPConn *conn, GISPConnCallback::Event ev) 
{
	COMM::Boolean r = FALSE;
	
	switch (ev) 
	{
	case GISPConnCallback::InputReady:
		r = handle_input (conn);
		return r;
		
	case GISPConnCallback::Idle:
		if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::IISP)
				<< "IISP: shutting down idle conn to "
				<< conn->transport()->peer()->stringify() << endl;
		}
		conn_closed (conn);
		kill_conn (conn);
		return FALSE;
		
	case GISPConnCallback::Closed: 
		{
			if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
			{
				SRBP::Logger::Stream (SRBP::Logger::IISP)
					<< "IISP: connection to "
					<< conn->transport()->peer()->stringify() 
					<< " closed or broken" << endl;
			}
			kill_conn (conn);
			return FALSE;
		}
	default:
		assert (0);
	}
	
	return TRUE;
}

void
SRBP::IISPServer::callback (COMM::TransportServer *tserv,COMM::TransportServerCallback::Event ev) 
{
	switch (ev) 
	{
	case COMM::TransportServerCallback::Accept: 
		{
		COMM::Transport *t = tserv->accept();	//������ Client�� ���� TCPTransport ��ü ���� �� Return 
		if (t) 
		{
			if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
			{
				SRBP::Logger::Stream (SRBP::Logger::IISP)
					<< "IISP: new connection opened from "
					<< t->peer()->stringify() << endl;
			}
			if (t->bad()) 
			{
				if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
				{
					SRBP::Logger::Stream (SRBP::Logger::IISP)
						<< "IISP: connection from "
						<< t->peer()->stringify() 
						<< " is bad: " << t->errormsg() << endl;
				}
				delete t;
				break;
			}
			
			GISPConn *conn = new GISPConn (_srb, t, this,new GISPCodec (new CDRDecoder, new CDREncoder, _iisp_ver),
				0L /* no tmout */, _max_message_size);
			_conns.push_back (conn);
		}
		break;
												}
	default:
		assert (0);
	}
}

SRBP::IISPServerInvokeRec *
SRBP::IISPServer::create_invoke () 
{
#ifdef _WINDOWS
	EnterCriticalSection(&iisp_s_ir_mut);
#else
	pthread_mutex_lock(&iisp_s_ir_mut);
#endif
	
	if (!_cache_used) {                         //if nobody uses _cache_rec, then just returns it. 
		_cache_used = TRUE;
#ifdef _WINDOWS
		LeaveCriticalSection(&iisp_s_ir_mut);
#else
		pthread_mutex_unlock(&iisp_s_ir_mut);
#endif
		return _cache_rec;
	}
	
	SRBP::IISPServerInvokeRec *ret = new IISPServerInvokeRec;  //if used, then allocate IISPServerInvokeRec and returns it
	
#ifdef _WINDOWS
	LeaveCriticalSection(&iisp_s_ir_mut);
#else
	pthread_mutex_unlock(&iisp_s_ir_mut);
#endif
	
	return ret;
}

SRBP::IISPServerInvokeRec *
SRBP::IISPServer::get_invoke_reqid (MsgId msgid, GISPConn *conn) 
{
#ifdef _WINDOWS
	EnterCriticalSection(&iisp_s_ir_mut);
#else
	pthread_mutex_lock(&iisp_s_ir_mut);
#endif
	
	if (_cache_used && _cache_rec->reqid() == msgid &&
		_cache_rec->conn() == conn) {
#ifdef _WINDOWS
		LeaveCriticalSection(&iisp_s_ir_mut);
#else
		pthread_mutex_unlock(&iisp_s_ir_mut);
#endif
		return _cache_rec;
	}
	
	// XXX slow, but only needed for cancel
	
	IISPServerInvokeRec *rec;
	for (MapIdConn::iterator i = _srbids.begin(); i != _srbids.end(); ++i) {
		rec = (*i).second;
		if (rec->reqid() == msgid && rec->conn() == conn) 
		{
#ifdef _WINDOWS
			LeaveCriticalSection(&iisp_s_ir_mut);
#else
			pthread_mutex_unlock(&iisp_s_ir_mut);
#endif
			return rec;
		}
	}
#ifdef _WINDOWS
	LeaveCriticalSection(&iisp_s_ir_mut);
#else
	pthread_mutex_unlock(&iisp_s_ir_mut);
#endif
	return 0;
}

SRBP::IISPServerInvokeRec *
SRBP::IISPServer::get_invoke_srbid (MsgId msgid) 
{
#ifdef _WINDOWS
	EnterCriticalSection(&iisp_s_ir_mut);
#else
	pthread_mutex_lock(&iisp_s_ir_mut);
#endif
	if (_cache_used && _cache_rec->srbid() == msgid) 
	{
#ifdef _WINDOWS
		LeaveCriticalSection(&iisp_s_ir_mut);
#else
		pthread_mutex_unlock(&iisp_s_ir_mut);
#endif
		return _cache_rec;
	}
	
	MapIdConn::iterator i = _srbids.find (msgid);
    
	SRBP::IISPServerInvokeRec *ret = ( i != _srbids.end()) ? (*i).second : 0;
	
#ifdef _WINDOWS
	LeaveCriticalSection(&iisp_s_ir_mut);
#else
	pthread_mutex_unlock(&iisp_s_ir_mut);
#endif
	return (ret);
}

void
SRBP::IISPServer::add_invoke (IISPServerInvokeRec *rec) 
{
#ifdef _WINDOWS
	EnterCriticalSection(&iisp_s_ir_mut);
#else
	pthread_mutex_lock(&iisp_s_ir_mut);
#endif
	if (_cache_rec == rec) {
#ifdef _WINDOWS
		LeaveCriticalSection(&iisp_s_ir_mut);
#else
		pthread_mutex_unlock(&iisp_s_ir_mut);
#endif
		return;
	}
	
	//assert (_srbids.count (rec->srbid()) == 0);
	_srbids[rec->srbid()] = rec;
	
#ifdef _WINDOWS
	LeaveCriticalSection(&iisp_s_ir_mut);
#else
	pthread_mutex_unlock(&iisp_s_ir_mut);
#endif
}

void
SRBP::IISPServer::del_invoke_srbid (MsgId msgid)
 {	
#ifdef _WINDOWS
	EnterCriticalSection(&iisp_s_ir_mut);
#else
	pthread_mutex_lock(&iisp_s_ir_mut);
#endif
	if (_cache_used && _cache_rec->srbid() == msgid) 
	{
		_cache_rec->free();
		_cache_used = FALSE;
#ifdef _WINDOWS
		LeaveCriticalSection(&iisp_s_ir_mut);
#else
		pthread_mutex_unlock(&iisp_s_ir_mut);
#endif
		return;
	}
	
	MapIdConn::iterator i = _srbids.find (msgid);
	if (i != _srbids.end()) 
	{
		delete (*i).second;
		_srbids.erase (i);
	}
#ifdef _WINDOWS
	LeaveCriticalSection(&iisp_s_ir_mut);
#else
	pthread_mutex_unlock(&iisp_s_ir_mut);
#endif
}

void
SRBP::IISPServer::del_invoke_reqid (MsgId msgid, GISPConn *conn) 
{
#ifdef _WINDOWS
	EnterCriticalSection(&iisp_s_ir_mut);
#else
	pthread_mutex_lock(&iisp_s_ir_mut);
#endif
	if (_cache_used && _cache_rec->reqid() == msgid &&
		_cache_rec->conn() == conn) {
		_cache_rec->free();
		_cache_used = FALSE;
#ifdef _WINDOWS
		LeaveCriticalSection(&iisp_s_ir_mut);
#else
		pthread_mutex_unlock(&iisp_s_ir_mut);
#endif
		return;
	}
	
	// XXX slow, but rarely used
	IISPServerInvokeRec *rec;
	for (MapIdConn::iterator i = _srbids.begin(); i != _srbids.end(); ++i) 
	{
		rec = (*i).second;
		if (rec->reqid() == msgid && rec->conn() == conn) 
		{
			delete rec;
			_srbids.erase (i);
			break;
		}
	}
#ifdef _WINDOWS
	LeaveCriticalSection(&iisp_s_ir_mut);
#else
	pthread_mutex_unlock(&iisp_s_ir_mut);
#endif
}

void
SRBP::IISPServer::abort_invoke_srbid (MsgId msgid) 
{
	//TODO _srb->cancel (msgid);
	del_invoke_srbid (msgid);
}

COMM::Boolean
SRBP::IISPServer::handle_input (GISPConn *conn) 
{
	if (SRBP::Logger::IsLogged (SRBP::Logger::IISP)) 
	{
		SRBP::Logger::Stream (SRBP::Logger::IISP)
			<< "IISP: incoming data from "
			<< conn->transport()->peer()->stringify() << endl;
	}
	
	GISPInContext in (conn->codec(), conn->input());  //IN_DATA ���� 
	
	GISP::MsgType mt;
	COMM::ULong size;
	COMM::Octet flags;
	
	if (!conn->codec()->get_header (in, mt, size, flags)) 
	{
		if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::GISP)
				<< "GISP: cannot decode incoming header from "
				<< conn->transport()->peer()->stringify() << endl;
		}
		conn_error (conn, 0);
		return FALSE;
	}
	
	switch (mt)				//Message type 
	{
	case GISP::Request:
		return handle_invoke_request (conn, in);			//Response�� �ʿ��� Message�� ���� 
		
	case GISP::MessageError:
		if (!conn->codec()->get_error_msg (in)) 
		{
			if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
			{
				SRBP::Logger::Stream (SRBP::Logger::GISP)
					<< "GISP: cannot decode MessageError from "
					<< conn->transport()->peer()->stringify() << endl;
			}
			conn_error (conn, FALSE);
		}
		else 
		{
			if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
			{
				SRBP::Logger::Stream (SRBP::Logger::GISP)
					<< "GISP: incoming MessageError from "
					<< conn->transport()->peer()->stringify() << endl;
			}
			kill_conn (conn);
		}
		return FALSE;
		
	case GISP::CancelRequest:
		return handle_cancel_request (conn, in);
		
	case GISP::CloseConnection:
		if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::GISP)
				<< "GISP: incoming CloseConnection from "
				<< conn->transport()->peer()->stringify() << endl;
		}
		kill_conn (conn);
		break;
		
	default:
		if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::GISP)
				<< "GISP: bad incoming message type (" << mt 
				<< ") from "
				<< conn->transport()->peer()->stringify() << endl;
		}
		break;
	}
	
	return TRUE;
}

SRBP::IISPServer::MsgId
SRBP::IISPServer::exec_invoke_request (GISPInContext &in, COMM::SRBRequest *req,
									   COMM::Boolean resp_exp, GISPConn *conn,
									   MsgId srbid) 
{
	SRBP::IISPServer::MsgId ret;
	
	ret = _srb->invoke_async (req, resp_exp, this, srbid);
	
	return ret; 
}

COMM::Boolean
SRBP::IISPServer::handle_invoke_request (GISPConn *conn, GISPInContext &in) 
{
	COMM::ULong req_id;
	COMM::Boolean resp;
	COMM::SRBRequest *req;
	
	if (!conn->codec()->get_invoke_request (in, req_id, resp, req))
	{
		if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
		{
			SRBP::Logger::Stream (SRBP::Logger::GISP)
				<< "GISP: cannot decode Request from "
				<< conn->transport()->peer()->stringify() << endl;
		}
		
		conn_error (conn, 0);
		return FALSE;
	}
	
	if (SRBP::Logger::IsLogged (SRBP::Logger::GISP))
	{
		SRBP::Logger::Stream (SRBP::Logger::GISP)
			<< "GISP: incoming Request from "
			<< conn->transport()->peer()->stringify()
			<< " with msgid " << req_id << endl;
	}
	
	MsgId srbid = _srb->new_msgid();
	if (resp)
	{
		conn->ref ();
		IISPServerInvokeRec *rec = create_invoke();
		rec->init_invoke (conn, req_id, srbid, req);
		add_invoke (rec);
	}
	MsgId srbid2 = exec_invoke_request (in, req, resp, conn, srbid); //Send() , Thread ���� 
	assert (srbid == srbid2 || (srbid2 == 0 && !resp));
	
	if (!resp)
		COMM::release (req);
	
	return FALSE;
}

COMM::Boolean
SRBP::IISPServer::handle_cancel_request (GISPConn *conn, GISPInContext &in)
{
	COMM::ULong req_id;
	
	if (!conn->codec()->get_cancel_request (in, req_id)) {
		if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) {
			SRBP::Logger::Stream (SRBP::Logger::GISP)
				<< "GISP: cannot decode CancelRequest from "
				<< conn->transport()->peer()->stringify() << endl;
		}
		conn_error (conn, 0);
		return FALSE;
	}
	
	if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) {
		SRBP::Logger::Stream (SRBP::Logger::GISP)
			<< "GISP: incoming CancelRequest from "
			<< conn->transport()->peer()->stringify()
			<< " for msgid " << req_id << endl;
	}
	
	IISPServerInvokeRec *rec = get_invoke_reqid (req_id, conn);
	if (!rec) {
		return TRUE;
	}
	
	MsgId srbid = rec->srbid();
	rec->conn()->deref ();
	del_invoke_srbid (srbid);
    
	_srb->cancel (srbid);
	
	return FALSE;
}

void
SRBP::IISPServer::handle_invoke_reply (MsgId msgid) 
{
	COMM::SRBRequest *req;
	COMM::InvokeStatus stat = _srb->get_invoke_reply (msgid, req);
	IISPServerInvokeRec *rec = get_invoke_srbid (msgid);
	if (!rec) {
		// invocation canceled (perhaps connection to client broken)
		return;
	}
	
	GISP::ReplyStatusType gisp_stat = GISP::NO_EXCEPTION;
	switch (stat) {
	case COMM::InvokeSysEx:
		gisp_stat = GISP::SYSTEM_EXCEPTION;
		break;
		
	case COMM::InvokeUsrEx:
		gisp_stat = GISP::USER_EXCEPTION;
		break;
		
	case COMM::InvokeOk:
		gisp_stat = GISP::NO_EXCEPTION;
		break;
	}
	
	if (SRBP::Logger::IsLogged (SRBP::Logger::GISP)) 
	{
		SRBP::Logger::Stream (SRBP::Logger::GISP)
			<< "GISP: sending Reply to "
			<< rec->conn()->transport()->peer()->stringify()
			<< " for msgid " << rec->reqid()
			<< " status is " << (COMM::ULong) gisp_stat
			<< endl;
	}
	
	GISPOutContext out (rec->conn()->codec());
	if (!rec->conn()->codec()->put_invoke_reply (out, rec->reqid(), 
		gisp_stat, req)) {
		/*TODO
		out.reset ();
		COMM::MARSHAL ex;
		req->set_out_args (&ex);
		rec->conn()->codec()->put_invoke_reply (out, rec->reqid(),
		GISP::SYSTEM_EXCEPTION,
		obj, req, ad);
		*/
	}
	
	rec->conn()->output (out._retn());
	
	rec->conn()->deref ();
	del_invoke_srbid (rec->srbid());
}

void
SRBP::IISPServer::kill_conn (GISPConn *conn)
{
	COMM::Boolean again;
	
	do {
		again = FALSE;
		for (ListConn::iterator i = _conns.begin(); i != _conns.end(); ++i) 
		{
			if (*i == conn) 
			{
				_conns.erase (i);
				again = TRUE;
				break;
			}
		}
	} while (again);
	
	if (_cache_used && _cache_rec->conn() == conn)
	{
		_srb->cancel (_cache_rec->srbid());
		_cache_used = FALSE;
	}
	
	do 
	{
		again = FALSE;
		IISPServerInvokeRec *rec;
		for (MapIdConn::iterator i = _srbids.begin(); i != _srbids.end(); ++i) 
		{
			rec = (*i).second;
			if (rec->conn() == conn)
			{
				abort_invoke_srbid (rec->srbid());
				again = TRUE;
				break;
			}
		}
	} while (again);
	
	delete conn;
}

void
SRBP::IISPServer::conn_error (GISPConn *conn, COMM::Boolean send_error)
{
	if (!send_error) 
	{
		kill_conn (conn);
		return;
	}
	
	GISPOutContext out (conn->codec());
	conn->codec()->put_error_msg (out);
	conn->output (out._retn());
	conn->deref (TRUE);
	conn->flush();
}

void
SRBP::IISPServer::conn_closed (GISPConn *conn) 
{
	GISPOutContext out (conn->codec());
	conn->codec()->put_close_msg (out);
	conn->output (out._retn());
	conn->deref (TRUE);
	conn->flush();
}

COMM::Boolean
SRBP::IISPServer::has_service (const char *svcname)
{
	return FALSE;
}

COMM::Boolean
SRBP::IISPServer::is_local () const
{
	return FALSE;
}

COMM::Boolean
SRBP::IISPServer::invoke (MsgId, COMM::SRBRequest *, COMM::Boolean)
{
	assert (0);
	return TRUE;
}

int
SRBP::IISPServer::answer_invoke (COMM::ULong, COMM::SRBRequest *, COMM::InvokeStatus) 
{
	assert (0);
	return 0;
}

void
SRBP::IISPServer::shutdown (COMM::Boolean wait_for_completion) 
{
	for (srbp_vec_size_type i2 = 0; i2 < _tservers.size(); ++i2) 
	{
		_tservers[i2]->aselect (_srb->dispatcher(), 0);
		delete _tservers[i2];
	}
	_tservers.erase (_tservers.begin(), _tservers.end());
	
	for (ListConn::iterator i0 = _conns.begin(); i0 != _conns.end(); ++i0) 
	{
		conn_closed (*i0);
		delete *i0;
	}
	_conns.erase (_conns.begin(), _conns.end());
	
	if (_cache_used)
		_srb->cancel (_cache_rec->srbid());
	for (MapIdConn::iterator i1 = _srbids.begin(); i1 != _srbids.end(); ++i1) 
	{
		_srb->cancel ((*i1).first);
		delete (*i1).second;
	}
	_srbids.erase (_srbids.begin(), _srbids.end());
	_cache_used = FALSE;
	
	_srb->answer_shutdown (this);
}


