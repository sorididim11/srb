
#ifndef __srbp_process_impl_h__
#define __srbp_process_impl_h__

#if defined (_WINDOWS)
#include <winbase.h>
#endif

namespace SRBP {

class UnixProcess : public SRBP::Process, public COMM::DispatcherCallback {

    COMM::Long _exit_status;
    COMM::Boolean _detached;
    SRBP::ProcessCallback *_cb;
    string _args;
#if defined (_WINDOWS)
#define OSWIN_MAXPROCS 128
    HANDLE _hProcess;
    static HANDLE SRBP::UnixProcess::s_childprocs[OSWIN_MAXPROCS+1];
    static DWORD s_childpids[OSWIN_MAXPROCS+1];
    static int s_numofchildren;
    static void process_died(DWORD pid);
#if !defined(__MINGW32__)
    static unsigned int __stdcall wait_thread_func (VOID *arg);
    static unsigned int __stdcall ThreadExitFunc(VOID *arg);
#else
    static void __stdcall wait_thread_func (VOID *arg);
    static void __stdcall ThreadExitFunc(VOID *arg);
#endif
    static HANDLE s_waitthread;
    static int s_stop_waiting;

    HANDLE hRequestExitEvent;
    
    DWORD _pid;
#else
    COMM::Long _pid;
#endif
    typedef list<UnixProcess *> ListProcess;
    static ListProcess _procs;

    static void signal_handler (int sig);
public:
#if defined (_WINDOWS)
    static void _init();
    static void win32_process_init();
#endif
    UnixProcess (const char *cmd, SRBP::ProcessCallback * = 0);
    virtual ~UnixProcess ();

    virtual COMM::Boolean run ();
    virtual COMM::Boolean exited ();
    virtual COMM::Boolean exit_status ();
    virtual void terminate ();
    virtual void detach ();
    virtual operator COMM::Boolean ();

    virtual void callback (COMM::Dispatcher *,
			   COMM::DispatcherCallback::Event);
};

}

#endif // __srbp_process_impl_h__
