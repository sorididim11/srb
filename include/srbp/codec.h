
#ifndef __srbp_codec_h__
#define __srbp_codec_h__

namespace COMM {

/************************* ForwardDecls ******************************/


class Buffer;


/************************** Interfaces *******************************/


enum ByteOrder {
	BigEndian,
	LittleEndian,
	DefaultEndian
};


class DataEncoder {
public:
protected:
	Buffer *buf;
	Boolean dofree_buf;
public:
	DataEncoder ();
	DataEncoder (Buffer *b, Boolean dofree_b = TRUE);
	virtual ~DataEncoder ();

	virtual DataEncoder *clone () const = 0;
	virtual DataEncoder *clone (Buffer *b, 
			Boolean dofree_b = TRUE) const = 0;

	virtual DataDecoder *decoder () const = 0;
	virtual DataDecoder *decoder (Buffer *b, 
			Boolean dofree_b = TRUE) const = 0;

	virtual void put_short (Short) = 0;
	virtual void put_ushort (UShort) = 0;
	virtual void put_long (Long) = 0;
	virtual void put_longlong (LongLong) = 0;
	virtual void put_ulong (ULong) = 0;
	virtual void put_ulonglong (ULongLong) = 0;
	virtual void put_float (Float) = 0;
	virtual void put_double (Double) = 0;
	virtual void put_longdouble (LongDouble) = 0;
	virtual void put_char (Char) = 0;
	virtual void put_char_raw (Char) = 0;
	virtual void put_wchar (WChar) = 0;
	virtual void put_octet (Octet) = 0;
	virtual void put_boolean (Boolean) = 0;
	virtual void put_string (const char *) = 0;
	virtual void put_string_raw (const char *) = 0;
	virtual void put_wstring (const wchar_t *) = 0;

	virtual void put_shorts (const Short *, ULong) = 0;
	virtual void put_ushorts (const UShort *, ULong) = 0;
	virtual void put_longs (const Long *, ULong) = 0;
	virtual void put_longlongs (const LongLong *, ULong) = 0;
	virtual void put_ulongs (const ULong *, ULong) = 0;
	virtual void put_ulonglongs (const ULongLong *, ULong) = 0;
	virtual void put_floats (const Float *, ULong) = 0;
	virtual void put_doubles (const Double *, ULong) = 0;
	virtual void put_longdoubles (const LongDouble *, ULong) = 0;
	virtual void put_chars (const Char *, ULong) = 0;
	virtual void put_chars_raw (const Char *, ULong) = 0;
	virtual void put_wchars (const WChar *, ULong) = 0;
	virtual void put_booleans (const Boolean *, ULong) = 0;

	void put_string (const string &s);
	void put_string_raw (const string &s);
	virtual void put_buffer (const Buffer &);
	virtual void put_octets (const void *, ULong len);

	virtual void enumeration (ULong);

	virtual void struct_begin ();
	virtual void struct_end ();

	virtual void arr_begin ();
	virtual void arr_end ();

	virtual ULong max_alignment () const = 0;

	Buffer *buffer ()
	{ return buf; }

	void buffer (Buffer *, Boolean dofree = TRUE);

	virtual ByteOrder byteorder () const;
	virtual void byteorder (ByteOrder);
};

class DataDecoder {
public:
protected:
	Buffer *buf;
	Boolean dofree_buf;
public:
	DataDecoder (Buffer *b, Boolean dofree_b = TRUE);
	virtual ~DataDecoder ();

	virtual DataDecoder *clone () const = 0;
	virtual DataDecoder *clone (Buffer *b, 
			Boolean dofree_b = TRUE) const = 0;

	virtual DataEncoder *encoder () const = 0;
	virtual DataEncoder *encoder (Buffer *b, 
			Boolean dofree_b = TRUE) const = 0;

	virtual Boolean get_short (Short &) = 0;
	virtual Boolean get_ushort (UShort &) = 0;
	virtual Boolean get_long (Long &) = 0;
	virtual Boolean get_longlong (LongLong &) = 0;
	virtual Boolean get_ulong (ULong &) = 0;
	virtual Boolean get_ulonglong (ULongLong &) = 0;
	virtual Boolean get_float (Float &) = 0;
	virtual Boolean get_double (Double &) = 0;
	virtual Boolean get_longdouble (LongDouble &) = 0;
	virtual Boolean get_char (Char &) = 0;
	virtual Boolean get_char_raw (Char &) = 0;
	virtual Boolean get_wchar (WChar &) = 0;
	virtual Boolean get_octet (Octet &) = 0;
	virtual Boolean get_boolean (Boolean &) = 0;
	virtual Boolean get_string (String_out) = 0;
	virtual Boolean get_string_raw (String_out) = 0;
	virtual Boolean get_wstring (WString_out) = 0;

	virtual Boolean get_shorts (Short *, ULong) = 0;
	virtual Boolean get_ushorts (UShort *, ULong) = 0;
	virtual Boolean get_longs (Long *, ULong) = 0;
	virtual Boolean get_longlongs (LongLong *, ULong) = 0;
	virtual Boolean get_ulongs (ULong *, ULong) = 0;
	virtual Boolean get_ulonglongs (ULongLong *, ULong) = 0;
	virtual Boolean get_floats (Float *, ULong) = 0;
	virtual Boolean get_doubles (Double *, ULong) = 0;
	virtual Boolean get_longdoubles (LongDouble *, ULong) = 0;
	virtual Boolean get_chars (Char *, ULong) = 0;
	virtual Boolean get_chars_raw (Char *, ULong) = 0;
	virtual Boolean get_wchars (WChar *, ULong) = 0;
	virtual Boolean get_booleans (Boolean *, ULong) = 0;

	Boolean get_string_stl (string &);
	Boolean get_string_raw_stl (string &);
	virtual Boolean get_octets (void *, ULong len);

	virtual Boolean enumeration (ULong &);

	virtual Boolean struct_begin ();
	virtual Boolean struct_end ();

	virtual Boolean arr_begin ();
	virtual Boolean arr_end ();

	Boolean check_chunk ()
	{
		return TRUE;
	}

	virtual ULong max_alignment () const = 0;

	Buffer *buffer ()
	{ return buf; }

	void buffer (Buffer *, Boolean dofree = TRUE);

	virtual ByteOrder byteorder () const;
	virtual void byteorder (ByteOrder);
};

}

#endif //__srbp_codec_h__

