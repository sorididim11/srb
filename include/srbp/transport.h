
#ifndef __srbp_transport_h__
#define __srbp_transport_h__

namespace COMM {

/************************* ForwardDecls ******************************/


class Address;
class Dispatcher;
struct DispatcherCallback;
class Transport;
struct TransportCallback;
class TransportServer;
struct TransportServerCallback;
class Buffer;


/************************** Interfaces *******************************/


class Dispatcher 
{
public:

	//enum type
    enum Event { Timer, Read, Write, Except, All, Remove, Moved };

	//virtual Interface
    virtual void rd_event (DispatcherCallback *, Long fd) = 0;
    virtual void wr_event (DispatcherCallback *, Long fd) = 0;
    virtual void ex_event (DispatcherCallback *, Long fd) = 0;
    virtual void tm_event (DispatcherCallback *, ULong tmout) = 0;
    virtual void remove (DispatcherCallback *, Event) = 0;
    virtual void run (Boolean infinite = TRUE) = 0;
    virtual void move (Dispatcher *) = 0;
    virtual Boolean idle () const = 0;

    virtual ~Dispatcher ();
};

struct DispatcherCallback 
{
	//typddef
    typedef Dispatcher::Event Event;

	//virtual Interface
    virtual void callback (Dispatcher *, Event) = 0;

	//destructor
    virtual ~DispatcherCallback ();
};

class Timeout : public DispatcherCallback 
{
private:
	//data Block
    Boolean _ready;
    Dispatcher *_disp;
    Boolean _have_tmout;

public:
	//constructor & destructor
    Timeout (Dispatcher *d, Long tm);
    ~Timeout ();
	
	//override DispatcherCallback
    void callback (Dispatcher *, Event);
	
	//generic Function 
    Boolean done () const {		return _ready;		}
};


class Transport //Abstract 
{
public:

	//virtual Interface
    virtual void rselect (Dispatcher *, TransportCallback *) = 0;
    virtual void wselect (Dispatcher *, TransportCallback *) = 0;

    virtual Boolean bind (const Address *) = 0;
    virtual Boolean connect (const Address *) = 0;
    virtual void close () = 0;
    virtual void block (Boolean doblock = TRUE) = 0;
    virtual COMM::Boolean isblocking () = 0;
    virtual void buffering (Boolean dobuffering = TRUE);
    virtual COMM::Boolean isbuffering ();
    virtual COMM::Boolean isreadable () = 0;
   	virtual const Address *addr () = 0;
    virtual const Address *peer () = 0;
    virtual Boolean eof () const = 0;
    virtual Boolean bad () const = 0;
    virtual string errormsg () const = 0;

	virtual Long read (void *, Long len) = 0;
	virtual Long write (const void *, Long len) = 0;

	//generic Function
	Long read (Buffer &, Long len);
    Long write (Buffer &, Long len, Boolean eat = TRUE);
   
    //virtual COMM::Principal_ptr get_principal ();
	//destructor 
    virtual ~Transport ();
};

struct TransportCallback
 {
    enum Event { Read, Write, Remove };
    virtual void callback (Transport *, Event) = 0;
    virtual ~TransportCallback ();
};


class TransportServer
 {
public:

	//virtual Interface
    virtual void aselect (Dispatcher *, TransportServerCallback *) = 0;
    virtual Boolean bind (const Address *) = 0;
    virtual void close () = 0;
    virtual void block (Boolean doblock = TRUE) = 0;
    virtual COMM::Boolean isblocking () = 0;
    virtual Transport *accept () = 0;
    virtual const Address *addr () = 0;
    virtual Boolean bad () const = 0;
    virtual string errormsg () const = 0;

	//destructor 
    virtual ~TransportServer ();
};

struct TransportServerCallback 
{
    enum Event { Accept, Remove };
    virtual void callback (TransportServer *, Event) = 0;
    virtual ~TransportServerCallback ();
};

}

#endif // __srbp_transport_h__
