
#ifndef __srbp_sequence_h__
#define __srbp_sequence_h__

template<class T> class TSeqVar;


/*
 * C++ template for unbounded sequences. The element type of the sequence
 * can be of any type except for arrays and recursive types (see
 * SequenceIndTmpl)
 */
template<class T, int TID>
class SequenceTmpl {
public:
    typedef T &ElementType; // Needed in TSeqVar (see var.h)
    typedef TSeqVar<SequenceTmpl<T,TID> > _var_type;
private:
    vector<T> vec;
public:
    SequenceTmpl () {}
    SequenceTmpl (SRBP_ULong maxval)
    {
	vec.reserve (maxval);
    }
    SequenceTmpl (SRBP_ULong max, SRBP_ULong length, T *value,
		  SRBP_Boolean rel = FALSE);

    SequenceTmpl (const SequenceTmpl<T,TID> &s)
    {
	vec = s.vec;
    }
    
    ~SequenceTmpl ()
    {
    }
    
    void replace (SRBP_ULong max, SRBP_ULong length, T *value,
		  SRBP_Boolean rel = FALSE);

    SequenceTmpl<T,TID> &operator= (const SequenceTmpl<T,TID> &s)
    {
	vec = s.vec;
	return *this;
    }

    SRBP_ULong maximum () const
    {
	return vec.capacity ();
    }

    SRBP_Boolean release () const
    {
	// we always own the buffer
	return TRUE;
    }

    T* get_buffer (SRBP_Boolean orphan = FALSE);

    const T* get_buffer () const
    {
	assert (vec.size() > 0);
	return &vec[0];
    }

    void length (SRBP_ULong l);

    SRBP_ULong length () const;
    T &operator[] (SRBP_ULong idx);
    const T &operator[] (SRBP_ULong idx) const;

    static T *allocbuf (SRBP_ULong len)
    {
	return new T[len];
    }

    static void freebuf (T *b)
    {
	delete[] b;
    }

#if defined( __SUNPRO_CC ) || (defined( _WINDOWS ) && _MSC_VER < 1200)
    friend SRBP_Boolean
    operator== (const SequenceTmpl<T,TID> &v1, const SequenceTmpl<T,TID> &v2)
    {
	if (v1.length() != v2.length())
	    return FALSE;
	for (SRBP_ULong _i = 0; _i < v1.length(); ++_i) {
	    if (!(v1[_i] == v2[_i]))
		return FALSE;
	}
	return TRUE;
    }
#endif
};

template<class T, int TID>
SequenceTmpl<T,TID>::SequenceTmpl (SRBP_ULong maxval, SRBP_ULong lengthval, T *value,
				   SRBP_Boolean rel)
{
    assert (lengthval <= maxval);
    vec.reserve (maxval);
    vec.insert (vec.begin(), value, value+lengthval);
    if (rel)
	freebuf (value);
}

template<class T, int TID>
void
SequenceTmpl<T,TID>::replace (SRBP_ULong maxval, SRBP_ULong lengthval, T *value,
			      SRBP_Boolean rel)
{
    assert (lengthval <= maxval);
    vec.erase (vec.begin(), vec.end());
    vec.reserve (maxval);
    vec.insert (vec.begin(), value, value+lengthval);
    if (rel)
	freebuf (value);
}

template<class T, int TID>
void
SequenceTmpl<T,TID>::length (SRBP_ULong l)
{
    if (l < vec.size ()) {
	vec.erase (vec.begin() + l, vec.end());
    } else if (l > vec.size()) {
	T t;
	// the (long) cast is needed for SGI STL
	vec.insert (vec.end(), long(l - vec.size()), t);
    }
}

template<class T, int TID>
inline SRBP_ULong
SequenceTmpl<T,TID>::length () const
{
    return vec.size ();
}

template<class T, int TID>
inline T &
SequenceTmpl<T,TID>::operator[] (SRBP_ULong idx)
{
    return vec[idx];
}
    

template<class T, int TID>
inline const T &
SequenceTmpl<T,TID>::operator[] (SRBP_ULong idx) const
{
    return vec[idx];
}

template<class T, int TID>
T *
SequenceTmpl<T,TID>::get_buffer (SRBP_Boolean orphan)
{
    if (orphan) {
	T *b = allocbuf (vec.capacity());
	for (srbp_vec_size_type i = 0; i < vec.size(); ++i)
	    b[i] = vec[i];
	vec.erase (vec.begin(), vec.end());
	return b;
    } else {
	assert (vec.size() > 0);
	return &vec[0];
    }
}

#if !(defined( __SUNPRO_CC ) || (defined( _WINDOWS ) && _MSC_VER < 1200))
template<class T, int TID>
SRBP_Boolean
operator== (const SequenceTmpl<T,TID> &v1, const SequenceTmpl<T,TID> &v2)
{
    if (v1.length() != v2.length())
	return FALSE;
    for (SRBP_ULong _i = 0; _i < v1.length(); ++_i) {
	if (!(v1[_i] == v2[_i]))
	    return FALSE;
    }
    return TRUE;
}
#endif

/*
 * Same as SequenceTmpl except that this template is for bounded sequences.
 */
template<class T, int TID, int max>
class BoundedSequenceTmpl {
public:
    typedef T &ElementType; // Needed in TSeqVar (see var.h)
private:
    vector<T> vec;
public:
    BoundedSequenceTmpl ()
    {
	vec.reserve (max);
    }
    BoundedSequenceTmpl (SRBP_ULong length, T *value,
			 SRBP_Boolean rel = TRUE);
    
    BoundedSequenceTmpl (const BoundedSequenceTmpl<T, TID, max> &s)
    {
	vec = s.vec;
    }

    ~BoundedSequenceTmpl ()
    {
    }

    void replace (SRBP_ULong length, T *value,
		  SRBP_Boolean rel = TRUE);

    BoundedSequenceTmpl<T, TID, max> &operator=
	(const BoundedSequenceTmpl<T, TID, max> &s)
    {
	vec = s.vec;
	return *this;
    }

    SRBP_ULong maximum () const
    {
	return max;
    }

    SRBP_Boolean release () const
    {
	// we always own the buffer
	return TRUE;
    }

    T* get_buffer (SRBP_Boolean orphan = FALSE);

    const T* get_buffer () const
    {
	assert (vec.size() > 0);
	return &vec[0];
    }

    void length (SRBP_ULong l);

    SRBP_ULong length () const
    {
	return vec.size ();
    }

    T &operator[] (SRBP_ULong idx)
    {
	return vec[idx];
    }

    const T &operator[] (SRBP_ULong idx) const
    {
	return vec[idx];
    }

    static T *allocbuf (SRBP_ULong len)
    {
	return new T[len];
    }

    static void freebuf (T *b)
    {
	delete[] b;
    }

#if defined( __SUNPRO_CC ) || (defined( _WINDOWS ) && _MSC_VER < 1200)
    friend SRBP_Boolean
    operator== (const BoundedSequenceTmpl<T,TID,max> &v1,
		const BoundedSequenceTmpl<T,TID,max> &v2)
    {
	if (v1.length() != v2.length())
	    return FALSE;
	for (SRBP_ULong _i = 0; _i < v1.length(); ++_i) {
	    if (!(v1[_i] == v2[_i]))
		return FALSE;
	}
	return TRUE;
    }
#endif
};

template<class T, int TID, int max>
BoundedSequenceTmpl<T,TID,max>::BoundedSequenceTmpl (SRBP_ULong lengthval,
						     T *value,
						     SRBP_Boolean rel)
{
    assert (lengthval <= max);
    vec.reserve (max);
    vec.insert (vec.begin(), value, value+lengthval);
    if (rel)
	freebuf (value);
}

template<class T, int TID, int max>
void
BoundedSequenceTmpl<T,TID,max>::replace (SRBP_ULong lengthval,
					 T *value, SRBP_Boolean rel)
{
    assert (lengthval <= max);
    vec.erase (vec.begin(), vec.end());
    vec.reserve (max);
    vec.insert (vec.begin(), value, value+lengthval);
    if (rel)
	freebuf (value);
}

template<class T, int TID, int max>
void
BoundedSequenceTmpl<T,TID,max>::length (SRBP_ULong l)
{
    assert (l <= max);
    if (l < vec.size ()) {
	vec.erase (vec.begin() + l, vec.end());
    } else if (l > vec.size()) {
	T t;
	vec.insert (vec.end(), long(l - vec.size()), t);
    }
}


template<class T, int TID, int max>
T *
BoundedSequenceTmpl<T,TID,max>::get_buffer (SRBP_Boolean orphan)
{
    if (orphan) {
	T *b = allocbuf (vec.capacity());
	for (srbp_vec_size_type i = 0; i < vec.size(); ++i)
	    b[i] = vec[i];
	vec.erase (vec.begin(), vec.end());
	return b;
    } else {
	assert (vec.size() > 0);
	return &vec[0];
    }
}

#if !(defined( __SUNPRO_CC ) || (defined( _WINDOWS ) && _MSC_VER < 1200))
template<class T, int TID, int max>
SRBP_Boolean
operator== (const BoundedSequenceTmpl<T,TID,max> &v1,
	    const BoundedSequenceTmpl<T,TID,max> &v2)
{
    if (v1.length() != v2.length())
	return FALSE;
    for (SRBP_ULong _i = 0; _i < v1.length(); ++_i) {
	if (!(v1[_i] == v2[_i]))
	    return FALSE;
    }
    return TRUE;
}
#endif

/*
 * C++ template for sequences of arrays and recursive types. The difference
 * is, that this template maintains it members only indirectly through
 * pointers. This is especially necessary to break infinite recursion
 * for recursive types. If this template is used for arrays, then T_elem
 * denotes the element type of the array, T the array type itself and
 * n the total number of array elements. If the template is used for
 * recursive types then T_elem and T are the same and n should be set to 1.
 */
template<class T_elem, class T, SRBP_ULong n>
class SequenceIndTmpl {
public:
  typedef T &ElementType; // Needed in TSeqVar (see var.h)
private:
  vector<T_elem*> vec;
public:
  SequenceIndTmpl () {}
  SequenceIndTmpl (SRBP_ULong maxval)
  {
    vec.reserve (maxval);
  }
  SequenceIndTmpl (SRBP_ULong max, SRBP_ULong length, T* value,
		   SRBP_Boolean rel = TRUE);

  void replace (SRBP_ULong max, SRBP_ULong length, T *value,
		SRBP_Boolean rel = TRUE);

  SequenceIndTmpl (const SequenceIndTmpl<T_elem,T,n> &s);
    
  ~SequenceIndTmpl ();
  
  SequenceIndTmpl<T_elem,T,n>& 
  operator= (const SequenceIndTmpl<T_elem,T,n> &s);
  
  SRBP_ULong maximum () const
  {
    return vec.capacity ();
  }
  
  void length (SRBP_ULong l);
  
  SRBP_ULong length () const
  {
    return vec.size ();
  }

  SRBP_Boolean release () const
  {
    // we always own the buffer
    return TRUE;
  }

  // get_buffer() not supported ...
  
  T& operator[] (SRBP_ULong idx)
  {
    return (T&) *vec[idx];
  }
  
  const T& operator[] (SRBP_ULong idx) const
  {
    return (T&) *vec[idx];
  }
  
  static T *allocbuf (SRBP_ULong len);
  
  static void freebuf( T* b );
  
#if defined( __SUNPRO_CC ) || (defined( _WINDOWS ) && _MSC_VER < 1200)
  friend SRBP_Boolean
  operator== (const SequenceIndTmpl<T_elem,T,n> &v1,
	      const SequenceIndTmpl<T_elem,T,n> &v2)
  {
    if( v1.length() != v2.length() )
      return FALSE;
    for( SRBP_ULong i = 0; i < v1.length(); i++ ) {
      for( SRBP_ULong j = 0; j < n; j++ ) {
	T_elem e1 = ((T_elem*) v1[ i ])[ j ];
	T_elem e2 = ((T_elem*) v2[ i ])[ j ];
	if( !(e1 == e2) )
	  return FALSE;
      }
    }
    return TRUE;
  }
#endif
};

#if !(defined( __SUNPRO_CC ) || (defined( _WINDOWS ) && _MSC_VER < 1200))
template<class T_elem, class T, SRBP_ULong n>
SRBP_Boolean
operator== (const SequenceIndTmpl<T_elem,T,n> &v1,
	    const SequenceIndTmpl<T_elem,T,n> &v2)
{
  if( v1.length() != v2.length() )
    return FALSE;
  for( SRBP_ULong i = 0; i < v1.length(); i++ ) {
    for( SRBP_ULong j = 0; j < n; j++ ) {
      T_elem e1 = ((T_elem*) v1[ i ])[ j ];
      T_elem e2 = ((T_elem*) v2[ i ])[ j ];
      if( !(e1 == e2) )
	return FALSE;
    }
  }
  return TRUE;
}
#endif

/*
 * Same as SequenceIndTmpl except that this one is for bounded sequences.
 */
template<class T_elem, class T, SRBP_ULong n, int max>
class BoundedSequenceIndTmpl {
public:
  typedef T &ElementType; // Needed in TSeqVar (see var.h)
private:
  vector<T_elem*> vec;
public:
  BoundedSequenceIndTmpl ()
  {
    vec.reserve (max);
  }
  BoundedSequenceIndTmpl (SRBP_ULong length, T *value,
			  SRBP_Boolean rel = TRUE);
  
  BoundedSequenceIndTmpl
  (const BoundedSequenceIndTmpl<T_elem,T,n,max> &s);
  
  ~BoundedSequenceIndTmpl ();
  
  BoundedSequenceIndTmpl<T_elem,T,n, max> &operator=
  (const BoundedSequenceIndTmpl<T_elem,T,n, max> &s);
  
  void replace (SRBP_ULong length, T *value,
		SRBP_Boolean rel = TRUE);

  SRBP_ULong maximum () const
  {
    return max;
  }
  
  void length (SRBP_ULong l);
  
  SRBP_ULong length () const
  {
    return vec.size ();
  }
  
  SRBP_Boolean release () const
  {
    // we always own the buffer
    return TRUE;
  }

  // get_buffer() not supported ...

  T &operator[] (SRBP_ULong idx)
  {
    return (T&) *vec[idx];
  }
  
  const T &operator[] (SRBP_ULong idx) const
  {
    return (T&) *vec[idx];
  }
  
  static T *allocbuf (SRBP_ULong len);
  
  static void freebuf (T *b);

#if defined( __SUNPRO_CC ) || (defined( _WINDOWS ) && _MSC_VER < 1200)
  friend SRBP_Boolean
  operator== (const BoundedSequenceIndTmpl<T_elem,T,n,max> &v1,
	      const BoundedSequenceIndTmpl<T_elem,T,n,max> &v2)
  {
    if( v1.length() != v2.length() )
      return FALSE;
    for( SRBP_ULong i = 0; i < v1.length(); i++ ) {
      for( SRBP_ULong j = 0; j < n; j++ ) {
	T_elem e1 = ((T_elem*) v1[ i ])[ j ];
	T_elem e2 = ((T_elem*) v2[ i ])[ j ];
	if( !(e1 == e2) )
	  return FALSE;
      }
    }
    return TRUE;
  }
#endif
};

#if !(defined( __SUNPRO_CC ) || (defined( _WINDOWS ) && _MSC_VER < 1200))
template<class T_elem, class T, SRBP_ULong n, int max>
SRBP_Boolean
operator== (const BoundedSequenceIndTmpl<T_elem,T,n,max> &v1,
	    const BoundedSequenceIndTmpl<T_elem,T,n,max> &v2)
{
  if( v1.length() != v2.length() )
    return FALSE;
  for( SRBP_ULong i = 0; i < v1.length(); i++ ) {
    for( SRBP_ULong j = 0; j < n; j++ ) {
      T_elem e1 = ((T_elem*) v1[ i ])[ j ];
      T_elem e2 = ((T_elem*) v2[ i ])[ j ];
      if( !(e1 == e2) )
	return FALSE;
    }
  }
  return TRUE;
}
#endif

#endif // __srbp_sequence_h__
