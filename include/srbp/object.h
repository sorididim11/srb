
#ifndef __srbp_object_h__
#define __srbp_object_h__

namespace COMM
{
	
	class ServerlessObject : public MagicChecker
	{
	private:
		
		//data Block
		Long refs;
		
	public:
		
		//virtual Function
		virtual ~ServerlessObject ();
		
		//generic Function
		void _ref ();
		Boolean _deref ();
		Long _refcnt () const;
		
		//static Function
		static ServerlessObject* _nil () {		return 0;		}
		static ServerlessObject* _duplicate (ServerlessObject *o) 
		{
			if (o)
				o->_ref();
			return o;
		}
		
	protected:
		
		//constructor &destructor
		ServerlessObject (){		 refs = 1;		}
		ServerlessObject (const ServerlessObject &so): SRBP_SCOPE (COMM,MagicChecker) (so){		 refs = 1;		}
		
		//operator Function
		ServerlessObject &operator= (const ServerlessObject &){		 return *this;		}
	};
	
	
	//typedef
	typedef ServerlessObject *ServerlessObject_ptr;
	typedef ObjVar<ServerlessObject> ServerlessObject_var;
	typedef ObjOut<ServerlessObject> ServerlessObject_out;
	
	SRBP_INLINE_FCT_DECL void release (ServerlessObject *o) 
	{
		if (o && o->_deref())
			delete o;
	}
	
	SRBP_INLINE_FCT_DECL Boolean is_nil (ServerlessObject *o) 
	{
		return !o;
	}
	
}

#endif // __srbp_object_h__
