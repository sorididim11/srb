
/* Cmakek Variables */
#define HAVE_BYTEORDER_BE 0

#define HAVE_ANSI_CPLUSPLUS_HEADERS 1

#define HAVE_NAMESPACE 1

#define HAVE_BYTEORDER_H 

#define HAVE_EXCEPTION 
#define HAVE_DLFCN_H 1
#define HAVE_FCNTL_H 1
#define HAVE_DL_H 
#define HAVE_FLOAT_H 1
#define HAVE_IEEEFP_H 



#define SIZEOF_UNSIGNED_CHAR 1
#define SIZEOF_SHORT 2
#define SIZEOF_UNSIGNED_SHORT 2

#define SIZEOF_INT 4
#define SIZEOF_UNSIGNED_INT 4
#define SIZEOF_LONG 8
#define SIZEOF_UNSIGNED_LONG 8
#define SIZEOF_LONG_DOUBLE 16
#define SIZEOF_LONG_LONG 8
#define SIZEOF_UNSIGNED_LONG_LONG 8
#define SIZEOF_FLOAT 4
#define SIZEOF_DOUBLE 8




/* whether C++ compiler supports exception handling */
#undef HAVE_EXCEPTS

/* whether C++ compiler supports exceptions in shared libs */
#undef HAVE_SHARED_EXCEPTS

/* whether C++ compiler supports standard exception handling */
#undef HAVE_STD_EH




/* autoconf AC_HEADER_TIME
AC_CHECK_SOCKET_SIZE_T
AC_CHECK_SOCKET_ADDR_T
AC_CHECK_SELECT_ADDR_T
AC_CHECK_GETTIMEOFDAY
 */


/* type of size argument to socket calls */
#define socket_size_t 

/* type of sockaddr argument to socket calls */
#define socket_addr_t struct sockaddr *

/* type of fd_set arguments to select calls */
#define select_addr_t fd_set *









