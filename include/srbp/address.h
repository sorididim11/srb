
#ifndef __srbp_address_h__
#define __srbp_address_h__

namespace COMM 
{
	
	/************************* ForwardDecls ******************************/
	
	
	class Address;
	class AddressParser;
	class Transport;
	class TransportServer;
	
	
	/************************** Interfaces *******************************/
	
	
	class AddressParser
	{
	public:

		//virtual Interface
		virtual Address *parse (const char *rest, const char *proto) const = 0;
		virtual COMM::Boolean has_proto (const char *) const = 0;
		
		//destructor
		virtual ~AddressParser ();
	};
	
	class Address 
	{
	private:
		//Data Block
		static vector<AddressParser *> *parsers;
		
	protected:
		
		//generic Function
		void copy (Address *);
		
	public:
		
		//static Function
		static Address *parse (const char *);
		static void register_parser (AddressParser *);
		static void unregister_parser (AddressParser *);
		
		//virtual Interface
		virtual string stringify () const = 0;
		virtual const char *proto () const = 0;
		virtual Transport *make_transport () const = 0;
		virtual TransportServer *make_transport_server () const = 0;
		virtual Boolean is_local () const = 0;
		virtual Address *clone () const = 0;
		virtual Long compare (const Address &) const = 0;
		virtual Boolean operator== (const Address &) const = 0;
		virtual Boolean operator< (const Address &) const = 0;
		
		//destructor
		virtual ~Address ();
	};
	
}

#endif // __srbp_address_h__
