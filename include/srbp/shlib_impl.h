
#ifndef __srbp_shlib_impl_h__
#define __srbp_shlib_impl_h__

namespace SRBP {

class UnixSharedLib : public SRBP::SharedLib {
    void *_handle;
    string _error;
    string _name;
public:
    UnixSharedLib (const char *name);
    virtual ~UnixSharedLib ();

    virtual void *symbol (const char *);
    virtual const char *error ();
    virtual operator COMM::Boolean ();

    virtual const char *name ();
};

}

#endif // __srbp_shlib_impl_h__
