
#ifndef __srbp_srb_h__
#define __srbp_srb_h__

extern pthread_mutex_t gisp_conn_mut;
extern pthread_mutex_t handle_mut;
extern pthread_mutex_t iisp_p_ir;
extern pthread_mutex_t iisp_s_ir_mut;
extern pthread_mutex_t iisp_p_ir_mut;
extern pthread_mutex_t job_q_mut;
extern pthread_mutex_t msgid_mut;
extern pthread_mutex_t so_mut;
extern pthread_mutex_t srb_ir_mut;
extern pthread_mutex_t thread_count_mut;


namespace COMM 
{
	
	enum InvokeStatus
	{
		InvokeOk,
			InvokeSysEx,
			InvokeUsrEx
	};
	
	class SRBConn;
	class SRBRequest;
	class ServiceAdapter;
	class SRBService;
	class StaticRequest;
	

	//typedef 
	typedef SRBConn *SRBConn_ptr;
	typedef ObjVar<SRBConn> SRBConn_var;
	typedef ObjOut<SRBConn> SRBConn_out;
	
	typedef SRBRequest *SRBRequest_ptr;
	typedef ObjVar<SRBRequest> SRBRequest_var;
	typedef ObjOut<SRBRequest> SRBRequest_out;
	
	class SRBMarshaller
	{
	public:
		virtual void marshal (COMM::DataEncoder *) = 0;
	};
	
	class SRBDemarshaller 
	{
	public:
		virtual COMM::Boolean demarshal (COMM::DataDecoder *) = 0;
	};
	

	struct SRBCallback 
	{
		enum Event { Invoke };
		virtual void callback (SRB_ptr, ULong, Event) = 0;
		virtual ~SRBCallback ();
	};

	
	
	class SRBRequest : public ServerlessObject	
	{
	public:
		
		//virtual Interface 
		virtual ~SRBRequest ();
		virtual const COMM::Address *addr () = 0;
		virtual const COMM::Address *peer () = 0;
		virtual const char *svc_name () = 0;
		virtual COMM::DataEncoder *get_encoder () = 0;
		virtual COMM::DataDecoder *get_decoder () = 0;
		virtual void set_marshaller (SRBMarshaller *, SRBDemarshaller *) = 0;
		virtual void marshal (COMM::DataEncoder *) = 0;
		virtual COMM::Boolean demarshal (COMM::DataDecoder *) = 0;
		virtual void set_sys_exception (COMM::SysExceptionCode) = 0;
		virtual void set_sys_exception (COMM::SysExceptionCode, const char *) = 0;
		virtual void set_exception_type (COMM::ExceptionType) = 0;
		virtual COMM::ExceptionType get_exception_type () = 0;
		
		virtual COMM::Boolean copy_out_data (SRBRequest *) = 0;
		virtual COMM::Boolean copy_in_data (SRBRequest *) = 0;
		
		
		//Static Functon
		static SRBRequest* _nil () {		return 0;		}
		static SRBRequest* _duplicate (SRBRequest *o) 
		{
			if (o)
				o->_ref();
			return o;
		}
	};


	
	class SRBInvokeRec 
	{
	public:
		
		//typedef
		typedef ULong MsgId;
		
	private:
		
		//Data Block
		MsgId _myid;
		Boolean _have_result;
		Boolean _response_expected;
		InvokeStatus _invoke_stat;
		ServiceAdapter *_adapter;
		SRBRequest *_req;
		SRBCallback *_cb;
		SRB_ptr _srb;
		
		
	public:
		
		//construction & destruction
		SRBInvokeRec ();
		virtual ~SRBInvokeRec ();
		
		//generic Function
		void free ();
		void init_invoke (SRB_ptr, MsgId, SRBRequest *,Boolean response_expected, SRBCallback *, ServiceAdapter * = 0);
		MsgId id () const {		return _myid;	}
		Boolean completed () const {		return _have_result;		}
		ServiceAdapter *sa () {		return _adapter;		}
		SRBRequest *request () {		return _req;		}
		SRBCallback *callback () {		return _cb;			}
		void set_answer_invoke (InvokeStatus, SRBRequest *);
		Boolean get_answer_invoke (InvokeStatus &, SRBRequest *&);
	};
	
	class SRB : public ServerlessObject 
	{
	public:
		//typedef
		typedef ULong MsgId;

	private:
		//typedef 
		typedef vector<ServiceAdapter *> SAVec;
		typedef map<MsgId, SRBInvokeRec *, less<MsgId> > InvokeMap;
		
		//Data Block
		SAVec _adapters;
		InvokeMap _invokes;
		Dispatcher *_disp;
		MsgId _theid;
		Boolean _is_running;
		Boolean _is_shutdown;
		Boolean _is_stopped;
		Boolean _wait_for_completion;
		SAVec _shutting_down_adapters;
		
		SRBInvokeRec *_cache_rec;
		COMM::Boolean _cache_used;
		

		// private Function
		Boolean is_local (const char *svcname);
		ServiceAdapter *get_sa (const char *svcname);
		SRBInvokeRec *create_invoke ();
		void add_invoke (SRBInvokeRec *);
		SRBInvokeRec *get_invoke (MsgId);
		void del_invoke (MsgId);
		void do_shutdown ();
		
	public:
		//constructor & destructor
		SRB (int &argc, char **argv);
		virtual ~SRB ();
		

		//generic Function
		void run ();
		void shutdown (Boolean wait_for_completion);
		void destroy ();
		SRBConn *create_connection (const char *host = NULL, COMM::UShort port = 0);
		void close_connection (SRBConn *);
		Dispatcher *dispatcher () {		return _disp;		}
		void register_sa (ServiceAdapter *);
		void unregister_sa (ServiceAdapter *);
		void register_service (const char *, SRBService *);
		void unregister_service (const char *);
		MsgId new_msgid ();
		MsgId invoke_async (SRBRequest *, Boolean rply = TRUE, SRBCallback * = 0, MsgId = 0);
		InvokeStatus invoke (SRBRequest *, Boolean rply = TRUE);
		void cancel (MsgId);
		Boolean wait (MsgId, Long tmout = -1);
		int answer_invoke (MsgId, InvokeStatus, SRBRequest *);
		void answer_shutdown (ServiceAdapter *);
		InvokeStatus get_invoke_reply (MsgId, SRBRequest *&);
		
		//static Function
		static SRB_ptr _nil () {		return 0;		}
		static SRB_ptr _duplicate (SRB_ptr o) 
		{
			if (o)
				o->_ref();
			return o;
		}
		
		
	private :

		//struct
		typedef struct _job_node 
		{
			ServiceAdapter *sa;
			MsgId msgid;
			SRBRequest *req;
			Boolean response_exp;
			struct _job_node *next;
		} job_node;

		//Data Block
		job_node *head, *tail, *insert_ptr;
		int job_count;
		int thread_count, idle_thread_count;
		
		//private Function queue 
		void init_job_queue();
		int  get_job(job_node *job);
		void add_job(job_node *job);
		
		//static Thread Function
		static void* invoke_thread (void *args);
		static void  invoke_thread_win (void *args);
	};
	
	SRBP_EXPORT_FCT_DECL SRB_ptr SRB_init (int &argc, char **argv);
	
	
	
	class ServiceAdapter 
	{
	public:
		
		//typedef
		typedef ULong MsgId;
		
		//virtual Interface
		virtual Boolean has_service (const char *svcname) = 0;
		virtual Boolean is_local () const = 0;
		virtual Boolean invoke (MsgId, SRBRequest *, Boolean response_exp = TRUE) = 0;
		virtual int answer_invoke(MsgId, SRBRequest *, InvokeStatus) = 0;
		virtual void shutdown (Boolean wait_for_completion) = 0;
		
		//destructor
		virtual ~ServiceAdapter ();
	};
	
	class SRBService 
	{
	public:
		
		//virtual Interface
		virtual SRBMarshaller *marshaller () = 0;
		virtual SRBDemarshaller *demarshaller () = 0;
		virtual Boolean doservice () = 0;
	};
	
	class SRBConn 
	{
	public:
		virtual COMM::StaticRequest *create_request (const char *svcname) = 0;
	};
	
}

#endif //__srbp_srb_h__

