
#ifndef __srbp_transport_impl_h__
#define __srbp_transport_impl_h__

namespace SRBP
 {	
	
class SelectDispatcher : public COMM::Dispatcher 
{

private:

	//typedef
    typedef fd_set FDSet;


    struct FileEvent	//socket 반응 설정
	{
	Event event;
	COMM::Long fd;
	COMM::DispatcherCallback *cb;
	COMM::Boolean deleted;

	FileEvent () {}
	FileEvent (Event _ev, COMM::Long _fd, COMM::DispatcherCallback *_cb): event(_ev), fd(_fd), cb(_cb), deleted(FALSE)  {}
    };

    struct TimerEvent //시간 반응 설정 
	{
	Event event;
	COMM::Long delta;
	COMM::DispatcherCallback *cb;

	TimerEvent () {}
	TimerEvent (Event _ev, COMM::Long _delta,COMM::DispatcherCallback *_cb): event(_ev), delta(_delta), cb(_cb)	{}
    };


	//Data Block
    list<FileEvent> fevents;
    list<TimerEvent> tevents;

    COMM::Long last_update;
    COMM::Boolean init;
    COMM::Long locked;
    COMM::Boolean modified;
    FDSet curr_wset, curr_rset, curr_xset;			//fd_set socket의 반응 감지 
    COMM::Long fd_max;


	//private Function	
    void lock ();
    void unlock ();
    COMM::Boolean islocked () const;
    COMM::Long gettime () const;
    void update_tevents ();
    void handle_tevents ();
    void handle_fevents (FDSet &rset, FDSet &wset, FDSet &xset);
    void update_fevents ();
    void sleeptime (OSMisc::TimeVal &);

public:

	//constructor & destructor
    SelectDispatcher ();
    virtual ~SelectDispatcher ();

	//override Dispatcher
    virtual void rd_event (COMM::DispatcherCallback *, COMM::Long fd);
    virtual void wr_event (COMM::DispatcherCallback *, COMM::Long fd);
    virtual void ex_event (COMM::DispatcherCallback *, COMM::Long fd);
    virtual void tm_event (COMM::DispatcherCallback *, COMM::ULong tmout);
    virtual void remove (COMM::DispatcherCallback *, Event);
    virtual void move (COMM::Dispatcher *);
    virtual void run (COMM::Boolean infinite = TRUE);
    virtual COMM::Boolean idle () const;
};


class TCPTransport : public COMM::Transport,public COMM::DispatcherCallback 
{
private:
	
	//Data Block
    COMM::Dispatcher *rdisp, *wdisp;
    COMM::TransportCallback *rcb, *wcb;
    InetAddress local_addr, peer_addr;

	string err;
	COMM::Boolean ateof;
    COMM::Boolean is_blocking;
    COMM::Boolean is_buffering;

public:

	//Data Block
    COMM::Long fd;

	//constructor & destructor
    TCPTransport (COMM::Long fd = -1);
    ~TCPTransport ();
    

	//override Transport
    virtual void rselect (COMM::Dispatcher *, COMM::TransportCallback *);
    virtual void wselect (COMM::Dispatcher *, COMM::TransportCallback *);
    virtual COMM::Boolean bind (const COMM::Address *);
    virtual COMM::Boolean connect (const COMM::Address *);
    virtual void close ();
    virtual void block (COMM::Boolean doblock = TRUE);
    virtual COMM::Boolean isblocking ();
    virtual void buffering (COMM::Boolean dobuffering = TRUE);
    virtual COMM::Boolean isbuffering ();
    virtual COMM::Boolean isreadable ();
    virtual COMM::Long read (void *, COMM::Long len);
    virtual COMM::Long write (const void *, COMM::Long len);
    virtual const COMM::Address *addr ();
    virtual const COMM::Address *peer ();
    virtual COMM::Boolean eof () const;
    virtual COMM::Boolean bad () const;
    virtual string errormsg () const;

	//override DispatcherCallback
	virtual void callback (COMM::Dispatcher *, COMM::Dispatcher::Event);
};


class TCPTransportServer : public COMM::TransportServer,public COMM::DispatcherCallback 
{
private:

	//Data Block
    COMM::Dispatcher *adisp;
    COMM::TransportServerCallback *acb;
    InetAddress local_addr;
	string err;
	COMM::Long fd;
    COMM::Boolean listening;
    COMM::Boolean is_blocking;

	//private Function
	void listen ();
	
public:

	//constructor & destructor
    TCPTransportServer ();
    ~TCPTransportServer ();
    
	//override TransportServer
    void aselect (COMM::Dispatcher *,COMM::TransportServerCallback *);
    COMM::Boolean bind (const COMM::Address *);
    void close ();
    void block (COMM::Boolean doblock = TRUE);
    COMM::Boolean isblocking ();
    COMM::Transport *accept ();
    const COMM::Address *addr ();
    COMM::Boolean bad () const;
    string errormsg () const;

	//override DispatcherCallback
	void callback (COMM::Dispatcher *, COMM::Dispatcher::Event);
};

}

#endif // __srbp_transport_impl_h__
