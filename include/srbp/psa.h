
#ifndef __srbp_psa_h__
#define __srbp_psa_h__

namespace SRBP 
{

class PSA : public COMM::ServiceAdapter 
{
	//typedef
	typedef map<string, COMM::SRBService *, less<string> > ServiceMap;

	//Data Block
	COMM::SRB_ptr _srb;
	ServiceMap _services;

public:

	//constructor & destructor
	PSA (COMM::SRB_ptr);
	~PSA ();

	
	void add_service (const char *svcname, COMM::SRBService *);
	void del_service (const char *svcname);
	COMM::SRBService *get_service (const char *svcname);

	//override ServiceAdapter
	virtual COMM::Boolean has_service (const char *svcname);
	virtual COMM::Boolean is_local () const;
	virtual COMM::Boolean invoke (COMM::ULong, COMM::SRBRequest *, COMM::Boolean response_exp);
	virtual int answer_invoke(COMM::ULong, COMM::SRBRequest *, COMM::InvokeStatus);
	virtual void shutdown (COMM::Boolean wait_for_completion);
};

}

#endif // _srbp_psa_h__
