
#ifndef __SRBP_TEMPLATE_IMPL_H__
#define __SRBP_TEMPLATE_IMPL_H__

/*
 * The following declarations belong to the ObjVar and
 * SequenceIndTmpl template.
 * The reason they are defined here is that these particular
 * methods make reference to interface methods (like _nil(),
 * _release(), etc) which are not known at the time these
 * templates are being used.
 */

//----- ObjVar implementations ---------------------------------------------

template<class T>
T *
ObjVar<T>::duplicate (T *t)
{
    return T::_duplicate (t);
}

template<class T>
void
ObjVar<T>::release (T *t)
{
    COMM::release (t);
}

//----- ObjOut implementations ---------------------------------------------

template <class T>
ObjOut<T>::ObjOut (T*& p)
  : _ptr (p)
{
    _ptr = T::_nil();
}

template <class T>
ObjOut<T>::ObjOut (ObjVar<T>& p)
  : _ptr (p._ptr)
{
    COMM::release (_ptr);
    _ptr = T::_nil();
}

template <class T>
ObjOut<T>&
ObjOut<T>::operator= (const ObjVar<T>& p)
{
    _ptr = T::_duplicate (p._ptr);
    return *this;
}


//----- ValueVar implementations -------------------------------------------

template<class T>
T *
ValueVar<T>::duplicate (T *t)
{
    if (t)
	t->_add_ref();
    return t;
}

template<class T>
void
ValueVar<T>::release (T *t)
{
    if (t)
	t->_remove_ref();
}

//----- SequenceIndTmpl implementations ------------------------------------

template<class T_elem, class T, SRBP_ULong n>
SequenceIndTmpl<T_elem,T,n>::SequenceIndTmpl (SRBP_ULong maxval,
					      SRBP_ULong lengthval,
					      T* value,
					      SRBP_Boolean rel)
{
  assert (lengthval <= maxval);
  vec.reserve (maxval);
  for (SRBP_ULong i = 0; i < lengthval; ++i) {
    T_elem* new_elem = new T_elem[ n ];
    T_elem* dest = new_elem;
    T_elem* src = (T_elem*) (value + i);
    SRBP_ULong j = 0;
    while( j < n ) {
      *dest = *src;
      ++dest;
      ++src;
      j++;
    }
    vec.push_back ( new_elem );
  }
  if (rel)
    freebuf (value);
}

template<class T_elem, class T, SRBP_ULong n>
void
SequenceIndTmpl<T_elem,T,n>::replace (SRBP_ULong maxval,
				      SRBP_ULong lengthval,
				      T* value,
				      SRBP_Boolean rel)
{
  assert (lengthval <= maxval);
  for( srbp_vec_size_type i0 = 0; i0 < vec.size(); i0++ )
    delete[] vec[ i0 ];
  vec.erase (vec.begin(), vec.end());
  vec.reserve (max);
  for (SRBP_ULong i1 = 0; i1 < lengthval; ++i1) {
    T_elem* new_elem = new T_elem[ n ];
    T_elem* dest = new_elem;
    T_elem* src = (T_elem*) (value + i1);
    SRBP_ULong j = 0;
    while( j < n ) {
      *dest = *src;
      ++dest;
      ++src;
      j++;
    }
    vec.push_back ( new_elem );
  }
  if (rel)
    freebuf (value);
}

template<class T_elem, class T, SRBP_ULong n>
SequenceIndTmpl<T_elem,T,n>::
SequenceIndTmpl (const SequenceIndTmpl<T_elem,T,n> &s)
{
  for( COMM::ULong i = 0; i < s.length(); i++ ) {
    T_elem* new_elem = new T_elem[ n ];
    T_elem* dest = new_elem;
    T_elem* src = s.vec[ i ];
    SRBP_ULong j = 0;
    while( j < n ) {
      *dest = *src;
      ++dest;
      ++src;
      j++;
    }
    vec.push_back ( new_elem );
  }
}

template<class T_elem, class T, SRBP_ULong n>
SequenceIndTmpl<T_elem,T,n>::~SequenceIndTmpl ()
{
  for( srbp_vec_size_type i = 0; i < vec.size(); i++ )
    delete[] vec[ i ];
}

template<class T_elem, class T, SRBP_ULong n>
SequenceIndTmpl<T_elem,T,n>& 
SequenceIndTmpl<T_elem,T,n>::operator= (const SequenceIndTmpl<T_elem,T,n> &s)
{
  for( srbp_vec_size_type i0 = 0; i0 < vec.size(); i0++ )
    delete[] vec[ i0 ];
  vec.erase( vec.begin(), vec.end() );
  for( SRBP_ULong i1 = 0; i1 < s.length(); i1++ ) {
    T_elem* new_elem = new T_elem[ n ];
    T_elem* dest = new_elem;
    T_elem* src = s.vec[ i1 ];
    SRBP_ULong j = 0;
    while( j < n ) {
      *dest = *src;
      ++dest;
      ++src;
      j++;
    }
    vec.push_back ( new_elem );
  }
  return *this;
}

template<class T_elem, class T, SRBP_ULong n>
void
SequenceIndTmpl<T_elem,T,n>::length (SRBP_ULong l)
{
  if (l < vec.size ()) {
    for( srbp_vec_size_type i = l; i < vec.size(); i++ )
      delete[] vec[ i ];
    vec.erase (vec.begin() + l, vec.end());
  } else if (l > vec.size()) {
    int limit = l - vec.size();
    for( int i = 0; i < limit; i++ )
      vec.push_back( new T_elem[ n ] );
  }
}

template<class T_elem, class T, SRBP_ULong n>
T*
SequenceIndTmpl<T_elem,T,n>::allocbuf (SRBP_ULong len)
{
  return (T*) new T_elem[ n * len ];
}

template<class T_elem, class T, SRBP_ULong n>
void
SequenceIndTmpl<T_elem,T,n>::freebuf( T* b )
{
  delete[] (T_elem*) b;
}


//----- BoundedSequenceIndTmpl implementations -----------------------------

template<class T_elem, class T, SRBP_ULong n, int max>
BoundedSequenceIndTmpl<T_elem,T,n,max>::
BoundedSequenceIndTmpl (SRBP_ULong lengthval, T *value,
			SRBP_Boolean rel)
{
  assert (lengthval <= max);
  vec.reserve (max);
  for (SRBP_ULong i = 0; i < lengthval; ++i) {
    T_elem* new_elem = new T_elem[ n ];
    T_elem* dest = new_elem;
    T_elem* src = (T_elem*) (value + i);
    SRBP_ULong j = 0;
    while( j < n ) {
      *dest = *src;
      ++dest;
      ++src;
      j++;
    }
    vec.push_back ( new_elem );
  }
  if (rel)
    freebuf (value);
}

template<class T_elem, class T, SRBP_ULong n, int max>
void
BoundedSequenceIndTmpl<T_elem,T,n,max>::replace (SRBP_ULong lengthval, T *value,
						 SRBP_Boolean rel)
{
  assert (lengthval <= max);
  for( srbp_vec_size_type i0 = 0; i0 < vec.size(); i0++ )
    delete[] vec[ i0 ];
  vec.erase (vec.begin(), vec.end());
  vec.reserve (max);
  for (SRBP_ULong i1 = 0; i1 < lengthval; ++i1) {
    T_elem* new_elem = new T_elem[ n ];
    T_elem* dest = new_elem;
    T_elem* src = (T_elem*) (value + i1);
    SRBP_ULong j = 0;
    while( j < n ) {
      *dest = *src;
      ++dest;
      ++src;
      j++;
    }
    vec.push_back ( new_elem );
  }
  if (rel)
    freebuf (value);
}

template<class T_elem, class T, SRBP_ULong n, int max>
BoundedSequenceIndTmpl<T_elem,T,n,max>::
BoundedSequenceIndTmpl (const BoundedSequenceIndTmpl<T_elem,T,n,max> &s)
{
  vec.reserve( max );
  for( COMM::ULong i = 0; i < s.length(); i++ ) {
    T_elem* new_elem = new T_elem[ n ];
    T_elem* dest = new_elem;
    T_elem* src = s.vec[ i ];
    SRBP_ULong j = 0;
    while( j < n ) {
      *dest = *src;
      ++dest;
      ++src;
      j++;
    }
    vec.push_back ( new_elem );
  }
}

template<class T_elem, class T, SRBP_ULong n, int max>
BoundedSequenceIndTmpl<T_elem,T,n,max>::~BoundedSequenceIndTmpl ()
{
  for( srbp_vec_size_type i = 0; i < vec.size(); i++ )
    delete[] vec[ i ];
}

template<class T_elem, class T, SRBP_ULong n, int max>
BoundedSequenceIndTmpl<T_elem,T,n, max> &
BoundedSequenceIndTmpl<T_elem,T,n,max>::
operator= (const BoundedSequenceIndTmpl<T_elem,T,n, max> &s)
{
  for( srbp_vec_size_type i0 = 0; i0 < vec.size(); i0++ )
    delete[] vec[ i0 ];
  vec.erase( vec.begin(), vec.end() );
  for( SRBP_ULong i1 = 0; i1 < s.length(); i1++ ) {
    T_elem* new_elem = new T_elem[ n ];
    T_elem* dest = new_elem;
    T_elem* src = s.vec[ i1 ];
    SRBP_ULong j = 0;
    while( j < n ) {
      *dest = *src;
      ++dest;
      ++src;
      j++;
    }
    vec.push_back ( new_elem );
  }
  return *this;
}

template<class T_elem, class T, SRBP_ULong n, int max>
void
BoundedSequenceIndTmpl<T_elem,T,n,max>::length (SRBP_ULong l)
{
  assert (l <= max);
  if (l < vec.size ()) {
    for( srbp_vec_size_type i = l; i < vec.size(); i++ )
      delete[] vec[ i ];
    vec.erase (vec.begin() + l, vec.end());
  } else if (l > vec.size()) {
    int limit = l - vec.size();
    for( int i = 0; i < limit; i++ )
      vec.push_back( new T_elem[ n ] );
  }
}

template<class T_elem, class T, SRBP_ULong n, int max>
T*
BoundedSequenceIndTmpl<T_elem,T,n,max>::allocbuf (SRBP_ULong len)
{
  return (T*) new T_elem[ n * len ];
}

template<class T_elem, class T, SRBP_ULong n, int max>
void
BoundedSequenceIndTmpl<T_elem,T,n,max>::freebuf (T *b)
{
  delete[] (T_elem*) b;
}

#endif

