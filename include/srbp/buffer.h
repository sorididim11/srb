
#ifndef __srbp_srbp_h__
#define __srbp_srbp_h__

//#define DEBUG_BUF

namespace COMM 
{
    
	class Buffer
	{
	private:
		
		//enum type
		enum 
		{
			MINSIZE = 32,
				RESIZE_THRESH = 1024 * 1024,
				RESIZE_INCREMENT = 128 * 1024
		};
		
		//Data Block
		Boolean _readonly;
		ULong _rptr, _wptr;
		ULong _ralignbase, _walignbase;
		ULong _len;
		Octet *_buf;
		
		//private Function
		Octet *alloc (ULong sz);
		Octet *realloc (Octet *, ULong osz, ULong nsz);
		void free (Octet *);
		
	public:
		
		//constructor & destructor
		Buffer (void *);
		Buffer (ULong sz = 0);
		Buffer (const Buffer &);
		~Buffer ();
		
		//operator Function
		Buffer &operator= (const Buffer &);
		Boolean operator== (const Buffer &);
		
		
		//generic Function
		void reset (ULong size = MINSIZE);
		void doresize (ULong needed);
		void resize (ULong needed)
		{
			if (_wptr + needed > _len)
				doresize (needed);
		}
		
		Boolean rseek_rel (Long offs)
		{
			if (_rptr + offs > _wptr)
				return FALSE;
			_rptr += offs;
			return TRUE;
		}
		Boolean rseek_beg (ULong offs)
		{
			if (offs > _wptr)
				return FALSE;
			_rptr = offs;
			return TRUE;
		}
		Boolean rseek_end (ULong offs)
		{
			if (offs > _len || _len - offs > _wptr)
				return FALSE;
			_rptr = _len - offs;
			return TRUE;
		}
		ULong ralign_base () const
		{
			return _ralignbase;
		}
		void ralign_base (ULong b)
		{
			_ralignbase = b;
		}
		ULong rpos () const
		{
			return _rptr;
		}
		
		Boolean ralign (ULong modulo)
		{
#ifdef DEBUG_BUF
			assert (modulo >= 1);
			assert (_rptr >= _ralignbase);
#endif
			ULong r = modulo - ((_rptr - _ralignbase) % modulo);
			if (r != modulo) 
			{
				_rptr += r;
				if (_rptr > _wptr) 
				{
					_rptr -= r;
					return FALSE;
				}
			}
			return TRUE;
		}
		
		Boolean peek (void *, ULong blen);
		Boolean peek (Octet &);
		
		Boolean get (void *, ULong blen);
		Boolean get (Octet &);
		Boolean get1 (void *);
		Boolean get2 (void *);
		Boolean get4 (void *);
		Boolean get8 (void *);
		Boolean get16 (void *);
		
		void wseek_rel (Long offs)
		{
#ifdef DEBUG_BUF
			assert (!_readonly);
			assert (_wptr + offs >= _rptr &&_wptr + offs <= _len);
#endif
			_wptr += offs; // Packet Offeset �̵� 
		}
		void wseek_beg (ULong offs)
		{
#ifdef DEBUG_BUF        
			assert (!_readonly);
			assert (offs >= _rptr &&
				offs <= _len);
#endif
			_wptr = offs;
		}
		void wseek_end (ULong offs)
		{
#ifdef DEBUG_BUF
			assert (!_readonly);
			assert (_len - offs >= _rptr);
#endif 
			_wptr = _len - offs;
		}
		ULong walign_base () const
		{
			return _walignbase;
		}
		void walign_base (ULong b)
		{
			_walignbase = b;
		}
		ULong wpos () const
		{
#ifdef DEBUG_BUF
			assert (!_readonly);
#endif
			return _wptr;
		}
		
		void walign (ULong modulo)
		{
#ifdef DEBUG_BUF
			assert (!_readonly);
			assert (modulo >= 1);
			assert (_wptr >= _walignbase);
#endif
			ULong w = modulo - ((_wptr - _walignbase) % modulo);
			if (w != modulo) {
				resize (w);
				while (w--) {
					_buf[_wptr++] = 0;
				}
			}
		}
		
		void replace (const void *, ULong blen);
		void replace (Octet);
		
		void put (const void *, ULong blen);
		void put (Octet);
		void put1 (const void *);
		void put2 (const void *);
		void put4 (const void *);
		void put8 (const void *);
		void put16 (const void *);
		
		ULong buf_len () const
		{
			return _wptr;
		}
		ULong length () const
		{
			return _wptr - _rptr;
		}
		Octet *data ()
		{
			return &_buf[_rptr];
		}
		const Octet *data () const
		{
			return &_buf[_rptr];
		}
		Octet *wdata ()
		{
			return &_buf[_wptr];
		}
		const Octet *wdata () const
		{
			return &_buf[_wptr];
		}
		Octet *buffer ()
		{
			return _buf;
		}
		const Octet *buffer () const
		{
			return _buf;
		}
		
		void dump (const char *, ostream &) const;
};

typedef TVarVar<Buffer> Buffer_var;

}

#endif // __srbp_srbp_h__
