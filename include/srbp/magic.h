
#ifndef __srbp_magic_h__
#define __srbp_magic_h__

namespace COMM 
{
	
#define SRBP_OBJ_CHECK(o)    (o)->_check ()
#define SRBP_OBJ_CHECK2(o,e) (o)->_check (e)
#define SRBP_OBJ_MAGIC       0x31415927
	
	class MagicChecker 
	{
	private:
		
		//Data Block
		ULong magic;
		
	public:
		
		void _check () const;
		void _check (const COMM::Exception &) const;
		COMM::Boolean _check_nothrow () const;
	protected:
		
		//constructor & destructor
		MagicChecker (){		 magic = SRBP_OBJ_MAGIC;		}
		~MagicChecker (){		 magic = 0;			}
		MagicChecker (const MagicChecker &){ magic = SRBP_OBJ_MAGIC; }
		
		//operator
		MagicChecker &operator= (const MagicChecker &){		 return *this;		}
	};
	
}

#endif // __srbp_magic_h__
