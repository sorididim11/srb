
#ifndef __srbp_address_impl_h__
#define __srbp_address_impl_h__

namespace SRBP 
{
	
	class LocalAddress : public COMM::Address 
	{
	public:
		LocalAddress ();
		~LocalAddress ();
		
		string stringify () const;
		const char *proto () const;
		COMM::Transport *make_transport () const;
		COMM::TransportServer *make_transport_server () const;
		COMM::Boolean is_local () const;
		
		COMM::Address *clone () const;
		
		COMM::Long compare (const COMM::Address &) const;
		COMM::Boolean operator== (const COMM::Address &) const;
		COMM::Boolean operator< (const COMM::Address &) const;
	};
	
	class LocalAddressParser : public COMM::AddressParser 
	{
	public:
		LocalAddressParser ();
		~LocalAddressParser ();
		COMM::Address *parse (const char *, const char *) const;
		COMM::Boolean has_proto (const char *) const;
	};
	
	
	class InetAddress : public COMM::Address 
	{
	public:
		
		//enum type
		enum Family 
		{
			STREAM,
				DGRAM
		};
	private:
		
		//Data Block
		COMM::UShort _port;
		string _host;
		vector<COMM::Octet> _ipaddr;
		Family _family;
		
		static COMM::Boolean _resolve;
		static string hname;
		static vector<COMM::Octet> hid;
		
		//private Function
		COMM::Boolean resolve_ip () const;
		COMM::Boolean resolve_host () const;
		
	public:
		
		//constructor & destructor
		InetAddress (struct sockaddr_in &sin, Family = STREAM);
		InetAddress (const char *host = NULL, COMM::UShort port = 0,Family = STREAM);
		InetAddress (const vector<COMM::Octet> &, COMM::UShort port = 0,Family = STREAM);
		~InetAddress ();
		
		//override Address
		virtual string stringify () const;
		virtual const char *proto () const;
		virtual COMM::Transport *make_transport () const;
		virtual COMM::TransportServer *make_transport_server () const;
		virtual COMM::Boolean is_local () const;
		virtual COMM::Address *clone () const;
		virtual COMM::Long compare (const COMM::Address &) const;
		virtual COMM::Boolean operator== (const COMM::Address &) const;
		virtual COMM::Boolean operator< (const COMM::Address &) const;
		
		//generic Function
		COMM::Boolean valid () const;
		const char *host () const;
		void host (const char *);
		const vector<COMM::Octet> &ipaddr () const;
		void ipaddr (const vector<COMM::Octet> &);
		COMM::UShort port () const;
		void port (COMM::UShort port);
		void sockaddr (struct sockaddr_in &);
		struct sockaddr_in sockaddr () const;
		void family (Family f);
		Family family () const;
		
		//static Function
		static string hostname ();
		static vector<COMM::Octet> hostid();
		static COMM::Boolean samehosts (const string &h1, const string &h2);
		static COMM::Boolean resolve ();
		static void resolve (COMM::Boolean);
	};
	
	class InetAddressParser : public COMM::AddressParser {
	public:
		InetAddressParser ();
		~InetAddressParser ();
		COMM::Address *parse (const char *, const char *) const;
		COMM::Boolean has_proto (const char *) const;
	};
	
}

#endif // __srbp_address_impl_h__
