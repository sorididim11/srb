
#ifndef __srbp_os_math_h__
#define __srbp_os_math_h__

#if defined(_WINDOWS)

#include <math.h>
#include <limits>
#ifndef __BORLANDC__
#include <fpieee.h>
#endif

struct OSMath {
    static SRBP_Double infinity (SRBP_Boolean neg)
    {
	static SRBP_Double minus_inf = 0, plus_inf = 0;
	if (plus_inf == 0) {
	    plus_inf = numeric_limits<double>::infinity();
	    minus_inf = -plus_inf;
	}
	return neg ? minus_inf : plus_inf;
    }

    static SRBP_Double nan ()
    {
	static SRBP_Double not_a_number = 0;
	if (not_a_number == 0) {
	    not_a_number = numeric_limits<double>::quiet_NaN();
	}
	return not_a_number;
    }

    static SRBP_Boolean is_infinity (SRBP_Double d)
    {
	if (!_finite (d) && !_isnan (d)) 
	    return TRUE;
	else
	    return FALSE;
    }

    static SRBP_Boolean is_nan (SRBP_Double d)
    {
	if (_isnan (d))
	    return TRUE;
	else 
	    return FALSE;
    }

    static SRBP_LongDouble infinityl (SRBP_Boolean neg)
    {
	static SRBP_LongDouble minus_inf = 0, plus_inf = 0;
	if (plus_inf == 0) {
	    plus_inf = numeric_limits<long double>::infinity();
	    minus_inf = -plus_inf;
	}
	return neg ? minus_inf : plus_inf;
    }

    static SRBP_LongDouble nanl ()
    {
	static SRBP_LongDouble not_a_number = 0;
	if (not_a_number == 0) {
	    not_a_number = numeric_limits<long double>::quiet_NaN();
	}
	return not_a_number;
    }

    static SRBP_Boolean is_infinityl (SRBP_LongDouble d)
    {
	if (!_finite (d) && !_isnan (d)) 
	    return TRUE;
	else
	    return FALSE;
    }

    static SRBP_Boolean is_nanl (SRBP_LongDouble d)
    {
	if (_isnan (d))
	    return TRUE;
	else 
	    return FALSE;
    }
};

#else // neither _WINDOWS

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>

#if !defined(__GNUG__) || !defined(__sgi)
// conflict for initstate ()
#include <math.h>
#endif

#ifdef HAVE_FLOAT_H
#include <float.h>
#endif
#ifdef HAVE_IEEEFP_H
#include <ieeefp.h>
#endif
#ifdef HAVE_SUNMATH_H
#include <sunmath.h>
#endif

#include <srbp/lmath.h>

class OSMath {
    static SRBP_Double _infinity;
    static SRBP_LongDouble _long_infinity;
    static SRBP_Double _notanumber;
    static SRBP_LongDouble _long_notanumber;
public:
    static SRBP_Double infinity (SRBP_Boolean neg)
    {
#if defined(HAVE_INFNAN)
	return infnan (neg ? -ERANGE : ERANGE);
#else
	if (_infinity == 0) {
	    struct sigaction act, oldact;
	    act.sa_handler = SIG_IGN;
	    act.sa_flags = 0;
	    sigemptyset (&act.sa_mask);
	    sigaction (SIGFPE, &act, &oldact);
	    // XXX assumption: +/- 1/0 == +/- infinity
	    _infinity = -1 / floor (0.1);
	    sigaction (SIGFPE, &oldact, NULL);
	}
	return neg ? -_infinity : _infinity;
#endif
    }

    static SRBP_Double nan ()
    {
#if defined(HAVE_INFNAN)
	return infnan (0);
#else
	if (_notanumber == 0) {
	    struct sigaction act, oldact;
	    act.sa_handler = SIG_IGN;
	    act.sa_flags = 0;
	    sigemptyset (&act.sa_mask);
	    sigaction (SIGFPE, &act, &oldact);
	    // XXX assumption: arcsin (2.0) == NAN
	    _notanumber = asin (2.0);
	    sigaction (SIGFPE, &oldact, NULL);
	}
	return _notanumber;
#endif
    }

    static SRBP_Boolean is_infinity (SRBP_Double d)
    {
#if defined(HAVE_ISINF)
	return isinf (d);
#elif defined(HAVE_FINITE) && defined(HAVE_ISNAN)
	return !finite (d) && !isnan (d);
#else
	return d == infinity (TRUE) || d == infinity (FALSE);
#endif
    }

    static SRBP_Boolean is_nan (SRBP_Double d)
    {
#if defined(HAVE_ISNAN)
	return isnan (d);
#else
	// d == mk_nan() does not work ...
	assert (0);
#endif
    }

    static SRBP_LongDouble infinityl (SRBP_Boolean neg)
    {
#if defined(HAVE_INFNANL)
	return infnanl (neg ? -ERANGE : ERANGE);
#else
	if (_long_infinity == 0) {
	    struct sigaction act, oldact;
	    act.sa_handler = SIG_IGN;
	    act.sa_flags = 0;
	    sigemptyset (&act.sa_mask);
	    sigaction (SIGFPE, &act, &oldact);
	    // XXX assumption: +/- 1/0 == +/- infinity
	    _long_infinity = (long double)-1 / floor (0.1);
	    sigaction (SIGFPE, &oldact, NULL);
	}
	return neg ? -_long_infinity : _long_infinity;
#endif
    }
/*TODO truevoid
    static SRBP_LongDouble nanl ()
    {
#if defined(HAVE_INFNANL)
	return infnanl (0);
#else
	if (_long_notanumber == 0) {
	    struct sigaction act, oldact;
	    act.sa_handler = SIG_IGN;
	    act.sa_flags = 0;
	    sigemptyset (&act.sa_mask);
	    sigaction (SIGFPE, &act, &oldact);
	    // XXX assumption: arcsin (2.0) == NAN
	    _long_notanumber = asinl (2.0);
	    sigaction (SIGFPE, &oldact, NULL);
	}
	return _long_notanumber;
#endif
    }
*/
    static SRBP_Boolean is_infinityl (SRBP_LongDouble d)
    {
#if defined(HAVE_ISINFL)
	return isinfl (d);
#elif defined(HAVE_ISINF)
	return isinf (d);
#else
	return d == infinityl (TRUE) || d == infinityl (FALSE);
#endif
    }

    static SRBP_Boolean is_nanl (SRBP_LongDouble d)
    {
#if defined(HAVE_ISNANL)
	return isnanl (d);
#elif defined(HAVE_ISNAN)
	return isnan (d);
#else
	// d == mk_nan() does not work ...
	assert (0);
#endif
    }
};

#endif

#endif // __srbp_os_math_h__
