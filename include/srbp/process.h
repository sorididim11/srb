
#ifndef __srbp_process_h__
#define __srbp_process_h__

namespace SRBP {

class Process;

struct ProcessCallback {
    enum Event { Exited };
    virtual void callback (Process *, Event) = 0;
    virtual ~ProcessCallback ();
};

class Process {
public:
    virtual ~Process ();
    virtual COMM::Boolean run () = 0;
    virtual COMM::Boolean exited () = 0;
    virtual COMM::Boolean exit_status () = 0;
    virtual void terminate () = 0;
    virtual void detach () = 0;
    virtual operator COMM::Boolean () = 0;
};

}

#endif // __srbp_process_h__
