
#ifndef __srbp_types_h__
#define __srbp_types_h__

/* basic types */
#if SIZEOF_SHORT == 2
typedef short SRBP_Short;
#else
#error "sizeof(short) != 2"
#endif

#if SIZEOF_LONG == 4
typedef long SRBP_Long;
#elif SIZEOF_INT == 4
typedef int SRBP_Long;
#else
#error "sizeof(long) != 4 and sizeof(int) != 4"
#endif

#if SIZEOF_LONG == 8
typedef long SRBP_LongLong;
#elif SIZEOF_LONG_LONG == 8 || defined(__MINGW32__)
typedef long long SRBP_LongLong;
#elif defined(_WINDOWS)
typedef __int64 SRBP_LongLong;
#else
#error "sizeof(long) != 8 and sizeof(long long) != 8"
#endif

#if SIZEOF_UNSIGNED_SHORT == 2
typedef unsigned short SRBP_UShort;
#else
#error "sizeof(unsigned short) != 2"
#endif

#if SIZEOF_UNSIGNED_LONG == 4
typedef unsigned long SRBP_ULong;
#elif SIZEOF_UNSIGNED_INT == 4
typedef unsigned int SRBP_ULong;
#else
#error "sizeof(unsigned long) != 4 and sizeof(unsigned int) != 4"
#endif

#if SIZEOF_UNSIGNED_LONG == 8
typedef unsigned long SRBP_ULongLong;
#elif SIZEOF_UNSIGNED_LONG_LONG == 8 || defined(__MINGW32__)
typedef unsigned long long SRBP_ULongLong;
#elif defined(_WINDOWS)
typedef unsigned __int64 SRBP_ULongLong;
#else
#error "sizeof(unsigned long) != 8 and sizeof(unsigned long long) != 8"
#endif

#if SIZEOF_FLOAT == 4
typedef float SRBP_Float;
#else
#error "sizeof(float) != 4"
#endif

#if SIZEOF_DOUBLE == 8
typedef double SRBP_Double;
#else
#error "sizeof(double) != 8"
#endif

typedef long double SRBP_LongDouble;

#if SIZEOF_UNSIGNED_CHAR == 1
// XXX changed to char by AP for testsuite
typedef char SRBP_Char;
typedef unsigned char SRBP_Boolean;
typedef unsigned char SRBP_Octet;
#else
#error "sizeof(unsigned char) != 1"
#endif

typedef wchar_t SRBP_WChar;

#define SRBP_TID_DEF   0
#define SRBP_TID_BOOL  1
#define SRBP_TID_CHAR  2
#define SRBP_TID_OCTET 3
#define SRBP_TID_WCHAR 4

#ifdef __cplusplus


#define SRBP_MAKE_WRAPPER(T,V)			\
class T {					\
    V rep;					\
public:						\
    T ()					\
    {						\
    }						\
    T (const T &t)				\
	: rep (t.rep)				\
    {						\
    }						\
    T (V t)					\
	: rep(t)				\
    {						\
    }						\
    T &operator= (const T &t)			\
    {						\
	rep = t.rep;				\
	return *this;				\
    }						\
    T &operator= (V t)				\
    {						\
	rep = t;				\
	return *this;				\
    }						\
    int operator== (const T &t) const		\
    {						\
	return rep == t.rep;			\
    }						\
    int operator!= (const T &t) const		\
    {						\
	return rep != t.rep;			\
    }						\
    operator V & () const			\
    {						\
	return (V &)rep;			\
    }						\
};

#endif /* __cplusplus */

#endif /* __srbp_types_h__ */
